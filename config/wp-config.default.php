<?php
/**
 * Default config settings
 *
 * Enter any WordPress config settings that are default to all environments
 * in this file. These can then be overridden in the environment config files.
 * 
 * Please note if you add constants in this file (i.e. define statements) 
 * these cannot be overridden in environment config files.
 * 
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */
  

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '749Z?Cc$&H/YS8@DkotD<<PF,y{,&|% BUU2jm&SgRVMs^-NAiIQKbGcOyo:9cgB');
define('SECURE_AUTH_KEY',  'u@<C$RyT{6_adR-2_5`Hq)gfvw}!`F_oInw,*7SKAsZ+@gG6}HLxP:R)5Lzc)6~4');
define('LOGGED_IN_KEY',    't4=fi[&wc?jTSxex|D^bsE<KT.7H5xX{o,nkz5b||pAS0$XMX%@),2-|jqX{R4)S');
define('NONCE_KEY',        'p.R,UGiFHQ5=RzZh4PBGy88#_pgUb6>Pr7xc6g&w*ZTKK%R@qOqlTeBTK.#GMb,k');
define('AUTH_SALT',        '&,<-d=s=&8O_X%B }|pqGkxTWNKOkxq}_<|btD3*bovWV$wjTlsLGn x+l@sFNA1');
define('SECURE_AUTH_SALT', 'BM7JQ<6USy0R.=^tUg1/A9Fy8h.X?F/`PbhUia:fwBUb=`0aZU dH7U4#n#(a5fl');
define('LOGGED_IN_SALT',   '8eGp{k}{^:0CaS~KN6w#6UQIZ^S|uBGdNR@&[j#t6Cx*clk!;VV [1TdQwCf5>~j');
define('NONCE_SALT',       '1l[!%J! Ri1~`plnb$jJ541|ee.ZSww{ Nu/ bG B!AM3dCB@DhT8N.rtmbh.YEL');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'le_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * Increase memory limit. 
 */
define('WP_MEMORY_LIMIT', '512M');