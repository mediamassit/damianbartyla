(function ($) {
    $(document).ready(function () {

        dependence();

        var melements = getalldependence();

        var nelements = melements.join(", ");

        $(nelements).change(function () {

            dependence();

        });

        timepicker_init();

        var $autoplay = false;

        if ($(".inowlcarousel").data('autoplay') > 0) {
            $autoplay = true;
        }

        var $autoWidth = false;

        if ($(".inowlcarousel").data('autowidth') > 0) {
            $autoWidth = true;
        }

        var $navText = ['', ''];

        if ($(".inowlcarousel").data('navtext') > 0) {
            $navText = ["<i class='custom-icon-owl-controls-next'></i>", "<i class='custom-icon-owl-controls-prev'></i>"];
        }


        var $autoplayTimeout = 3000;

        if ($(".inowlcarousel").data('scroll-timeout') > 0) {
            $autoplayTimeout = $(".inowlcarousel").data('scroll-timeout');
        }

        if ($.fn.simpleexpand) {
            $('.expander').simpleexpand();
        }

        if (jQuery().owl2Carousel) {

            $(".inowlcarousel").owl2Carousel({
                autoplay: $autoplay,
                autoplayTimeout: $autoplayTimeout,
                autoplayHoverPause: true,
                autoWidth: $autoWidth,
                navText: $navText,
                nav: true,
                autoHeight: true,
                transitionStyle: "fade",
                items: 1,
            });
        }

        colorpicker_init();

    });


    $(document).ajaxComplete(function () {

        dependence();

        var melements = getalldependence();

        var nelements = melements.join(", ");

        $(nelements).change(function () {

            dependence();

        });

        colorpicker_init();

        timepicker_init();

    });

    function timepicker_init() {
        if ($.fn.wtimepicker) {

            $(".wtimepicker:not([id^='widget-timetable-widget-__i__'])").wtimepicker({
                timeFormat: $(this).data('format'),
                onClose: function (dateText, inst) {

                    var el = $(this).attr('id');
                    var pos = el.indexOf('_e');
                    if (pos != -1) {
                        var startDateTextBox = $(this).parent().find('input:first');
                        var endDateTextBox = $(this);
                        if ($(this).val() == '') $(this).val('00:00');
                        if (startDateTextBox.val() != '') {
                            var testStartDate = startDateTextBox.datepicker('getDate');
                            var testEndDate = endDateTextBox.datepicker('getDate');
                            if (testStartDate > testEndDate)
                                startDateTextBox.datepicker('setDate', testEndDate);
                            /* 									console.log(testEndDate); */
                        }
                        else {
                            startDateTextBox.val(dateText);
                        }
                    } else {
                        var startDateTextBox = $(this);
                        var endDateTextBox = $(this).parent().find('input:eq(1)');
                        if ($(this).val() != '') {

                            var testStartDate = startDateTextBox.datepicker('getDate');
                            var testEndDate = endDateTextBox.datepicker('getDate');
                            if (testStartDate > testEndDate)
                                endDateTextBox.datepicker('setDate', testStartDate);
                        }
                        else {
                            $(this).val('00:00');
                            endDateTextBox.val(dateText);
                        }

                    }


                },
            });


        }
    }

    function colorpicker_init() {
        if ($.fn.spectrum) {
            $('.color-picker').spectrum({
                preferredFormat: "rgb",
                showAlpha: true,
                clickoutFiresChange: true,
                showButtons: true,
                change: function (color) {
                    $(this).spectrum("set", color);
                },


            });
        }
    }

    function dependence() {
        $(".dependence").each(function (i) {

            if ($(this).data('dependence') != '') {

                var rules = $(this).data('dependence').split(/\;\s*/g);
                for (var i = 0; i < rules.length; i++) {
                    rules[i] = rules[i].split(/\:\s*/g);

                    var $container = $(this),
                        $el = $('#' + rules[i][0]);

                    var arrneed = rules[i][1].split(/\,\s*/g);

                    if ($el.getType() == 'checkbox') {
                        var need = arrneed,
                            $el_val = $el.is(':checked').toString();
                    } else {
                        var need = arrneed,
                            $el_val = $el.val();
                    }

                    //console.log(need);

                    if ($.inArray($el_val, need) !== -1) {
                        $(this).show();
                    } else {
                        $(this).hide();
                        break;
                    }
                }


            }
        });
    }


    function getalldependence() {

        var $elements = [];

        $(".dependence").each(function (k) {

            if ($(this).data('dependence') != '') {

                var rules = $(this).data('dependence').split(/\;\s*/g);
                for (var i = 0; i < rules.length; i++) {
                    rules[i] = rules[i].split(/\:\s*/g);

                    $elements.push('#' + rules[i][0]);

                }

                $elements = $.unique($elements);

            }
        });

        return $elements;

    }


    jQuery.fn.selcat = function (jcategories, cont) {

        if (typeof cont === 'undefined') {
            var mmcont = $(".buildq");
        } else {
            var mmcont = cont.closest('div.buildq');
        }

        mmcont.each(function () {
            var checked = new Array();

            var mcont = $(this);
            var $select = mcont.find('select.depcats');

            var defval = $select.data('defval');


            $select.find('option').remove().end();


            /* console.log(defval); */

            mcont.find("input.clposttype:checked").each(function () {
                checked.push($(this).val());
            });

            var cuselected = '';
            if (0 == JSON.stringify(defval)) {
                cuselected = 'selected="selected"';
            } else {
                cuselected = '';
            }

            $select.append($("<option " + cuselected + "></option>").attr("value", 0).text('All'));


            $.each(jcategories, function (i) {
                if ($.inArray(i, checked) > -1) {

                    $.each(jcategories[i], function (ind) {
                        //console.log(jcategories[i]);

                        var val = {
                            id: jcategories[i][ind]['term_id'],
                            slug: jcategories[i][ind]['slug'],
                            taxonomy: jcategories[i][ind]['taxonomy']
                        };
                        var selected = '';
                        if (JSON.stringify(val) == JSON.stringify(defval)) {
                            selected = 'selected="selected"';
                        } else {
                            selected = '';
                        }


                        $select.append($("<option " + selected + "></option>")
                            .attr("value", JSON.stringify(val))
                            .text(jcategories[i][ind]['cat_name']));


                    });
                }
            });


        });

        /* return this; */


    };


    $.fn.getType = function () {
        return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase();
    }

})(jQuery);

