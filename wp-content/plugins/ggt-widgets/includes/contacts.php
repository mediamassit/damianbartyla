<?php
add_action('widgets_init', 'ggt_contacts_load_widgets');
add_filter( 'wpseo_sitemap_index', array( 'GGT_Geositemap', 'add_to_wpseo_sitemap' ) );


function ggt_contacts_load_widgets()
{
	register_widget('ggt_contacts_widget');
}

class GGT_Geositemap {
    protected $kml_data;
    protected $sitemap_data;
    protected $name;
    protected $coords;
    protected $complete_address;

    public function __construct( $data ) {
        $upload_dir = wp_upload_dir();
        $this->name = strtolower( remove_accents( trim( str_replace( ' ', '', $data['name'] ) ) ) );
        $complete_address = $data['address'];
        $coords = $this->get_coords( $complete_address );
        $this->kml_data = '<?xml version="1.0" encoding="UTF-8"?>
            <kml xmlns="http://www.opengis.net/kml/2.2">
              <Placemark>
                <name>' . $data['name'] . '</name>
                <description></description>
                <Point>
                  <coordinates>' . $coords['lat'] . ',' . $coords['lon'] . '</coordinates>
                </Point>
              </Placemark>
            </kml>';
        
        $this->sitemap_data = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
                        <url>
                           <loc>' . $upload_dir['baseurl'] . '/' . $this->name . '.kml</loc>
                        </url>
                        
                        </urlset>';
    }

    public function save() {
        $upload_dir = wp_upload_dir();
        $url = wp_nonce_url('widgets.php?editwidget=ggt_contacts-widget-2','ggt-contact-widget');
        if (false === ($creds = request_filesystem_credentials($url, '', false, false, null) ) ) {
            return; // stop processing here
        }
        if ( ! WP_Filesystem($creds) ) {
            request_filesystem_credentials($url, '', true, false, null);
            return;
        }
        global $wp_filesystem;
        $wp_filesystem->put_contents(
            $upload_dir['basedir'] . '/' . $this->name . '.kml',
            $this->kml_data
        );
        $wp_filesystem->put_contents(
            $upload_dir['basedir'] . '/rc_geositemap.xml',
            $this->sitemap_data
        );
    }

    private function get_coords( $address ) {
    	$coords=array();
        $base_url="http://maps.googleapis.com/maps/api/geocode/xml?";
        // ajouter &region=FR si ambiguité (lieu de la requete pris par défaut)
        $request_url = $base_url . "address=" . urlencode($address).'&sensor=false';
        $data = wp_remote_get( $request_url );
        $xml = wp_remote_retrieve_body( $data );
        $xml_content = simplexml_load_string( $xml );

        $coords['lat'] = $coords['lon'] = '';
        $coords['status'] = $xml_content->status ;
        if($coords['status']=='OK') {
            $coords['lat'] = $xml_content->result->geometry->location->lat ;
            $coords['lon'] = $xml_content->result->geometry->location->lng ;
        }
        return $coords;
	}

    public function add_to_wpseo_sitemap() {
        $upload_dir = wp_upload_dir();
        return '<sitemap>
        <loc>' . $upload_dir['baseurl'] . '/rc_geositemap.xml</loc>
        <lastmod>1970-01-01T00:00:00+00:00</lastmod>
        </sitemap>';
    }
}


class ggt_contacts_widget extends WP_Widget {

	public function widget_keys() {
		$widget_keys = apply_filters( 'rc_widget_keys', array(
			'title',
			'name',
			'address',
			'phone',
			'mobile',
			'fax',
			'email',
			'email_text',
			'web',
			'web_text',
			'map',
			'mapzoom',
			'map_width',
			'map_height',
			)
		);
		return $widget_keys;
	}


	function ggt_contacts_widget()
	{
		$widget_ops = array('classname' => 'ggt_contacts_widget', 'description' => 'Contact Info');

		$control_ops = array('id_base' => 'ggt_contacts-widget');

		$this->WP_Widget('ggt_contacts-widget', __('GGT Contact'), $widget_ops, $control_ops);
	}

	public function widget( $args, $instance ) {
	
		  wp_enqueue_style( 'ggt_swidget_extend_style', plugins_url('/../assets/css/custom.css', __FILE__) );
		  wp_enqueue_style( 'ggt_swidget_font-ggtcontrols', plugins_url('/../assets/css/font-ggtcontrols.css', __FILE__) );

	      wp_enqueue_style('thickbox');
	
	      wp_enqueue_script( 'ggt_swidget_extend_js', plugins_url('/../assets/js/custom.js', __FILE__), array('jquery') );
	      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
	      wp_enqueue_script( 'ggt_swidget_jquery-expand', plugins_url('/../assets/js/expand.min.js', __FILE__), array('jquery') );

		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
		
		$widget_output = '<ul class="vcard" itemscope itemtype="http://schema.org/'. (!empty($instance['type']) ? $instance['type'] : '') . '">';
			if ( !empty( $instance['name'] ) )
				$widget_output .= '<li class="fn" itemprop="name"><strong>' . $instance['name'] . '</strong></li>';
			if ( !empty( $instance['address'] ) )
				$widget_output .= '<li class="addres" itemprop="address"><strong>' . $instance['address'] . '</strong></li>';
			if ( !empty( $instance['phone'] ) ) {
				$widget_output .= '<li class="tel" itemprop="telephone">';

				global $wp_version;
				if ( version_compare( $wp_version, '3.4', '>=' ) && wp_is_mobile() ) {
				        $widget_output .= '<a href="tel:' . $instance['phone'] . '">' . $instance['phone'] . '</a>';
				}
				else
				    $widget_output .= $instance['phone'];
				$widget_output .= '</li>';
			}
			if ( !empty( $instance['mobile'] ) ) {
				$widget_output .= '<li class="mobile" itemprop="mobile">';

				global $wp_version;
				if ( version_compare( $wp_version, '3.4', '>=' ) && wp_is_mobile() ) {
				        $widget_output .= '<a href="tel:' . $instance['mobile'] . '">' . $instance['mobile'] . '</a>';
				}
				else
				    $widget_output .= $instance['mobile'];
				$widget_output .= '</li>';
			}
			if ( !empty( $instance['fax'] ) ) {
				$widget_output .= '<li class="fax" itemprop="fax">';

				global $wp_version;
				if ( version_compare( $wp_version, '3.4', '>=' ) && wp_is_mobile() ) {
				        $widget_output .= '<a href="tel:' . $instance['fax'] . '">' . $instance['fax'] . '</a>';
				}
				else
				    $widget_output .= $instance['fax'];
				$widget_output .= '</li>';
			}
			if ( !empty( $instance['email'] ) ) {
				$widget_output .= '<li class="email" itemprop="email"><a href="mailto:' . antispambot($instance['email']) . '">' . $instance['email_text'] . '</a></li>';
			}
			if ( !empty( $instance['web'] ) ) {
				$widget_output .= '<li class="web" itemprop="web"><a href="' .$instance['web']. '">' . $instance['web_text'] . '</a></li>';
			}


		$widget_output .= '</ul>';
		if (!empty($instance['map']) && $instance['map']) {
		
            if ( empty( $instance['map_width'] ) ) {
                $instance['map_width'] = 200;
            }

            if ( $instance['map_width'] > 640 ) {
                $instance['map_width'] = 640;
            }

            if ( empty( $instance['map_height'] ) ) {
                $instance['map_height'] = 200;
            }

            if ( $instance['map_height'] > 640 ) {
                $instance['map_height'] = 640;
            }

		      $map_adress = $instance['address'];
		      $zoom = str_replace('-','',filter_var($instance['mapzoom'], FILTER_SANITIZE_NUMBER_INT));
		      
		      $mapzoom = ((int)$zoom > 0) ? (int)$zoom : '15';
		      
		      
		$encoded_map_adress = str_replace( ' ', '+', $map_adress );
            
    		$widget_output .= '<a href="http://mapof.it/'. $encoded_map_adress . '"><img class="ggt_contacts_widget_mapimg" src="http://maps.googleapis.com/maps/api/staticmap?center=' . $encoded_map_adress . '&amp;zoom='.$mapzoom.'&amp;size=' . $instance['map_width'] . 'x' . $instance['map_height'] . '&amp;sensor=false&amp;markers=' . $encoded_map_adress . '" alt="' . __('Map for', 'rich_contact-widget') . ' ' . $map_adress . '" width="' . $instance['map_width'] . '" height="' . $instance['map_height'] . '"></a>';
        }
		$widget_output = apply_filters( 'rc_widget_output', $widget_output, $instance );
		echo $widget_output;
		echo $after_widget;
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		foreach ( $this->widget_keys() as $key=>$value ) {
			if ( $old_instance[ $value ] != $new_instance[ $value ] || !array_key_exists($value, $old_instance) ) {
				$new_instance[ $value ] = strip_tags( $new_instance[$value] );
			}
		}
		
		$old_instance['map'] = isset( $new_instance['map'] ) ? (bool) $new_instance['map'] : false;

        $geositemap = new GGT_Geositemap( $new_instance );
        $geositemap->save();
		return $new_instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		foreach ( $this->widget_keys() as $key=>$value ) {
			if ( !array_key_exists( $value, $instance ) && $value == 'title' ) {
				${$value} = __( 'Contact', 'ggt-contact-widget' );
			} elseif ( !array_key_exists( $value, $instance ) ) {
				${$value} = '';
			} else {
				${$value} = $instance[ $value ];
			}
		}
		
		$map     = isset( $instance['map'] ) ? (bool) $instance['map'] : false;

		$widget_form_output = '<p>
			<label for="' . $this->get_field_id( 'map' ) . '">' . __( 'Show Google map? :', 'ggt-contact-widget' ) . '</label>
			
						<input id="'.$this->get_field_id( 'map' ).'" class="checkbox" type="checkbox" '.checked( $map, 1, false ).' name="' . $this->get_field_name( 'map' ) . '" />
		</p>';
		
		$widget_form_output .= '<p class="dependence" data-dependence="'.$this->get_field_id( 'map' ).':true">
			<label for="' . $this->get_field_id( 'mapzoom' ) . '">' . __( 'Map zoom:', 'ggt-contact-widget' ) . '</label>
			
						<input class="widefat" id="' . $this->get_field_id( 'mapzoom' ). '" name="' . $this->get_field_name( 'mapzoom' ) . '" type="text" value="' . esc_attr( $mapzoom ) . '" />
		</p>';
		
		$widget_form_output .= '<p>
		<label for="' . $this->get_field_id( 'title' ) . '">' . __( 'Title :' , 'ggt-contact-widget') . '</label> 
		<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" />
		</p>';
		
		$widget_form_output .= '<p>
			<label for="' . $this->get_field_id( 'name' ) . '">' . __( 'Company name/Your name :', 'ggt-contact-widget' ) . '</label>
			<input class="widefat" id="' . $this->get_field_id( 'name' ). '" name="' . $this->get_field_name( 'name' ) . '" type="text" value="' . esc_attr( $name ) . '" />
		</p>';
		$widget_form_output .= '<p>
			<label for="' . $this->get_field_id( 'address' ) . '">' . __( 'Company address :', 'ggt-contact-widget' ) . '</label>
			<textarea class="widefat" id="' . $this->get_field_id( 'address' ) . '" name="' . $this->get_field_name( 'address' ) . '">' . esc_textarea( $address ) . '</textarea>
		</p>';
		$widget_form_output .= '<p>
			<label for="' . $this->get_field_id( 'phone' ) . '">' . __( 'Phone number :', 'ggt-contact-widget' ) . '</label>
			<input class="widefat" id="' . $this->get_field_id( 'phone' ) . '" name="' . $this->get_field_name( 'phone' ) . '" type="text" value="' . esc_attr( $phone ) . '" />
		</p>';
		$widget_form_output .= '<p>
			<label for="' . $this->get_field_id( 'mobile' ) . '">' . __( 'Mobile :', 'ggt-contact-widget' ) . '</label>
			<input class="widefat" id="' . $this->get_field_id( 'mobile' ) . '" name="' . $this->get_field_name( 'mobile' ) . '" type="text" value="' . esc_attr( $mobile ) . '" />
		</p>';
		$widget_form_output .= '<p>
			<label for="' . $this->get_field_id( 'fax' ) . '">' . __( 'Fax number :', 'ggt-contact-widget' ) . '</label>
			<input class="widefat" id="' . $this->get_field_id( 'fax' ) . '" name="' . $this->get_field_name( 'fax' ) . '" type="text" value="' . esc_attr( $fax ) . '" />
		</p>';
		$widget_form_output .= '<p>
			<label for="' . $this->get_field_id( 'email' ) . '">' . __( 'Email address :', 'ggt-contact-widget' ) . '</label>
			<input class="widefat" id="' . $this->get_field_id( 'email' ) . '" name="' . $this->get_field_name( 'email' ) . '" type="text" value="' . esc_attr( $email ) . '" />
		</p>';
		$widget_form_output .= '<p>
			<label for="' . $this->get_field_id( 'email_text' ) . '">' . __( 'Email link text :', 'ggt-contact-widget' ) . '</label>
			<input class="widefat" id="' . $this->get_field_id( 'email_text' ) . '" name="' . $this->get_field_name( 'email_text' ) . '" type="text" value="' . esc_attr( $email_text ) . '" />
		</p>';
		$widget_form_output .= '<p>
			<label for="' . $this->get_field_id( 'web' ) . '">' . __( 'Website URL (with HTTP) :', 'ggt-contact-widget' ) . '</label>
			<input class="widefat" id="' . $this->get_field_id( 'web' ) . '" name="' . $this->get_field_name( 'web' ) . '" type="text" value="' . esc_attr( $web ) . '" />
		</p>';
		$widget_form_output .= '<p>
			<label for="' . $this->get_field_id( 'web_text' ) . '">' . __( 'Website URL Text :', 'ggt-contact-widget' ) . '</label>
			<input class="widefat" id="' . $this->get_field_id( 'web_text' ) . '" name="' . $this->get_field_name( 'web_text' ) . '" type="text" value="' . esc_attr( $web_text ) . '" />
		</p>';
		$widget_form_output = apply_filters( 'rc_widget_form_output', $widget_form_output, $instance );
		echo $widget_form_output;
	}

}

?>