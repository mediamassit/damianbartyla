<?php
/*
Plugin Name: Posts Share
*/



function ggt_social_sharing_buttons($content) {
    global $post;
    if(is_single()){

        // Get current page URL
        $ggtURL = urlencode(get_permalink());

        // Get current page title
        $ggtTitle = str_replace( ' ', '%20', get_the_title());

        // Get Post Thumbnail for pinterest
        $ggtThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

        // Construct sharing URL without using any script
        $twitterURL = 'https://twitter.com/intent/tweet?text='.$ggtTitle.'&amp;url='.$ggtURL.'&amp;via=GGT';
        $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$ggtURL;
        $googleURL = 'https://plus.google.com/share?url='.$ggtURL;
        $bufferURL = 'https://bufferapp.com/add?url='.$ggtURL.'&amp;text='.$ggtTitle;
        // $whatsappURL = 'whatsapp://send?text='.$ggtTitle . ' ' . $ggtURL;
        $linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$ggtURL.'&amp;title='.$ggtTitle;

        // Based on popular demand added Pinterest too
        $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$ggtURL.'&amp;media='.$ggtThumbnail[0].'&amp;description='.$ggtTitle;

        // Add sharing button at the end of page/page content
        $content .= '<div class="posts-socials">';
        $content .= '<h5>Share:</h5> <a class="posts-socials-icon" href="'. $twitterURL .'" target="_blank"><i class="socicon socicon-twitter"></i><span>Twitter</span></a>';
        $content .= '<a class="posts-socials-icon" href="'.$facebookURL.'" target="_blank"><i class="socicon socicon-facebook"></i><span>Facebook</span></a>';
        // $content .= '<a class="posts-socials-icon socicon socicon-whatsapp" href="'.$whatsappURL.'" target="_blank"><i class="socicon socicon-twitter"></i><span>WhatsApp</span></a>';
        $content .= '<a class="posts-socials-icon" href="'.$googleURL.'" target="_blank"><i class="socicon socicon-google"></i><span>Google+</span></a>';
        $content .= '<a class="posts-socials-icon" href="'.$bufferURL.'" target="_blank"><i class="socicon socicon-buffer"></i><span>Buffer</span></a>';
        $content .= '<a class="posts-socials-icon" href="'.$linkedInURL.'" target="_blank"><i class="socicon socicon-linkedin"></i><span>LinkedIn</span></a>';
        $content .= '<a class="posts-socials-icon" href="'.$pinterestURL.'" target="_blank"><i class="socicon socicon-pinterest"></i><span>Pinterest</span></a>';
        $content .= '</div>';

        return $content;
    }else{
        // if not a post/page then don't include sharing button
        return $content;
    }
};
add_filter( 'the_content', 'ggt_social_sharing_buttons');

?>
