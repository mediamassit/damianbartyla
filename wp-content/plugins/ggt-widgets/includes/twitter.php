<?php


class ggt_twitter_widget extends WP_Widget {
	
	function ggt_twitter_widget()
	{
		$widget_ops = array('classname' => 'ggt_tweets_widget', 'description' => '');

		$this->WP_Widget('ggt_twitter_widget', 'GGT Social Widgets: Twitter', $widget_ops);
	}
	
	function widget($args, $instance)
	{
	
		  wp_enqueue_style( 'ggt_swidget_extend_style', plugins_url('/../assets/css/custom.css', __FILE__) );
	      wp_enqueue_style( 'ggt_swidget_owlcss', plugins_url('/../assets/css/owl.carousel.css', __FILE__) );
	      wp_enqueue_style( 'ggt_swidget_font-ggtcontrols', plugins_url('/../assets/css/font-ggtcontrols.css', __FILE__) );

	      wp_enqueue_style('thickbox');
	
	      wp_enqueue_script( 'ggt_swidget_waypoint', plugins_url('/../assets/js/waypoints.min.js', __FILE__), array('jquery') );
	      wp_enqueue_script( 'ggt_swidget_extend_js', plugins_url('/../assets/js/custom.js', __FILE__), array('jquery') );
	      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
	      wp_enqueue_script( 'ggt_swidget_jquery-expand', plugins_url('/../assets/js/expand.min.js', __FILE__), array('jquery') );
	
	      wp_enqueue_script( 'ggt_swidget_owljs', plugins_url('/../assets/js/owl.carousel.js', __FILE__), array('jquery') );
	      wp_enqueue_script( 'ggt_swidget_carousel_js', plugins_url('/../assets/js/carousel.js', __FILE__), array('jquery', 'ggt_swidget_owljs') );


		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$consumer_key = esc_attr(trim($instance['consumer_key']));
		$consumer_secret = esc_attr(trim($instance['consumer_secret']));
		$access_token = esc_attr(trim($instance['access_token']));
		$access_token_secret = esc_attr(trim($instance['access_token_secret']));
		$twitter_id = esc_attr(trim($instance['twitter_id']));
		$count = $instance['count'];
		$layout = $instance['layout'];
		$autoplay = !empty($instance['autoplay']) ? 'true' : 'false';

		echo $before_widget;
		
		if($title) {
			echo $before_title . esc_attr($title) . $after_title;
		}
		
		if($twitter_id && $consumer_key && $consumer_secret && $access_token && $access_token_secret && $count) { 
		$transName = 'list_tweets_'.$args['widget_id'];
		$cacheTime = 10;
		delete_transient($transName);
		if(false === ($twitterData = get_transient($transName))) {

		     // require the twitter auth class
		     @require_once 'twitteroauth/twitteroauth.php';
		     $twitterConnection = new TwitterOAuth(
							$consumer_key,	// Consumer Key
							$consumer_secret,   	// Consumer secret
							$access_token,       // Access token
							$access_token_secret    	// Access token secret
							);

		    $twitterData = $twitterConnection->get(
							  'statuses/user_timeline',
							  array(
							    'screen_name'     => $twitter_id,
							    'count'           => $count,
							    'exclude_replies' => false
							  )
							);
		     if($twitterConnection->http_code != 200)
		     {
		          $twitterData = get_transient($transName);
		     }

		     // Save our new transient.
		     set_transient($transName, $twitterData, 60 * $cacheTime);
		};
		$twitter = get_transient($transName);
		
		if($twitter && is_array($twitter)) {
			//var_dump($twitter);
			
			$carousel = ($layout == 'vertical') ? '' : 'class="inowlcarousel" data-autoplay="'.$autoplay.'"';
		?>
		<div class="twitter-box">
			<div class="twitter-holder">
				<div class="b">
					<div class="tweets-container" id="tweets_<?php echo esc_attr($args['widget_id']); ?>">
						<ul id="jtwt" <?php echo $carousel; ?>>
							<?php foreach($twitter as $tweet): ?>
							<li class="jtwt_tweet">
								<p class="jtwt_tweet_text">
								<?php
								$latestTweet = $tweet->text;
								$latestTweet = preg_replace('/http:\/\/([a-z0-9_\.\-\+\&\!\#\~\/\,]+)/i', '&nbsp;<a href="http://$1" target="_blank">http://$1</a>&nbsp;', $latestTweet);
								$latestTweet = preg_replace('/@([a-z0-9_]+)/i', '&nbsp;<a href="http://twitter.com/$1" target="_blank">@$1</a>&nbsp;', $latestTweet);
								echo $latestTweet;
								?>
								</p>
								<?php
								$twitterTime = strtotime($tweet->created_at);
								$timeAgo = $this->ago($twitterTime);
								?>
								<a href="http://twitter.com/<?php echo $tweet->user->screen_name; ?>/statuses/<?php echo $tweet->id_str; ?>" class="jtwt_date"><?php echo $timeAgo; ?></a>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
			<span class="arrow"></span>
		</div>
		<?php }}
		
		echo $after_widget;
	}
	
	function ago($time)
	{
	   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
	   $lengths = array("60","60","24","7","4.35","12","10");

	   $now = time();

	       $difference     = $now - $time;
	       $tense         = "ago";

	   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
	       $difference /= $lengths[$j];
	   }

	   $difference = round($difference);

	   if($difference != 1) {
	       $periods[$j].= "s";
	   }

	   return "$difference $periods[$j] ago ";
	}

	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['consumer_key'] = $new_instance['consumer_key'];
		$instance['consumer_secret'] = $new_instance['consumer_secret'];
		$instance['access_token'] = $new_instance['access_token'];
		$instance['access_token_secret'] = $new_instance['access_token_secret'];
		$instance['twitter_id'] = $new_instance['twitter_id'];
		$instance['count'] = $new_instance['count'];
		$instance['autoplay'] = $new_instance['autoplay'] ? 1 : 0;

		
		if ( in_array( $new_instance['layout'], array( 'vertical', 'carousel' ) ) ) {
			$instance['layout'] = $new_instance['layout'];
		} else {
			$instance['layout'] = 'vertical';
		}

		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Recent Tweets',
            'twitter_id' => '',
            'layout' => 'vertical', 
            'count' => 3,
            'consumer_key' => '',
            'consumer_secret' => '',
            'access_token' => '',
            'access_token_secret' => '',
            'autoplay' => 0,
            
    );
		$instance = wp_parse_args((array) $instance, $defaults); 
		
		$autoplay = $instance['autoplay'] ? 'checked="checked"' : '';

		?>
		
		<p><a href="http://dev.twitter.com/apps">Find or Create your Twitter App</a></p>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		
		<p id="pel<?php echo $this->get_field_id('layout'); ?>" >
			<label for="<?php echo $this->get_field_id('layout'); ?>"><?php _e( 'Widget layout:' ); ?></label>
			<select name="<?php echo $this->get_field_name('layout'); ?>" id="<?php echo $this->get_field_id('layout'); ?>" class="widefat">
				<option value="vertical"<?php selected( $instance['layout'], 'vertical' ); ?>><?php _e('Vertical list'); ?></option>
				<option value="carousel"<?php selected( $instance['layout'], 'carousel' ); ?>><?php _e('Carousel'); ?></option>
			</select>
		</p>

<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'layout' ); ?>:carousel"><label for="<?php echo $this->get_field_id('consumer_key'); ?>"><?php _e('AutoPlay'); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('autoplay'); ?>" <?php echo $autoplay; ?> /></p>

		<p>
			<label for="<?php echo $this->get_field_id('consumer_key'); ?>">Consumer Key:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('consumer_key'); ?>" name="<?php echo $this->get_field_name('consumer_key'); ?>" value="<?php echo $instance['consumer_key']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('consumer_secret'); ?>">Consumer Secret:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('consumer_secret'); ?>" name="<?php echo $this->get_field_name('consumer_secret'); ?>" value="<?php echo $instance['consumer_secret']; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('access_token'); ?>">Access Token:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('access_token'); ?>" name="<?php echo $this->get_field_name('access_token'); ?>" value="<?php echo $instance['access_token']; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('access_token_secret'); ?>">Access Token Secret:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('access_token_secret'); ?>" name="<?php echo $this->get_field_name('access_token_secret'); ?>" value="<?php echo $instance['access_token_secret']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('twitter_id'); ?>">Twitter username, without @:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('twitter_id'); ?>" name="<?php echo $this->get_field_name('twitter_id'); ?>" value="<?php echo $instance['twitter_id']; ?>" />
		</p>

			<label for="<?php echo $this->get_field_id('count'); ?>">Number of Tweets:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>" value="<?php echo $instance['count']; ?>" />
		</p>

	<?php
	}
}
?>