<?php
add_action('widgets_init', 'recent_works_load_widgets');


function recent_works_load_widgets()
{
    register_widget('ggt_recentworks_widget');
}

class ggt_recentworks_widget extends WP_Widget
{

    function ggt_recentworks_widget()
    {
        $widget_ops = array('classname' => 'ggt_recentworks_widget', 'description' => 'Recent works');

        $control_ops = array('id_base' => 'recent_works-widget');

        parent::__construct('recent_works-widget', __('GGT Recent Works'), $widget_ops, $control_ops);
    }

    function widget($args, $instance)
    {
        wp_enqueue_script('gridifier', plugins_url('../assets/js/gridifier.js', __FILE__), array('jquery'));
		wp_enqueue_style( 'ggt_swidget_owlcss', plugins_url('/../assets/css/owl.carousel.css', __FILE__) );
        wp_enqueue_style('ggt_swidget_font-ggtcontrols', plugins_url('/../assets/css/font-ggtcontrols.css', __FILE__));

        wp_enqueue_style('ggt_swidget_extend_style', plugins_url('/../assets/css/custom.css', __FILE__));
        wp_enqueue_style('thickbox');

        wp_enqueue_script('ggt_swidget_waypoint', plugins_url('/../assets/js/waypoints.min.js', __FILE__), array('jquery'));
        wp_enqueue_script('ggt_swidget_extend_js', plugins_url('/../assets/js/custom.js', __FILE__), array('jquery'));
        wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
        wp_enqueue_script('ggt_swidget_jquery-expand', plugins_url('/../assets/js/expand.min.js', __FILE__), array('jquery'));
	    wp_enqueue_script( 'ggt_swidget_owljs', plugins_url('/../assets/js/owl.carousel.js', __FILE__), array('jquery') );
	    wp_enqueue_script( 'ggt_swidget_carousel_js', plugins_url('/../assets/js/carousel.js', __FILE__), array('jquery', 'ggt_swidget_owljs') );
	    
        extract($args);

        $title = apply_filters('widget_title', $instance['title']);
        $number = $instance['number'];
        $post_types = (!empty($instance['post_types'])) ? $instance['post_types'] : array();
        $category = $instance['category'];
        $order_by = (!empty($instance['order_by'])) ? esc_attr($instance['order_by']) : 'date';
        $order_by_dir = (!empty($instance['order_by_dir'])) ? esc_attr($instance['order_by_dir']) : 'DESC';
        $custom_image_border_radius = (!empty($instance['custom_image_border_radius'])) ? esc_attr($instance['custom_image_border_radius']) : '';
        $css_classes = (!empty($instance['css_classes'])) ? esc_attr($instance['css_classes']) : '';

        $type = $instance['type'];
        $columns = $instance['columns'];
/*         $slider_layout = $instance['slider_layout']; */
        $autoplay     = ( ! empty( $instance['autoplay'] )) ? (bool) $instance['autoplay'] : false;
        $scrolltimeout     = ( ! empty( $instance['scrolltimeout'] )) ? (int) $instance['scrolltimeout'] : 3000;
		$hide_nav_bttns     = ( ! empty( $instance['hide_nav_bttns'] )) ? (bool) $instance['hide_nav_bttns'] : false;

		$prev_bttn     = ( ! empty( $instance['prev_bttn'] )) ? esc_attr( $instance['prev_bttn'] ) : '';
		$next_bttn     = ( ! empty( $instance['next_bttn'] )) ? esc_attr( $instance['next_bttn'] ) : '';

        $uid = 'works-' . uniqid();

        $box_sizes = array(11, 12, 21, 22);

        echo $before_widget;

        if ($title) {
            echo $before_title . $title . $after_title;
        }

        $add_att = $liclass = '';

        $add_class_att = $css_classes;

        $mbefore = '<div id="' . $uid . '" class="grid-cont">';
        $mafter = '</div>';

        if ($type == 'metro') {

            $add_class_att .= ' gridifier metro columns' . $columns;

            $add_att = 'data-type="metro" data-show-type="link" data-cols="' . $columns . '" data-load-type="static" data-init-rows="1" data-categories=""';
            $mbefore = '<div id="' . $uid . '" class="grid-wrap">';
            $mafter = '</div>';

            $liclass = 'grid-item port-block project-image';

            ?>
            <script>

                (function($){
                    $(document).ready(function(){
                        $("<?php echo "#" . $uid; ?>").gridifier({
                            itemClass: ".grid-item",
                            layout: "metro",
                            positionSpeed: 700,
                            animationSpeed: 700,
                            filterOutAnimation: "flipOutX", // animate.css
                            filterInAnimation: "flipInX", // animate.css
                            columns: <?php echo $columns; ?>
                        });
                    })
                })(jQuery);
            </script>


            <?php
        }
        $width = get_option('thumbnail' . '_size_w');
        $height = get_option('thumbnail' . '_size_h');


        if ($type == 'slider') {

            $add_class_att .= ' inowlcarousel';

            $add_att = ' data-navtext="'.($hide_nav_bttns ? 1 : 0).'"'.($autoplay ? ' data-autoplay="1" data-scroll-timeout="'.$scrolltimeout.'"' : '').(($next_bttn != '') && ($prev_bttn != '') ? ' data-navprev="'.$prev_bttn.'" data-navnext="'.$next_bttn.'"' : '').'';

            $mbefore = '<div id="' . $uid . '">';
            $mafter = '</div>';

        }


        ?>

        <?php if ($custom_image_border_radius != '') { ?>
        <style>
            #<?php echo $uid; ?>
            .ggtpost-thumb,

            #<?php echo $uid; ?>
            .ggtpost-thumb img {
                -moz-border-radius: <?php echo $custom_image_border_radius; ?>;
                -webkit-border-radius: <?php echo $custom_image_border_radius; ?>;
                -khtml-border-radius: <?php echo $custom_image_border_radius; ?>;
                border-radius: <?php echo $custom_image_border_radius; ?>;
                border-width: 1px;
            }
        </style>
    <?php } ?>

        <?php if ($type == 'grid') { ?>
        <script>
            (function ($) {
                $(document).ready(function ($) {
                    var items = $('<?php echo "#" . $uid; ?> .ggt-recent-works-items li'),
                        columns = <?php echo $columns; ?>;
                        
                        var width = 100 / columns+'%';
                        
                        items.width(width);
                        
                });
            })(jQuery);
        </script>

    <?php } ?>

        <?php echo $mbefore; ?>
        <ul class="ggt-recent-works-items <?php echo $add_class_att; ?>" <?php echo $add_att; ?>>
            <?php


            $args = array(
                'posts_per_page' => $number,
                'has_password' => false,
                'meta_key' => '_thumbnail_id',
                'post_status' => 'publish',
                'orderby' => $order_by,
                'order' => $order_by_dir,

            );

            $cat = array();

            if ($category == '0') {
                $args['post_type'] = $post_types;
            } else {

                $cat = json_decode($category);

                $args['tax_query'] = array(
                    array(
                        'taxonomy' => $cat->taxonomy,
                        'field' => 'slug',
                        'terms' => array($cat->slug),
                        'operator' => 'IN',
                        'include_children' => false,
                    )
                );
            }

            $width = get_option('thumbnail' . '_size_w');
            $height = get_option('thumbnail' . '_size_h');


            $portfolio = new WP_Query($args);


            /* print_r('<pre>');print_r($args);print_r('</pre>'); */
            if ($portfolio->have_posts()):
                ?>
                <?php while ($portfolio->have_posts()): $portfolio->the_post(); ?>
                <?php

                $link_target = "";
                $url_check = get_post_meta(get_the_ID(), 'pyre_link_icon_url', true);
                if (!empty($url_check)) {
                    $new_permalink = get_post_meta(get_the_ID(), 'pyre_link_icon_url', true);
                    if (get_post_meta(get_the_ID(), 'pyre_link_icon_target', true) == "yes") {
                        $link_target = ' target="_blank"';
                    }
                } else {
                    $new_permalink = get_permalink();
                }


                $box_size = (function_exists("get_field") && get_field('box_size')) ? get_field('box_size') : $box_sizes[array_rand($box_sizes)]; //
                $img_size = ($type == 'slider') ? 'medium' : 'medium'; //full
                $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), $img_size);
                /* 		print_r($img); */
                ?>
                <li class="<?php echo $liclass; ?>" data-post-id="<?php echo get_the_ID(); ?>" data-categories="{}"
                    data-box-size="<?php echo $box_size; ?>">

                    <?php if ($type == 'metro') { ?>
                        <div class="grid-inner">
                            <div class="grid-border">
                                <a href="<?php echo $new_permalink; ?>"<?php echo $link_target; ?>
                                   title="<?php the_title(); ?>">
                                    <?php if (isset($img[0])) { ?>
                                    <div class="ggtpost-thumb"
                                         style="background-image: url(<?php echo $img[0]; ?>);"></div><?php } ?>
                                </a>
                            </div>
                        </div>
                    <?php } else { ?>

                        <a href="<?php echo $new_permalink; ?>"<?php echo $link_target; ?>
                           title="<?php the_title(); ?>">
                            <?php if (isset($img[0])) { ?>
                                <div class="ggtpost-thumb">
                              <?php if ($type == 'slider') { ?>
                               <?php /* <img class="lazyOwl" src="<?php echo plugins_url('/../assets/images/blank.png', __FILE__); ?>" data-original="<?php echo $img[0]; ?>" alt="<?php the_title(); ?>"> */ ?>
                                <img src="<?php echo $img[0]; ?>" alt="<?php the_title(); ?>">
                                <?php } else { ?>
                                <img src="<?php echo $img[0]; ?>" alt="<?php the_title(); ?>">
                                <?php } ?>
                                </div><?php } ?>

                        </a>
                    <?php } ?>

                </li>
            <?php  endwhile;

                wp_reset_postdata();

            endif; ?>
        </ul>
        <?php echo $mafter; ?>
        <?php echo $after_widget;
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = $new_instance['number'];
        $instance['category'] = $new_instance['category'];
        $instance['post_types'] = $new_instance['post_types'];
        $instance['order_by'] = strip_tags($new_instance['order_by']);
        $instance['order_by_dir'] = strip_tags($new_instance['order_by_dir']);
        $instance['css_classes'] = strip_tags($new_instance['css_classes']);
        $instance['type'] = $new_instance['type'];
        $instance['columns'] = $new_instance['columns'];
        $instance['slider_layout'] = $new_instance['slider_layout'];
        $instance['autoplay'] = isset( $new_instance['autoplay'] ) ? (bool) $new_instance['autoplay'] : false;
        $instance['scrolltimeout'] = isset( $new_instance['scrolltimeout'] ) ? (int) $new_instance['scrolltimeout'] : 3000;
		$instance['hide_nav_bttns']     = isset( $new_instance['hide_nav_bttns'] ) ? (bool) $new_instance['hide_nav_bttns'] : false;
		$instance['prev_bttn'] = strip_tags($new_instance['prev_bttn']);
		$instance['next_bttn'] = strip_tags($new_instance['next_bttn']);

        $instance['custom_image_border_radius'] = strip_tags($new_instance['custom_image_border_radius']);
        return $instance;
    }

    function form($instance)
    {
        $defaults = array('title' => 'GGT Recent Works', 'number' => 6, 'category' => 0, 'order_by' => 'date', 'order_by_dir' => 'DESC', 'custom_image_border_radius' => '', 'css_classes' => '', 'post_types' => array(), 'type' => 'grid', 'columns' => 2, 'slider_layout' => 'horizontal', 'autoplay' => 0,);
        $order_by = array('date', 'id', 'author', 'title', 'modified', 'rand', 'comment count', 'order');
        $instance = wp_parse_args((array)$instance, $defaults);
        $autoplay     = isset( $instance['autoplay'] ) ? (bool) $instance['autoplay'] : false;
        
        $scrolltimeout     = isset( $instance['scrolltimeout'] ) ? (int) $instance['scrolltimeout'] : 3000;
		$hide_nav_bttns     = isset( $instance['hide_nav_bttns'] ) ? (bool) $instance['hide_nav_bttns'] : false;

		$prev_bttn     = isset( $instance['prev_bttn'] ) ? esc_attr( $instance['prev_bttn'] ) : '';
		$next_bttn     = isset( $instance['next_bttn'] ) ? esc_attr( $instance['next_bttn'] ) : '';

         ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
            <input class="widefat" type="text" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>"/>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('type'); ?>"><?php _e('Select type:'); ?></label>
            <select name="<?php echo $this->get_field_name('type'); ?>" id="<?php echo $this->get_field_id('type'); ?>"
                    class="widefat">
                <option value="metro" <?php selected($instance['type'], 'metro'); ?>><?php _e('Metro'); ?></option>
                <option value="grid" <?php selected($instance['type'], 'grid'); ?>><?php _e('Grid'); ?></option>
                <option value="slider" <?php selected($instance['type'], 'slider'); ?>><?php _e('Slider'); ?></option>
            </select>
        </p>

        <p class="dependence" data-dependence="<?php echo $this->get_field_id('type'); ?>:metro,grid">
            <label for="<?php echo $this->get_field_id('columns'); ?>"><?php _e('Columns:'); ?></label>
            <select name="<?php echo $this->get_field_name('columns'); ?>"
                    id="<?php echo $this->get_field_id('columns'); ?>" class="widefat">
                <option value="2" <?php selected($instance['columns'], 2); ?>><?php _e('2 columns'); ?></option>
                <option value="3" <?php selected($instance['columns'], 3); ?>><?php _e('3 columns'); ?></option>
                <option value="4" <?php selected($instance['columns'], 4); ?>><?php _e('4 columns'); ?></option>
            </select>
        </p>
        
        <p class="dependence" data-dependence="<?php echo $this->get_field_id( 'type' ); ?>:slider"><label for="<?php echo $this->get_field_id( 'autoplay' ); ?>"><?php _e('AutoPlay?', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'autoplay' ); ?>" name="<?php echo $this->get_field_name('autoplay'); ?>" <?php checked( $autoplay ); ?> /></p>

		<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'type' ); ?>:slider;<?php echo $this->get_field_id( 'autoplay' ); ?>:true"><label for="<?php echo $this->get_field_id( 'scrolltimeout' ); ?>"><?php _e('Slider auto-scroll timeout', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'scrolltimeout' ); ?>" name="<?php echo $this->get_field_name( 'scrolltimeout' ); ?>" type="text" value="<?php echo $scrolltimeout; ?>" /></p>

		<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'type' ); ?>:slider"><label for="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>"><?php _e('Hide Prev/Next buttons', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>" name="<?php echo $this->get_field_name('hide_nav_bttns'); ?>" <?php checked( $hide_nav_bttns ); ?> /></p>

<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>:false;<?php echo $this->get_field_id( 'type' ); ?>:slider">
            <label for="<?php echo $this->get_field_name( 'prev_bttn' ); ?>"><?php _e( 'Prev Button Image:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'prev_bttn' ); ?>" id="<?php echo $this->get_field_id( 'prev_bttn' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $prev_bttn ); ?>" />
            <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
        </p>


<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>:false;<?php echo $this->get_field_id( 'type' ); ?>:slider">
            <label for="<?php echo $this->get_field_name( 'next_bttn' ); ?>"><?php _e( 'Next Button Image:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'next_bttn' ); ?>" id="<?php echo $this->get_field_id( 'next_bttn' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $next_bttn ); ?>" />
            <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
        </p>


<!--
        <p class="dependence" data-dependence="<?php echo $this->get_field_id('type'); ?>:slider">
            <label for="<?php echo $this->get_field_id('slider_layout'); ?>"><?php _e('Slider layout:'); ?></label>
            <select name="<?php echo $this->get_field_name('slider_layout'); ?>"
                    id="<?php echo $this->get_field_id('slider_layout'); ?>" class="widefat">
                <option
                    value="vertical" <?php selected($instance['slider_layout'], 'vertical'); ?>><?php _e('Vertical'); ?></option>
                <option
                    value="horizontal" <?php selected($instance['slider_layout'], 'horizontal'); ?>><?php _e('Horizontal'); ?></option>
            </select>
        </p>
-->


        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>">Number to display:</label>
            <input class="widefat" type="text" style="width: 30px;" id="<?php echo $this->get_field_id('number'); ?>"
                   name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $instance['number']; ?>"/>
        </p>

        <?php

        $args = array(
            'public' => true,
            '_builtin' => true
        );

        $output = 'names'; // names or objects, note names is the default
        $operator = 'or'; // 'and' or 'or'

        $post_types = get_post_types($args, $output, $operator);

        $newarr = array();

        foreach ($post_types as $post_type) {
            $ntaxonomies = array();
            $ntaxonomies = get_object_taxonomies($post_type, 'names');
            $newarr[$post_type] = get_categories(array('orderby' => 'taxonomy', 'taxonomy' => $ntaxonomies));
        }

        $jcategories = json_encode($newarr);

        ?>

        <script>
            (function ($) {

                var jcategories = <?php echo $jcategories; ?>

                    $(document).ready(function () {
                        if ($.selcat && typeof $.selcat === 'function') {
                            $.selcat(jcategories);
                        }

                        $('.clposttype').change(function () {
                            if ($.selcat && typeof $.selcat === 'function') {
                                $.selcat(jcategories, $(this));
                            }
                        });
                    });

                $(document).ajaxComplete(function () {
                    if ($.selcat && typeof $.selcat === 'function') {
                        $.selcat(jcategories);
                    }

                    $('.clposttype').change(function () {
                        if ($.selcat && typeof $.selcat === 'function') {
                            $.selcat(jcategories, $(this));
                        }
                    });
                });


                $('.clposttype').change(function () {
                    if ($.selcat && typeof $.selcat === 'function') {
                        $.selcat(jcategories, $(this));
                    }
                });
            })(jQuery);
        </script>
        <div class="buildq">
            <p class=""><label
                    for="<?php echo $this->get_field_id('post_types'); ?>"><?php _e('Select post types:'); ?></label>

                <br>
                <?php
                foreach ($post_types as $post_type) {
                    ?>
                    <input id="<?php echo $this->get_field_id('post_types'); ?>" class="checkbox clposttype"
                           type="checkbox" value="<?php echo $post_type; ?>"
                           name="<?php echo $this->get_field_name('post_types'); ?>[]" <?php if (isset($instance['post_types'])) echo (in_array($post_type, $instance['post_types'])) ? 'checked="checked"' : ''; ?> /><?php _e($post_type, ''); ?>
                <?php } ?>
            </p>

            <p class=""><label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category:'); ?></label>

                <select name="<?php echo $this->get_field_name('category'); ?>"
                        data-defval='<?php echo $instance['category']; ?>'
                        id="<?php echo $this->get_field_id('category'); ?>" class="widefat depcats">
                    <option value="0">All</option>
                </select>

            </p>
        </div>
        <p>
            <label for="<?php echo $this->get_field_id('order_by'); ?>"><?php _e('Order by:'); ?></label>
            <select name="<?php echo $this->get_field_name('order_by'); ?>"
                    id="<?php echo $this->get_field_id('order_by'); ?>" class="widefat">
                <?php foreach ($order_by as $order_by_el) { ?>
                    <option
                        value="<?php echo $order_by_el; ?>" <?php selected($instance['order_by'], $order_by_el); ?>><?php _e($order_by_el); ?></option>
                <?php } ?>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('order_by_dir'); ?>"><?php _e('Order direction:'); ?></label>
            <select name="<?php echo $this->get_field_name('order_by_dir'); ?>"
                    id="<?php echo $this->get_field_id('order_by_dir'); ?>" class="widefat">
                <option value="ASC" <?php selected($instance['order_by_dir'], 'ASC'); ?>><?php _e('ASC'); ?></option>
                <option value="DESC" <?php selected($instance['order_by_dir'], 'DESC'); ?>><?php _e('DESC'); ?></option>
            </select>
        </p>
        <p><label
                for="<?php echo $this->get_field_id('custom_image_border_radius'); ?>"><?php _e('Image Border radius', ''); ?>
                : </label><input class="widefat" id="<?php echo $this->get_field_id('custom_image_border_radius'); ?>"
                                 name="<?php echo $this->get_field_name('custom_image_border_radius'); ?>" type="text"
                                 value="<?php echo $instance['custom_image_border_radius']; ?>"/></p>


        <p><label for="<?php echo $this->get_field_id('css_classes'); ?>"><?php _e('CSS classes', ''); ?>
                : </label><input class="widefat" id="<?php echo $this->get_field_id('css_classes'); ?>"
                                 name="<?php echo $this->get_field_name('css_classes'); ?>" type="text"
                                 value="<?php echo $instance['css_classes']; ?>"/></p>


    <?php
    }
}

?>