<?php

class ggt_recentposts_widget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'ggt_recentposts_widget_entries', 'description' => __( "") );
		parent::__construct('ggt_recentposts_widget', __('GGT Recent Posts'), $widget_ops);
		$this->alt_option_name = 'ggt_recentposts_widget_entries';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
		add_action( 'wp_enqueue_scripts', array($this, 'addstyle') );

	}

	function addstyle() {
	  wp_enqueue_style( 'recentbox_style', plugins_url('../assets/css/recentbox.css', __FILE__ ));
	}


	function widget($args, $instance) {
		
      wp_enqueue_style( 'ggt_swidget_font-ggtcontrols', plugins_url('/../assets/css/font-ggtcontrols.css', __FILE__) );
      wp_enqueue_style( 'ggt_swidget_owlcss', plugins_url('/../assets/css/owl.carousel.css', __FILE__) );

      
      	  wp_enqueue_style( 'ggt_swidget_extend_style', plugins_url('/../assets/css/custom.css', __FILE__) );
	      wp_enqueue_style('thickbox');
	
	      wp_enqueue_script( 'ggt_swidget_extend_js', plugins_url('/../assets/js/custom.js', __FILE__), array('jquery') );
	      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
	      wp_enqueue_script( 'ggt_swidget_jquery-expand', plugins_url('/../assets/js/expand.min.js', __FILE__), array('jquery') );
	      wp_enqueue_script( 'ggt_swidget_owljs', plugins_url('/../assets/js/owl.carousel.js', __FILE__), array('jquery') );
	      wp_enqueue_script( 'ggt_swidget_carousel_js', plugins_url('/../assets/js/carousel.js', __FILE__), array('jquery', 'ggt_swidget_owljs') );


		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'ggt_recentposts_widget', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts' );

		/** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', $instance['title']);
				
		$number = ( ! empty( $instance['number'] )) ? absint( $instance['number'] ) : 5;
		
		$category     = ( ! empty( $instance['category'] )) ? (int) $instance['category'] : 0;
		
		
		$order_by = ( ! empty( $instance['order_by'] )) ? esc_attr( $instance['order_by']) : 'date';
		$order_by_dir = ( ! empty( $instance['order_by_dir'] )) ? esc_attr( $instance['order_by_dir']) : 'DESC';


		$slider     = ( ! empty( $instance['slider'] )) ? (bool) $instance['slider'] : false;
		$autoplay     = ( ! empty( $instance['autoplay'] )) ? (bool) $instance['autoplay'] : false;
		$scrolltimeout     = ( ! empty( $instance['scrolltimeout'] )) ? (int) $instance['scrolltimeout'] : 3000;
		$hide_nav_bttns     = ( ! empty( $instance['hide_nav_bttns'] )) ? (bool) $instance['hide_nav_bttns'] : false;

		$prev_bttn     = ( ! empty( $instance['prev_bttn'] )) ? esc_attr( $instance['prev_bttn'] ) : '';
		$next_bttn     = ( ! empty( $instance['next_bttn'] )) ? esc_attr( $instance['next_bttn'] ) : '';
		
		$widget_type     = ( ! empty( $instance['widget_type'] )) ? esc_attr( $instance['widget_type'] ) : 'vertical';
		$image_position     = ( ! empty( $instance['image_position'] )) ? esc_attr( $instance['image_position'] ) : 'left';
		$columns     = ( ! empty( $instance['columns'] )) ? absint( $instance['columns'] ) : 2;
		$show_title     = ( ! empty( $instance['show_title'] )) ? (bool) $instance['show_title'] : false;
		$show_thumb     = ( ! empty( $instance['show_thumb'] )) ? (bool) $instance['show_thumb'] : false;
		$show_date = ( ! empty( $instance['show_date'] )) ? (bool) $instance['show_date'] : false;
		$show_author     = ( ! empty( $instance['show_author'] )) ? (bool) $instance['show_author'] : false;
		$show_excerpt     = ( ! empty( $instance['show_excerpt'] )) ? (bool) $instance['show_excerpt'] : false;
		$excerpt_size     = ( ! empty( $instance['excerpt_size'] )) ? absint( $instance['excerpt_size'] ) : 150;
		$show_readmore     = ( ! empty( $instance['show_readmore'] )) ? (bool) $instance['show_readmore'] : false;
		$readmore_text     = ( ! empty( $instance['readmore_text'] )) ? esc_attr( $instance['readmore_text'] ) : 'More';
		$css_classes     = ( ! empty( $instance['css_classes'] )) ? esc_attr( $instance['css_classes'] ) : '';
		$title_color     = ( ! empty( $instance['title_color'] )) ? esc_attr( $instance['title_color'] ) : '';


		$skin = ( ! empty( $instance['skin'] )) ? esc_attr( $instance['skin'] ) : ''; 
		$custom_bg_color     = ( ! empty( $instance['custom_bg_color'] )) ? esc_attr( $instance['custom_bg_color'] ) : '';
		$custom_items_bg_color     = ( ! empty( $instance['custom_items_bg_color'] )) ? esc_attr( $instance['custom_items_bg_color'] ) : '';
		$custom_items_title_color     = ( ! empty( $instance['custom_items_title_color'] )) ? esc_attr( $instance['custom_items_title_color'] ) : '';
		$custom_items_title_hover_color = ( ! empty( $instance['custom_items_title_hover_color'] )) ? esc_attr( $instance['custom_items_title_hover_color'] ) : '';
		$custom_items_title_font_size     = ( ! empty( $instance['custom_items_title_font_size'] )) ? esc_attr( $instance['custom_items_title_font_size'] ) : '';
		$custom_items_title_font     = ( ! empty( $instance['custom_items_title_font'] )) ? esc_attr( $instance['custom_items_title_font'] ) : '';
		$custom_items_text_color     = ( ! empty( $instance['custom_items_text_color'] )) ? esc_attr( $instance['custom_items_text_color'] ) : '';
		$custom_items_text_font_size     = ( ! empty( $instance['custom_items_text_font_size'] )) ? esc_attr( $instance['custom_items_text_font_size'] ) : '';
		$custom_items_text_font_family   = ( ! empty( $instance['custom_items_text_font_family'] )) ? esc_attr( $instance['custom_items_text_font_family'] ) : '';
		$custom_items_bttn_bg_color     = ( ! empty( $instance['custom_items_bttn_bg_color'] )) ? esc_attr( $instance['custom_items_bttn_bg_color'] ) : '';
		$custom_items_bttn_hover_bg_color = ( ! empty( $instance['custom_items_bttn_hover_bg_color'] )) ? esc_attr( $instance['custom_items_bttn_hover_bg_color'] ) : '';
		$custom_items_bttn_link_color     = ( ! empty( $instance['custom_items_bttn_link_color'] )) ? esc_attr( $instance['custom_items_bttn_link_color'] ) : '';
		$custom_items_bttn_link_hover_color = ( ! empty( $instance['custom_items_bttn_link_hover_color'] )) ? esc_attr( $instance['custom_items_bttn_link_hover_color'] ) : '';
		$custom_items_border_style     = ( ! empty( $instance['custom_items_border_style'] )) ? esc_attr( $instance['custom_items_border_style'] ) : '';
		
		$slider_item_w     = ( ! empty( $instance['slider_item_w'] )) ? esc_attr( $instance['slider_item_w'] ) : '240';
		$slider_item_h     = ( ! empty( $instance['slider_item_h'] )) ? esc_attr( $instance['slider_item_h'] ) : '125';
		
		$custom_items_border_radius     = ( ! empty( $instance['custom_items_border_radius'] )) ? esc_attr( $instance['custom_items_border_radius'] ) : '';
		$custom_items_bttn_border_radius     = ( ! empty( $instance['custom_items_bttn_border_radius'] )) ? esc_attr( $instance['custom_items_bttn_border_radius'] ) : '';
		$custom_image_border_radius     = ( ! empty( $instance['custom_image_border_radius'] )) ? esc_attr( $instance['custom_image_border_radius'] ) : '';

		$tax_query = array ('relation' => 'AND');

		if ( ! empty( $instance['post_format'] )) {
			$post_format =  (esc_attr( $instance['post_format']) == 'All') ? '' : array_push($tax_query , array('taxonomy' => 'post_format',
									'field' => 'slug',
									'terms' => 'post-format-'.esc_attr( $instance['post_format'])));
		}

		if (( ! empty( $instance['taxterms'] )) && (esc_attr($instance['taxterms']) != '')) {
		
			$taxterms = array_map('trim', explode(',', esc_attr($instance['taxterms'])));
			
			array_push($tax_query , array('taxonomy' => 'post_tag','field' => 'name','terms' => $taxterms));
		}
		
		 
		/**
		 * Filter the arguments for the Recent Posts widget.
		 *
		 * @since 3.4.0
		 *
		 * @see WP_Query::get_posts()
		 *
		 * @param array $args An array of arguments used to retrieve the recent posts.
		 */
		 
	
			$r = new WP_Query( apply_filters( 'widget_posts_args', array(
				'post_type' => 'post',
				'cat'      => $category,
				'posts_per_page'      => $number,
				'no_found_rows'       => true,
				'post_status'         => 'publish',
				'ignore_sticky_posts' => true,
				'orderby' => $order_by,
				'order' => $order_by_dir,
				'tax_query' => $tax_query,
			) ) );
		
		$uid = 'posts-'.uniqid();
		

		$add_class_att = $add_att = $mbefore_widget = $mafter_widget = $add_li_class_att = '';
		
		$nc = 0;
		
		$columnwidth = 1;
		
		echo $before_widget;

			//	print_r('<pre>');print_r($comments);print_r('</pre>');
		if ($r->have_posts()) :
		
	        if ($title) {
	            echo $before_title.'<span class="widget-title-ccolor"'.($title_color != '' ? ' style="color:'.$title_color.';"' : '').'>'. $title .'</span>'. $after_title;
	        }

		$add_class_att .= ' '.$css_classes.' '.$skin.''; 

		if ($slider) {

			$add_class_att .= ' inowlcarousel';
												
			$add_att = ' data-navtext="'.($hide_nav_bttns ? 1 : 0).'" data-columns="'.$columns.'" '.($autoplay ? 'data-autoplay="1" data-scroll-timeout="'.$scrolltimeout.'"' : '').(($next_bttn != '') && ($prev_bttn != '') ? ' data-navprev="'.$prev_bttn.'" data-navnext="'.$next_bttn.'"' : '').'';
						
			$mbefore_widget = '<div id="' . $uid . '">';
			$mafter_widget = '</div>';
			
		}
		
		if ($skin == 'custom') { ?>
			<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery('#<?php echo $uid; ?>').css({
					'background' : '<?php echo $custom_bg_color; ?>',
					});
				jQuery('#<?php echo $uid; ?> li').css({
					'background-color' : '<?php echo $custom_items_bg_color; ?>',
					'border' : '<?php echo $custom_items_border_style; ?>',
				    'border-radius' : '<?php echo $custom_items_border_radius; ?>',
				    'border-width' : '1px',
					'color' : '<?php echo $custom_items_text_color; ?>',
					'font-size' : '<?php echo $custom_items_text_font_size; ?>',
					'font-family' : '<?php echo $custom_items_text_font_family; ?>',
					});
					
				jQuery('#<?php echo $uid; ?> .post-title').css({
					'color' : '<?php echo $custom_items_title_color; ?>',
					'font-size' : '<?php echo $custom_items_title_font_size; ?>',
				    'font-family' : '<?php echo $custom_items_title_font; ?>',
					});
				jQuery('#<?php echo $uid; ?> .post-title').hover(function(){
					jQuery(this).css({
						'color' : '<?php echo $custom_items_title_hover_color; ?>',
						'font-size' : '<?php echo $custom_items_title_font_size; ?>',
					    'font-family' : '<?php echo $custom_items_title_font; ?>',});
					 },function(){
					jQuery(this).css({
						'color' : '<?php echo $custom_items_title_color; ?>',
						'font-size' : '<?php echo $custom_items_title_font_size; ?>',
					    'font-family' : '<?php echo $custom_items_title_font; ?>',});
				  	});
				jQuery('#<?php echo $uid; ?> .post-readmore').css({
					'color' : '<?php echo $custom_items_bttn_link_color; ?>',
					'background' : '<?php echo $custom_items_bttn_bg_color; ?>',
				    'border-radius' : '<?php echo $custom_items_bttn_border_radius; ?>',
				    'border-width' : '1px',
					});
				jQuery('#<?php echo $uid; ?> .post-readmore').hover(function(){
					jQuery(this).css({
						'color' : '<?php echo $custom_items_bttn_link_hover_color; ?>',
						'background' : '<?php echo $custom_items_bttn_hover_bg_color; ?>',});
					 },function(){
					jQuery(this).css({
						'color' : '<?php echo $custom_items_bttn_link_color; ?>',
						'background' : '<?php echo $custom_items_bttn_bg_color; ?>',});
				  	});
				jQuery('#<?php echo $uid; ?> li a').css({
					'color' : '<?php echo $custom_items_text_color; ?>',
					'font-size' : '<?php echo $custom_items_text_font_size; ?>',
					'font-family' : '<?php echo $custom_items_text_font_family; ?>',
					});
				jQuery('#<?php echo $uid; ?> .ggtpost-thumb img').css({
				    'border-radius' : '<?php echo $custom_image_border_radius; ?>',
					});
			});
			</script>
			
		<?php }
		
		
		
			$add_class_att .= ' cols-'.$columns.' direction-'.$widget_type.'';	
			
			$posts_count = count($r->posts);
			
			$columnwidth = ((int)$columns > $posts_count) ? (int)$posts_count : (int)$columns;	
			
/* 						print_r('<pre>');print_r($posts_count);print_r('</pre>'); */

?>
				
			<div class="ggt_recentposts_widget_entries_id">
			
		<?php echo $mbefore_widget; ?>

		<ul id="<?php echo $uid; ?>" class="recent-box-cont<?php echo $add_class_att; ?>" <?php echo $add_att; ?>>
<?php           
            while ( $r->have_posts() ) : $r->the_post(); ?>
                <li style="<?php if (!$slider) {echo 'width:'.(100/$columnwidth).'%';}?>">
                <a href="<?php the_permalink(); ?>">
                <?php if ( $show_thumb ) : ?><div class="ggtpost-thumb <?php echo 'position-'.$image_position; ?>"><?php echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' ); ?></span><?php endif; ?>
                <?php if ( $show_title ) : ?><div class="post-title"><?php get_the_title() ? the_title() : the_ID(); ?></div><?php endif; ?>
                <?php if ( $show_date ) : ?><div class="post-date"><?php echo get_the_date(); ?></div><?php endif; ?>
                <?php if ( $show_author ) : ?><div class="post-author"><?php echo get_the_author(); ?></div><?php endif; ?>
                <?php if ( $show_excerpt ) : ?><div class="post-excerpt"><?php echo  substr(get_the_content(), 0, $excerpt_size);?></div><?php endif; ?>
                <?php if ( $show_readmore ) : ?><div class="post-readmore"><?php echo  $readmore_text; ?></div><?php endif; ?>
                
                
                </a>
                </li>
                <?php  // if($widget_type == 'vertical' && !$slider) echo '<div class="clearfix"></div>'; ?>

            <?php endwhile; 
            
            wp_reset_postdata();

	            
	           
            ?>
		</ul>
		
		<?php echo $mafter_widget; ?>
		
		</div>
		
		<?php endif; ?>
		
		<?php echo $after_widget; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'ggt_recentposts_widget', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);

		$instance['number'] = (int) $new_instance['number'];
		$instance['category'] = (int) $new_instance['category'];
		$instance['order_by_dir'] = strip_tags( $new_instance['order_by_dir'] );

		$instance['post_format'] = strip_tags($new_instance['post_format']);
		$instance['order_by'] = strip_tags($new_instance['order_by']);
		$instance['taxterms'] = strip_tags($new_instance['taxterms']);
		
		$instance['slider'] = isset( $new_instance['slider'] ) ? (bool) $new_instance['slider'] : false;
		$instance['autoplay'] = isset( $new_instance['autoplay'] ) ? (bool) $new_instance['autoplay'] : false;
		$instance['scrolltimeout'] = isset( $new_instance['scrolltimeout'] ) ? (int) $new_instance['scrolltimeout'] : 3000;
		$instance['hide_nav_bttns']     = isset( $new_instance['hide_nav_bttns'] ) ? (bool) $new_instance['hide_nav_bttns'] : false;
		$instance['prev_bttn'] = strip_tags($new_instance['prev_bttn']);
		$instance['next_bttn'] = strip_tags($new_instance['next_bttn']);
		$instance['title_color'] = strip_tags($new_instance['title_color']);

		$instance['widget_type'] = strip_tags($new_instance['widget_type']);
		$instance['image_position'] = strip_tags($new_instance['image_position']);
		$instance['columns'] = (int) $new_instance['columns'];
		$instance['show_title']     = isset( $new_instance['show_title'] ) ? (bool) $new_instance['show_title'] : false;
		$instance['show_thumb']     = isset( $new_instance['show_thumb'] ) ? (bool) $new_instance['show_thumb'] : false;
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		$instance['show_author'] = isset( $new_instance['show_author'] ) ? (bool) $new_instance['show_author'] : false;
		$instance['show_excerpt'] = isset( $new_instance['show_excerpt'] ) ? (bool) $new_instance['show_excerpt'] : false;
		$instance['excerpt_size'] = (int) $new_instance['excerpt_size'];
		$instance['show_readmore'] = isset( $new_instance['show_readmore'] ) ? (bool) $new_instance['show_readmore'] : false;
		$instance['readmore_text'] = strip_tags($new_instance['readmore_text']);
		$instance['css_classes'] = strip_tags($new_instance['css_classes']);
		$instance['skin'] = strip_tags($new_instance['skin']);
		$instance['custom_bg_color'] = strip_tags($new_instance['custom_bg_color']);
		$instance['custom_items_bg_color'] = strip_tags($new_instance['custom_items_bg_color']);
		$instance['custom_items_title_color'] = strip_tags($new_instance['custom_items_title_color']);
		$instance['custom_items_title_hover_color'] = strip_tags($new_instance['custom_items_title_hover_color']);
		$instance['custom_items_title_font_size'] = strip_tags($new_instance['custom_items_title_font_size']);
		$instance['custom_items_title_font'] = strip_tags($new_instance['custom_items_title_font']);
		$instance['custom_items_text_color'] = strip_tags($new_instance['custom_items_text_color']);
		$instance['custom_items_text_font_size'] = (int) $new_instance['custom_items_text_font_size'];
		$instance['custom_items_text_font_family'] = strip_tags($new_instance['custom_items_text_font_family']);
		$instance['custom_items_bttn_bg_color'] = strip_tags($new_instance['custom_items_bttn_bg_color']);
		$instance['custom_items_bttn_hover_bg_color'] = strip_tags($new_instance['custom_items_bttn_hover_bg_color']);
		$instance['custom_items_bttn_link_color'] = strip_tags($new_instance['custom_items_bttn_link_color']);
		$instance['custom_items_bttn_link_hover_color'] = strip_tags($new_instance['custom_items_bttn_link_hover_color']);
		$instance['custom_items_border_style'] = strip_tags($new_instance['custom_items_border_style']);
		$instance['slider_item_w'] = strip_tags($new_instance['slider_item_w']);
		$instance['slider_item_h'] = strip_tags($new_instance['slider_item_h']);
		$instance['custom_items_border_radius'] = strip_tags($new_instance['custom_items_border_radius']);
		$instance['custom_items_bttn_border_radius'] = strip_tags($new_instance['custom_items_bttn_border_radius']);
		$instance['custom_image_border_radius'] = strip_tags($new_instance['custom_image_border_radius']);
	
		
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['ggt_recentposts_widget_entries']) )
			delete_option('ggt_recentposts_widget_entries');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('ggt_recentposts_widget', 'widget');
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$category     = isset( $instance['category'] ) ? (int) $instance['category'] : 0;
		
		$order_by_dir     = isset( $instance['order_by_dir'] ) ? esc_attr( $instance['order_by_dir'] ) : 'DESC';
		$taxterms     = isset( $instance['taxterms'] ) ? esc_attr( $instance['taxterms'] ) : '';
		$slider     = isset( $instance['slider'] ) ? (bool) $instance['slider'] : false;
		$autoplay     = isset( $instance['autoplay'] ) ? (bool) $instance['autoplay'] : false;
		$scrolltimeout     = isset( $instance['scrolltimeout'] ) ? (int) $instance['scrolltimeout'] : 3000;
		$hide_nav_bttns     = isset( $instance['hide_nav_bttns'] ) ? (bool) $instance['hide_nav_bttns'] : false;

		$prev_bttn     = isset( $instance['prev_bttn'] ) ? esc_attr( $instance['prev_bttn'] ) : '';
		$next_bttn     = isset( $instance['next_bttn'] ) ? esc_attr( $instance['next_bttn'] ) : '';
		
		$widget_type     = isset( $instance['widget_type'] ) ? esc_attr( $instance['widget_type'] ) : 'vertical';
		$image_position     = isset( $instance['image_position'] ) ? esc_attr( $instance['image_position'] ) : 'left';
		$columns     = isset( $instance['columns'] ) ? absint( $instance['columns'] ) : 2;
		$show_title     = isset( $instance['show_title'] ) ? (bool) $instance['show_title'] : true;
		$show_thumb     = isset( $instance['show_thumb'] ) ? (bool) $instance['show_thumb'] : false;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		$show_author     = isset( $instance['show_author'] ) ? (bool) $instance['show_author'] : false;
		$show_excerpt     = isset( $instance['show_excerpt'] ) ? (bool) $instance['show_excerpt'] : false;
		$excerpt_size     = isset( $instance['excerpt_size'] ) ? absint( $instance['excerpt_size'] ) : 150;
		$show_readmore     = isset( $instance['show_readmore'] ) ? (bool) $instance['show_readmore'] : false;
		$readmore_text     = isset( $instance['readmore_text'] ) ? esc_attr( $instance['readmore_text'] ) : '';
		$css_classes     = isset( $instance['css_classes'] ) ? esc_attr( $instance['css_classes'] ) : '';
		$title_color     = isset( $instance['title_color'] ) ? esc_attr( $instance['title_color'] ) : '';

		$custom_bg_color     = isset( $instance['custom_bg_color'] ) ? esc_attr( $instance['custom_bg_color'] ) : '';
		$custom_items_bg_color     = isset( $instance['custom_items_bg_color'] ) ? esc_attr( $instance['custom_items_bg_color'] ) : '';
		$custom_items_title_color     = isset( $instance['custom_items_title_color'] ) ? esc_attr( $instance['custom_items_title_color'] ) : '';
		$custom_items_title_hover_color = isset( $instance['custom_items_title_hover_color'] ) ? esc_attr( $instance['custom_items_title_hover_color'] ) : '';
		$custom_items_title_font_size     = isset( $instance['custom_items_title_font_size'] ) ? esc_attr( $instance['custom_items_title_font_size'] ) : '';
		$custom_items_title_font     = isset( $instance['custom_items_title_font'] ) ? esc_attr( $instance['custom_items_title_font'] ) : '';
		$custom_items_text_color     = isset( $instance['custom_items_text_color'] ) ? esc_attr( $instance['custom_items_text_color'] ) : '';
		$custom_items_text_font_size     = isset( $instance['custom_items_text_font_size'] ) ? esc_attr( $instance['custom_items_text_font_size'] ) : '';
		$custom_items_text_font_family   = isset( $instance['custom_items_text_font_family'] ) ? esc_attr( $instance['custom_items_text_font_family'] ) : '';
		$custom_items_bttn_bg_color     = isset( $instance['custom_items_bttn_bg_color'] ) ? esc_attr( $instance['custom_items_bttn_bg_color'] ) : '';
		$custom_items_bttn_hover_bg_color = isset( $instance['custom_items_bttn_hover_bg_color'] ) ? esc_attr( $instance['custom_items_bttn_hover_bg_color'] ) : '';
		$custom_items_bttn_link_color     = isset( $instance['custom_items_bttn_link_color'] ) ? esc_attr( $instance['custom_items_bttn_link_color'] ) : '';
		$custom_items_bttn_link_hover_color = isset( $instance['custom_items_bttn_link_hover_color'] ) ? esc_attr( $instance['custom_items_bttn_link_hover_color'] ) : '';
		$custom_items_border_style     = isset( $instance['custom_items_border_style'] ) ? esc_attr( $instance['custom_items_border_style'] ) : '';
		$custom_items_border_radius     = isset( $instance['custom_items_border_radius'] ) ? esc_attr( $instance['custom_items_border_radius'] ) : '';
		$custom_items_bttn_border_radius     = isset( $instance['custom_items_bttn_border_radius'] ) ? esc_attr( $instance['custom_items_bttn_border_radius'] ) : '';
		$custom_image_border_radius     = isset( $instance['custom_image_border_radius'] ) ? esc_attr( $instance['custom_image_border_radius'] ) : '';
		
		$slider_item_h   = isset( $instance['slider_item_h'] ) ? esc_attr( $instance['slider_item_h'] ) : '125';
		$slider_item_w   = isset( $instance['slider_item_w'] ) ? esc_attr( $instance['slider_item_w'] ) : '240';


		$post_formats = array( 'All', 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' );
		$order_by = array( 'date', 'id', 'author', 'title', 'modified', 'rand', 'comment count', 'order' );
		
		
		                        
?>

		<p class=""><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id( 'title_color' ); ?>" style="display:block;"><?php _e( 'Title color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'title_color' ); ?>" name="<?php echo $this->get_field_name( 'title_color' ); ?>" type="text" value="<?php echo esc_attr( $title_color ); ?>" /></p>


        <div style="color:#fff; font-size:12px; font-weight:bold; padding:3px; margin:0; text-align:center; background:#33302e;">Taxonomy</div>

		<p class=""><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number to display:' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

		<p class=""><label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category:' ); ?></label>
		<?php

			wp_dropdown_categories( array(

				'orderby'    => 'title',
				'show_option_all'   => 'All',
				'hide_empty' => false,
				'name'       => $this->get_field_name( 'category' ),
				'id'         => $this->get_field_id( 'category' ),
				'class'      => 'widefat',
				'selected'   => $category

			) );

			?>
			</p>
			
			
		<p>
			<label for="<?php echo $this->get_field_id('post_format'); ?>"><?php _e( 'Select Post format:' ); ?></label>
			<select name="<?php echo $this->get_field_name('post_format'); ?>" id="<?php echo $this->get_field_id('post_format'); ?>" class="widefat">
			<?php foreach ($post_formats as $post_format) { ?> 
				<option value="<?php echo $post_format; ?>" <?php if(isset($instance['post_format'])) selected( $instance['post_format'], $post_format ); ?>><?php _e($post_format); ?></option>
			<?php } ?>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('order_by'); ?>"><?php _e( 'Order by:' ); ?></label>
			<select name="<?php echo $this->get_field_name('order_by'); ?>" id="<?php echo $this->get_field_id('order_by'); ?>" class="widefat">
			<?php foreach ($order_by as $order_by_el) { ?> 
				<option value="<?php echo $order_by_el; ?>" <?php if(isset($instance['order_by'])) selected( $instance['order_by'], $order_by_el ); ?>><?php _e($order_by_el); ?></option>
			<?php } ?>
			</select>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('order_by_dir'); ?>"><?php _e( 'Order direction:' ); ?></label>
			<select name="<?php echo $this->get_field_name('order_by_dir'); ?>" id="<?php echo $this->get_field_id('order_by_dir'); ?>" class="widefat">
				<option value="ASC" <?php if(isset($instance['order_by_dir'])) selected( $instance['order_by_dir'], 'ASC' ); ?>><?php _e('ASC'); ?></option>
				<option value="DESC" <?php if(isset($instance['order_by_dir'])) selected( $instance['order_by_dir'], 'DESC' ); ?>><?php _e('DESC'); ?></option>
			</select>
		</p>
		
		

		<p class=""><label for="<?php echo $this->get_field_id( 'taxterms' ); ?>"><?php _e( 'by Post-Tags Name (separate by comma):' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'taxterms' ); ?>" name="<?php echo $this->get_field_name( 'taxterms' ); ?>" type="text" value="<?php echo $taxterms; ?>" /></p>
			

		
        <div style="color:#fff; font-size:12px; font-weight:bold; padding:3px; margin:0; text-align:center; background:#33302e;">Slider options</div>
		
		<p><label for="<?php echo $this->get_field_id( 'slider' ); ?>"><?php _e('Slider?', ''); ?>: </label><input id="<?php echo $this->get_field_id( 'slider' ); ?>" class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('slider'); ?>" <?php checked( $slider ); ?> /></p>
		
		<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'slider' ); ?>:true"><label for="<?php echo $this->get_field_id( 'autoplay' ); ?>"><?php _e('AutoPlay?', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'autoplay' ); ?>" name="<?php echo $this->get_field_name('autoplay'); ?>" <?php checked( $autoplay ); ?> /></p>


		<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'slider' ); ?>:true;<?php echo $this->get_field_id( 'autoplay' ); ?>:true"><label for="<?php echo $this->get_field_id( 'scrolltimeout' ); ?>"><?php _e('Slider auto-scroll timeout', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'scrolltimeout' ); ?>" name="<?php echo $this->get_field_name( 'scrolltimeout' ); ?>" type="text" value="<?php echo $scrolltimeout; ?>" /></p>

<!--
			<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'slider' ); ?>:true"><label for="<?php echo $this->get_field_id( 'slider_item_h' ); ?>"><?php _e('Slider Item Height (in px):', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'slider_item_h' ); ?>" name="<?php echo $this->get_field_name( 'slider_item_h' ); ?>" type="text" value="<?php echo $slider_item_h; ?>" /></p>

			<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'slider' ); ?>:true"><label for="<?php echo $this->get_field_id( 'slider_item_w' ); ?>"><?php _e('Slider Item Width (in px):', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'slider_item_w' ); ?>" name="<?php echo $this->get_field_name( 'slider_item_w' ); ?>" type="text" value="<?php echo $slider_item_w; ?>" /></p>
-->

		<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'slider' ); ?>:true"><label for="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>"><?php _e('Hide Prev/Next buttons', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>" name="<?php echo $this->get_field_name('hide_nav_bttns'); ?>" <?php checked( $hide_nav_bttns ); ?> /></p>

<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>:false;<?php echo $this->get_field_id( 'slider' ); ?>:true">
            <label for="<?php echo $this->get_field_name( 'prev_bttn' ); ?>"><?php _e( 'Prev Button Image:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'prev_bttn' ); ?>" id="<?php echo $this->get_field_id( 'prev_bttn' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $prev_bttn ); ?>" />
            <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
        </p>


<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>:false;<?php echo $this->get_field_id( 'slider' ); ?>:true">
            <label for="<?php echo $this->get_field_name( 'next_bttn' ); ?>"><?php _e( 'Next Button Image:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'next_bttn' ); ?>" id="<?php echo $this->get_field_id( 'next_bttn' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $next_bttn ); ?>" />
            <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
        </p>

        <div style="color:#fff; font-size:12px; font-weight:bold; padding:3px; margin:0; text-align:center; background:#33302e;">Widget Customization</div>
        
<!--
        <p>
			<label for="<?php echo $this->get_field_id('widget_type'); ?>"><?php _e( 'Select Post format:' ); ?></label>
			<select name="<?php echo $this->get_field_name('widget_type'); ?>" id="<?php echo $this->get_field_id('widget_type'); ?>" class="widefat">
				<option value="vertical" <?php if(isset($instance['widget_type'])) selected( $instance['widget_type'], 'vertical' ); ?>><?php _e('Vertical'); ?></option>
				<option value="horizontal" <?php if(isset($instance['widget_type'])) selected( $instance['widget_type'], 'horizontal' ); ?>><?php _e('Horizontal'); ?></option>
			</select>
		</p>
-->
        <p>
			<label for="<?php echo $this->get_field_id('image_position'); ?>"><?php _e( 'Image position:' ); ?></label>
			<select name="<?php echo $this->get_field_name('image_position'); ?>" id="<?php echo $this->get_field_id('image_position'); ?>" class="widefat">
				<option value="top" <?php if(isset($instance['image_position'])) selected( $instance['image_position'], 'top' ); ?>><?php _e('Image top'); ?></option>
				<option value="bottom" <?php if(isset($instance['image_position'])) selected( $instance['image_position'], 'bottom' ); ?>><?php _e('Image bottom'); ?></option>
				<option value="left" <?php if(isset($instance['image_position'])) selected( $instance['image_position'], 'left' ); ?>><?php _e('Image left'); ?></option>
				<option value="right" <?php if(isset($instance['image_position'])) selected( $instance['image_position'], 'right' ); ?>><?php _e('Image right'); ?></option>
			</select>
		</p>
<!--         <p class="dependence" data-dependence="<?php echo $this->get_field_id( 'widget_type' ); ?>:horizontal"> -->
        <p>
			<label for="<?php echo $this->get_field_id('columns'); ?>"><?php _e( 'Columns:' ); ?></label>
			<select name="<?php echo $this->get_field_name('columns'); ?>" id="<?php echo $this->get_field_id('columns'); ?>" class="widefat">
				<option value="1" <?php if(isset($instance['columns'])) selected( $instance['columns'], 1 ); ?>><?php _e('1 column (default)'); ?></option>
				<option value="2" <?php if(isset($instance['columns'])) selected( $instance['columns'], 2 ); ?>><?php _e('2 columns'); ?></option>
				<option value="4" <?php if(isset($instance['columns'])) selected( $instance['columns'], 4 ); ?>><?php _e('4 columns'); ?></option>
				<option value="6" <?php if(isset($instance['columns'])) selected( $instance['columns'], 6 ); ?>><?php _e('6 columns'); ?></option>
				<option value="8" <?php if(isset($instance['columns'])) selected( $instance['columns'], 8 ); ?>><?php _e('8 columns'); ?></option>
			</select>
		</p>
		
				<p><label for="<?php echo $this->get_field_id( 'show_title' ); ?>"><?php _e('Show title', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'show_title' ); ?>" name="<?php echo $this->get_field_name('show_title'); ?>" <?php checked( $show_title ); ?> /></p>

				<p><label for="<?php echo $this->get_field_id( 'show_thumb' ); ?>"><?php _e('Show post thumbnail', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'show_thumb' ); ?>" name="<?php echo $this->get_field_name('show_thumb'); ?>" <?php checked( $show_thumb ); ?> /></p>
				
		<p><label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Show Date' ); ?>: </label><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
		</p>
				
				<p><label for="<?php echo $this->get_field_id( 'show_author' ); ?>"><?php _e('Show Author', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'show_author' ); ?>" name="<?php echo $this->get_field_name('show_author'); ?>" <?php checked( $show_author ); ?> /></p>

				<p><label for="<?php echo $this->get_field_id( 'show_excerpt' ); ?>"><?php _e('Show Excerpt', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'show_excerpt' ); ?>" name="<?php echo $this->get_field_name('show_excerpt'); ?>" <?php checked( $show_excerpt ); ?> /></p>
				
				<p><label for="<?php echo $this->get_field_id( 'excerpt_size' ); ?>"><?php _e('Excerpt size', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'excerpt_size' ); ?>" name="<?php echo $this->get_field_name( 'excerpt_size' ); ?>" type="text" value="<?php echo $excerpt_size; ?>" /></p>


				<p><label for="<?php echo $this->get_field_id( 'show_readmore' ); ?>"><?php _e('Readmore button', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'show_readmore' ); ?>" name="<?php echo $this->get_field_name('show_readmore'); ?>" <?php checked( $show_readmore ); ?> /></p>
				
				<p><label for="<?php echo $this->get_field_id( 'readmore_text' ); ?>"><?php _e('Readmore button text', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'readmore_text' ); ?>" name="<?php echo $this->get_field_name( 'readmore_text' ); ?>" type="text" value="<?php echo $readmore_text; ?>" /></p>


		<p><label for="<?php echo $this->get_field_id( 'css_classes' ); ?>"><?php _e('CSS classes', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'css_classes' ); ?>" name="<?php echo $this->get_field_name( 'css_classes' ); ?>" type="text" value="<?php echo $css_classes; ?>" /></p>


        <p>
			<label for="<?php echo $this->get_field_id('skin'); ?>"><?php _e( 'Skin: ' ); ?></label>
			<select name="<?php echo $this->get_field_name('skin'); ?>" id="<?php echo $this->get_field_id('skin'); ?>" class="widefat">
				<option value="dark" <?php if(isset($instance['skin'])) selected( $instance['skin'], 'dark' ); ?>><?php _e('Dark'); ?></option>
				<option value="light" <?php if(isset($instance['skin'])) selected( $instance['skin'], 'light' ); ?>><?php _e('Light'); ?></option>
				<option value="custom" <?php if(isset($instance['skin'])) selected( $instance['skin'], 'custom' ); ?>><?php _e('Custom colors'); ?></option>
			</select>
		</p>
		
<div class="dependence" data-dependence="<?php echo $this->get_field_id( 'skin' ); ?>:custom" >		
		<p><label for="<?php echo $this->get_field_id( 'custom_bg_color' ); ?>" style="display:block;"><?php _e( 'Widget Background color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_bg_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_bg_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_bg_color ); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'custom_items_bg_color' ); ?>" style="display:block;"><?php _e( 'Items Background color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_bg_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bg_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_bg_color ); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'custom_items_title_color' ); ?>" style="display:block;"><?php _e( 'Items Title color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_title_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_title_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_title_color ); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'custom_items_title_hover_color' ); ?>" style="display:block;"><?php _e( 'Items Title hover color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_title_hover_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_title_hover_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_title_hover_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_title_font_size' ); ?>"><?php _e('Items Title Font size', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_title_font_size' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_title_font_size' ); ?>" type="text" value="<?php echo $custom_items_title_font_size; ?>" /></p>
		
			<p><label for="<?php echo $this->get_field_id( 'custom_items_title_font' ); ?>"><?php _e('Items Title Font-Family', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_title_font' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_title_font' ); ?>" type="text" value="<?php echo $custom_items_title_font; ?>" /></p>
				
			<p><label for="<?php echo $this->get_field_id( 'custom_items_title_hover_color' ); ?>" style="display:block;"><?php _e( 'Items Text color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_text_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_text_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_text_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_text_font_size' ); ?>"><?php _e('Items Text Font size', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_text_font_size' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_text_font_size' ); ?>" type="text" value="<?php echo $custom_items_text_font_size; ?>" /></p>
		
			<p><label for="<?php echo $this->get_field_id( 'custom_items_text_font_family' ); ?>"><?php _e('Items Text Font-Family', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_text_font_family' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_text_font_family' ); ?>" type="text" value="<?php echo $custom_items_text_font_family; ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_bttn_bg_color' ); ?>" style="display:block;"><?php _e( 'Items Button background color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_bttn_bg_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bttn_bg_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_bttn_bg_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_bttn_hover_bg_color' ); ?>" style="display:block;"><?php _e( 'Items Button hover background color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_bttn_hover_bg_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bttn_hover_bg_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_bttn_hover_bg_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_bttn_link_color' ); ?>" style="display:block;"><?php _e( 'Items Button link color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_bttn_link_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bttn_link_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_bttn_link_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_bttn_link_hover_color' ); ?>" style="display:block;"><?php _e( 'Items Button link hover color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_bttn_link_hover_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bttn_link_hover_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_bttn_link_hover_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_border_style' ); ?>"><?php _e('Items Border style', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_border_style' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_border_style' ); ?>" type="text" value="<?php echo $custom_items_border_style; ?>" /></p>
		
			<p><label for="<?php echo $this->get_field_id( 'custom_items_border_radius' ); ?>"><?php _e('Items Border radius', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_border_radius' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_border_radius' ); ?>" type="text" value="<?php echo $custom_items_border_radius; ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_bttn_border_radius' ); ?>"><?php _e('Items Button border radius', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_bttn_border_radius' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bttn_border_radius' ); ?>" type="text" value="<?php echo $custom_items_bttn_border_radius; ?>" /></p>
		
			<p><label for="<?php echo $this->get_field_id( 'custom_image_border_radius' ); ?>"><?php _e('Image Border radius', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_image_border_radius' ); ?>" name="<?php echo $this->get_field_name( 'custom_image_border_radius' ); ?>" type="text" value="<?php echo $custom_image_border_radius; ?>" /></p>
</div>

<?php
	}
}
      
add_action( 'widgets_init', create_function( '', 'register_widget("ggt_recentposts_widget");' ) );
