<?php

class ggt_instagram_widget extends WP_Widget {

	function ggt_instagram_widget() {
		global $ukey;
		$this->ukey = $ukey;
		$widget_ops = array('classname' => 'ggt-instagram-feed', 'description' => __('Displays your latest Instagram photos', $this->ukey) );
        parent::__construct('ggt-instagram-feed', __('GGT Social Widgets: Instagram', $this->ukey), $widget_ops);
	}

	function widget($args, $instance) {
	
		  wp_enqueue_style( 'ggt_swidget_extend_style', plugins_url('/../assets/css/custom.css', __FILE__) );
	      wp_enqueue_style( 'ggt_swidget_owlcss', plugins_url('/../assets/css/owl.carousel.css', __FILE__) );
	      wp_enqueue_style( 'ggt_swidget_font-ggtcontrols', plugins_url('/../assets/css/font-ggtcontrols.css', __FILE__) );
	      wp_enqueue_style('thickbox');
	
	      wp_enqueue_script( 'ggt_swidget_waypoint', plugins_url('/../assets/js/waypoints.min.js', __FILE__), array('jquery') );
	      wp_enqueue_script( 'ggt_swidget_extend_js', plugins_url('/../assets/js/custom.js', __FILE__), array('jquery') );
	      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
	      wp_enqueue_script( 'ggt_swidget_jquery-expand', plugins_url('/../assets/js/expand.min.js', __FILE__), array('jquery') );
	
	      wp_enqueue_script( 'ggt_swidget_owljs', plugins_url('/../assets/js/owl.carousel.js', __FILE__), array('jquery') );
	      wp_enqueue_script( 'ggt_swidget_carousel_js', plugins_url('/../assets/js/carousel.js', __FILE__), array('jquery', 'ggt_swidget_owljs') );

		extract($args, EXTR_SKIP);

		$title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
		$username = empty($instance['username']) ? '' : esc_attr(trim($instance['username']));

		$limit = empty($instance['numdisp']) ? 1 : $instance['numdisp'];
		
		$size = empty($instance['size']) ? 'thumbnail' : $instance['size'];
		$target = empty($instance['target']) ? '_self' : $instance['target'];
		$link = empty($instance['link']) ? '' : $instance['link'];
		$dc_posts = ! empty($instance['dc_posts']) ? '1' : '0';
		$dc_followers = ! empty($instance['dc_followers']) ? '1' : '0';
		$dc_following = ! empty($instance['dc_following']) ? '1' : '0';
		$d_useravatar = ! empty($instance['d_useravatar']) ? '1' : '0';
		$d_followbttn = ! empty($instance['d_followbttn']) ? '1' : '0';
		$d_website = ! empty($instance['d_website']) ? '1' : '0';
		$d_captions = ! empty($instance['d_captions']) ? '1' : '0';
		$autoplay = ! empty($instance['autoplay']) ? 'true' : 'false';
		$layout = empty( $instance['layout'] ) ? 'grid' : $instance['layout'];
		$widtype = empty( $instance['widtype'] ) ? 'profile' : $instance['widtype'];
		$mproc = 100 / 4;
		
		$add_att = 'class="finstagram-pics"';
		
		if ($widtype == 'profile') $limit = 12;
		
		

		echo $before_widget;
		if(!empty($title)) { echo $before_title . $title . $after_title; };

		do_action( 'ggt_before_widget', $instance );

		if ($username != '') {

			$media_array = $this->scrape_instagram($username, $limit);
			
/* 						print_r('<pre>');print_r($media_array);print_r('</pre>'); */

			if ( is_wp_error($media_array) ) {

			   echo $media_array->get_error_message();

			} else {

				// filter for images only?
				if ( $images_only = apply_filters( 'ggt_images_only', true ) )
					if (isset($media_array['media'])) $media_array['media'] = array_filter( $media_array['media'], array( $this, 'images_only' ) );
					
					if ($widtype == 'gallery' && isset($media_array['media']) ) {
						$cel = (count($media_array['media']) > 3) ? floor(count($media_array['media']) / 4) : count($media_array['media']) / 4;
						$sliced_array = array();
						
						if ($cel < 1) {
							$proc = 100 / count($media_array['media']);
						} else {
						
						$acel = 4;
						foreach ($media_array as $key => $minstagram) {
						   if ($key == 'media') $sliced_array = array_slice($minstagram, $acel, null, true); 						   
						}
							$proc = 100 / 4;
							$procost = 100 / (count($sliced_array) > 0 ? count($sliced_array) : 4);
					}
						
						$add_att = ($layout == 'grid') ? 'class="finstagram-pics"' : 'class="inowlcarousel finstagram-pics" data-autoplay="'.$autoplay.'"';

					}

//print_r('<pre>');print_r(count($sliced_array));print_r('</pre>');

				?>
				<ul <?php echo $add_att; ?>><?php
				if(isset($media_array['media'])) foreach ($media_array['media'] as $key=>$item) {
				
				if ($widtype == 'gallery') {
				
						if ($layout == 'grid') {
						
						$mproc = (isset($sliced_array[$key])) ? $procost : $proc;
						
							echo '<li style="width:'.$mproc.'%"><a href="'. esc_url( $item['link'] ) .'" target="'. esc_attr( $target ) .'"><img src="'. esc_url($item['low_resolution']['url']) .'"  alt="'. esc_attr( $item['description'] ) .'" title="'. esc_attr( $item['description'] ).'"  /></a></li>';
							
						} else {
						
							echo '<li><a href="'. esc_url( $item['link'] ) .'" target="'. esc_attr( $target ) .'"><img src="'. esc_url($item['low_resolution']['url']) .'"  alt="'. esc_attr( $item['description'] ) .'" title="'. esc_attr( $item['description'] ).'"  /></a></li>'; 						
						}
					
					} else {
						
						echo '<li style="width:'.$mproc.'%"><a href="'. esc_url( $item['link'] ) .'" target="'. esc_attr( $target ) .'"><img src="'. esc_url($item['low_resolution']['url']) .'"  alt="'. esc_attr( $item['description'] ) .'" title="'. esc_attr( $item['description'] ).'"/></a></li>';
					}
				
				}
				?>
				
				</ul>
				
				
				
				<?php if ($d_useravatar > 0) { ?>
				<div class="instaprof_avatar" style="background: url(<?php echo $media_array['info']['avatar']; ?>) no-repeat center center;"></div>	
				<?php } ?>
							
				<?php if (($d_captions == 0) && isset($media_array['info']['username']) && (trim($media_array['info']['username']) !== '')) { ?><h3 class="instaprof_username"><?php echo $media_array['info']['username']; ?></h3><?php } ?>	
				
				<?php if (($d_website > 0) && isset($media_array['info']['website']) && (trim($media_array['info']['website']) !== '')) { ?><div class="instaprof_website"><a href="<?php echo trim($media_array['info']['website']); ?>"><?php echo trim($media_array['info']['website']); ?></a></div><?php } ?>			

				<?php if (($d_captions == 0) && isset($media_array['info']['bio']) && (trim($media_array['info']['bio']) !== '')) { ?><div class="instaprof_bio"><?php echo $media_array['info']['bio']; ?></div><?php } ?>

				<?php if (($dc_posts > 0) || ($dc_followers > 0) || ($dc_following > 0)) { ?> <div class="instaprof_counters"> <?php } ?>
				
				<?php if ($dc_posts > 0) echo '<div class="count_posts">'.__("Posts", $this->ukey).': <span>'.$media_array['info']['count_posts'].'</span></div>'; ?>
				<?php if ($dc_followers > 0) echo '<div class="count_followers">'.__("Followers", $this->ukey).': <span>'.$media_array['info']['count_followers'].'</span></div>'; ?> 
				<?php if ($dc_following > 0) echo '<div class="count_following">'.__("Following", $this->ukey).': <span>'.$media_array['info']['count_following'].'</span></div>'; ?>
				
				<?php if (($dc_posts > 0) || ($dc_followers > 0) || ($dc_following > 0)) { ?></div>	<?php } ?>			
				
				<?php
				
			}
		}

		if (($link != '') && ($d_followbttn > 0)) {
			?><div class="follow_button"><a href="//instagram.com/<?php echo trim($username); ?>" rel="me" target="<?php echo esc_attr( $target ); ?>"><?php echo $link; ?></a></div><?php
		}

		do_action( 'ggt_before_widget', $instance );

		echo $after_widget;
	}

	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array(  'numdisp' => 1, 'layout' => 'grid', 'widtype' => 'profile', 'title' => __('Instagram', $this->ukey), 'username' => '', 'link' => __('Follow Us', $this->ukey), 'number' => 12, 'size' => 'thumbnail', 'target' => '_self', 'dc_posts' => 0, 'dc_followers' => 0, 'dc_following' => 0, 'd_useravatar' => 0, 'd_website' => 0, 'd_captions' => 0, 'd_followbttn' => 0, 'autoplay' => 0,) );
		$title = esc_attr($instance['title']);
		$username = esc_attr($instance['username']);
		$numdisp = absint($instance['numdisp']);
		$size = esc_attr($instance['size']);
		$target = esc_attr($instance['target']);
		$link = esc_attr($instance['link']);
		$dc_posts = $instance['dc_posts'] ? 'checked="checked"' : '';
		$dc_followers = $instance['dc_followers'] ? 'checked="checked"' : '';
		$dc_following = $instance['dc_following'] ? 'checked="checked"' : '';
		$d_useravatar = $instance['d_useravatar'] ? 'checked="checked"' : '';
		$d_website = $instance['d_website'] ? 'checked="checked"' : '';
		$d_captions = $instance['d_captions'] ? 'checked="checked"' : '';
		$d_followbttn = $instance['d_followbttn'] ? 'checked="checked"' : '';
		$autoplay = $instance['autoplay'] ? 'checked="checked"' : '';
		
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', $this->ukey); ?>: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
		<p><label for="<?php echo $this->get_field_id('username'); ?>"><?php _e('Username', $this->ukey); ?>: <input class="widefat" id="<?php echo $this->get_field_id('username'); ?>" name="<?php echo $this->get_field_name('username'); ?>" type="text" value="<?php echo $username; ?>" /></label></p>
		
		<p>
			<label for="<?php echo $this->get_field_id('widtype'); ?>"><?php _e( 'Widget type:' ); ?></label>
			<select name="<?php echo $this->get_field_name('widtype'); ?>" id="<?php echo $this->get_field_id('widtype'); ?>" class="widefat">
				<option value="profile"<?php selected( $instance['widtype'], 'profile' ); ?>><?php _e('Profile'); ?></option>
				<option value="gallery"<?php selected( $instance['widtype'], 'gallery' ); ?>><?php _e('Gallery'); ?></option>
			</select>
		</p>
		
		<p id="pel<?php echo $this->get_field_id('layout'); ?>" >
			<label for="<?php echo $this->get_field_id('layout'); ?>"><?php _e( 'Gallery layout:' ); ?></label>
			<select name="<?php echo $this->get_field_name('layout'); ?>" id="<?php echo $this->get_field_id('layout'); ?>" class="widefat">
				<option value="grid"<?php selected( $instance['layout'], 'grid' ); ?>><?php _e('Grid'); ?></option>
				<option value="slideshow"<?php selected( $instance['layout'], 'slideshow' ); ?>><?php _e('Slideshow'); ?></option>
			</select>
		</p>
		

		<p id="pel<?php echo $this->get_field_id('numdisp'); ?>" >
			<label for="<?php echo $this->get_field_id('numdisp'); ?>"><?php _e( 'Number of Photos to Display:' ); ?></label>
			<select name="<?php echo $this->get_field_name('numdisp'); ?>" id="<?php echo $this->get_field_id('numdisp'); ?>" class="widefat">
			<?php for($i=1;$i<11;$i++) { ?>
				<option value="<?php echo $i; ?>"<?php selected( $instance['numdisp'], $i ); ?>><?php echo $i; ?></option>
			<?php } ?>
			</select>
		</p>
		
<p id="<?php echo $this->get_field_id('autoplay'); ?>"><label><?php _e('AutoPlay', $this->ukey); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('autoplay'); ?>" <?php echo $autoplay; ?> /></p>
		
		
		
		<p><label><?php _e('Display counters', $this->ukey); ?>: </label><br><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('dc_posts'); ?>" <?php echo $dc_posts; ?> /><?php _e('Posts', $this->ukey); ?> 
<input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('dc_followers'); ?>" <?php echo $dc_followers; ?> /><?php _e('Followers', $this->ukey); ?><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('dc_following'); ?>" <?php echo $dc_following; ?> /><?php _e('Following', $this->ukey); ?></p>
<p><label><?php _e('Display user avatar', $this->ukey); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('d_useravatar'); ?>" <?php echo $d_useravatar; ?> /></p>
<p><label><?php _e('Display site url', $this->ukey); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('d_website'); ?>" <?php echo $d_website; ?> /></p>
<p><label><?php _e('Hide captions', $this->ukey); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('d_captions'); ?>" <?php echo $d_captions; ?> /></p>
<p><label><?php _e('Display  Follow button', $this->ukey); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('d_followbttn'); ?>" <?php echo $d_followbttn; ?> /></p>
		
		<p><label for="<?php echo $this->get_field_id('link'); ?>"><?php _e('Follow button text', $this->ukey); ?>: <input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo $link; ?>" /></label></p>
		
		
		
				<script>
				 (function ($) {
				 
				 $(document).ready(function() {
				 
				 		 if ($("#<?php echo $this->get_field_id('widtype'); ?>").val() == 'gallery') {
				 		 
					 		 if ($("#<?php echo $this->get_field_id('layout'); ?>").val() == 'slideshow') {
					              $('#<?php echo $this->get_field_id('autoplay'); ?>').show();
					         } else {
						           $('#<?php echo $this->get_field_id('autoplay'); ?>').hide();
					         }
					         
				              $('#pel<?php echo $this->get_field_id('numdisp'); ?>').show();
				              $("#pel<?php echo $this->get_field_id('layout'); ?>").show();

				              
				         } else {
				           	  $('#<?php echo $this->get_field_id('autoplay'); ?>').hide();
				              $('#pel<?php echo $this->get_field_id('numdisp'); ?>').hide();
				              $("#pel<?php echo $this->get_field_id('layout'); ?>").hide();
				         }


					 $("#<?php echo $this->get_field_id('layout'); ?>").change(function(){
						 if ($(this).val() == 'slideshow') {
				              $('#<?php echo $this->get_field_id('autoplay'); ?>').show();
				         } else {
				              $('#<?php echo $this->get_field_id('autoplay'); ?>').hide();
				         }
					 });
					 
					 
					 $("#<?php echo $this->get_field_id('widtype'); ?>").change(function(){
						 if ($(this).val() == 'gallery') {
							  $("#pel<?php echo $this->get_field_id('layout'); ?>").show();
							  $("#pel<?php echo $this->get_field_id('numdisp'); ?>").show();
							  
							  if ($("#<?php echo $this->get_field_id('layout'); ?>").val() == 'slideshow') {
							  
				             	 $('#<?php echo $this->get_field_id('autoplay'); ?>').show();
				              
				              }
				              
				         } else {
							  $("#pel<?php echo $this->get_field_id('layout'); ?>").hide();
							  $("#pel<?php echo $this->get_field_id('numdisp'); ?>").hide();
				              $('#<?php echo $this->get_field_id('autoplay'); ?>').hide();
				         }
					 });

	
				});
				 })(jQuery);
				</script>
				


		<?php

	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['username'] = trim(strip_tags($new_instance['username']));
		$instance['number'] = !absint($new_instance['number']) ? 12 : $new_instance['number'];
		$instance['size'] = (($new_instance['size'] == 'thumbnail' || $new_instance['size'] == 'large') ? $new_instance['size'] : 'thumbnail');
		$instance['target'] = (($new_instance['target'] == '_self' || $new_instance['target'] == '_blank') ? $new_instance['target'] : '_self');
		$instance['link'] = strip_tags($new_instance['link']);
		$instance['dc_posts'] = $new_instance['dc_posts'] ? 1 : 0;
		$instance['dc_followers'] = $new_instance['dc_followers'] ? 1 : 0;
		$instance['dc_following'] = $new_instance['dc_following'] ? 1 : 0;
		$instance['d_useravatar'] = $new_instance['d_useravatar'] ? 1 : 0;
		$instance['d_website'] = $new_instance['d_website'] ? 1 : 0;
		$instance['d_captions'] = $new_instance['d_captions'] ? 1 : 0;
		$instance['d_followbttn'] = $new_instance['d_followbttn'] ? 1 : 0;
		$instance['autoplay'] = $new_instance['autoplay'] ? 1 : 0;
		
		
		if ( in_array( $new_instance['widtype'], array( 'profile', 'gallery' ) ) ) {
			$instance['widtype'] = $new_instance['widtype'];
		} else {
			$instance['widtype'] = 'profile';
		}

		if ( in_array( $new_instance['layout'], array( 'grid', 'slideshow' ) ) ) {
			$instance['layout'] = $new_instance['layout'];
		} else {
			$instance['layout'] = 'grid';
		}

		if ( in_array( $new_instance['numdisp'], array( 1,2,3,4,5,6,7,8,9,10 ) ) ) {
			$instance['numdisp'] = $new_instance['numdisp'];
		} else {
			$instance['numdisp'] = 1;
		}
		
		


		
		return $instance;
	}

	// based on https://gist.github.com/cosmocatalano/4544576
	function scrape_instagram($username, $slice = 1) {

		if (false === ($instagram = get_transient('mninstagram-media-'.sanitize_title_with_dashes($username)))) {

			$remote = wp_remote_get('http://instagram.com/'.trim($username));

			if (is_wp_error($remote))
	  			return new WP_Error('site_down', __('Unable to communicate with Instagram.', $this->ukey));

  			if ( 200 != wp_remote_retrieve_response_code( $remote ) )
  				return new WP_Error('invalid_response', __('Instagram did not return a 200.', $this->ukey));

			$shards = explode('window._sharedData = ', $remote['body']);
			$insta_json = explode(';</script>', $shards[1]);
			$insta_array = json_decode($insta_json[0], TRUE);

			if (!$insta_array)
	  			return new WP_Error('bad_json', __('Instagram has returned invalid data.', $this->ukey));
			
			$images = $insta_array['entry_data']['UserProfile'][0]['userMedia'];
			
			$avatar = $insta_array['entry_data']['UserProfile'][0]['user']['profile_picture'];
			$count_posts = $insta_array['entry_data']['UserProfile'][0]['user']['counts']['media'];
			$count_followers = $insta_array['entry_data']['UserProfile'][0]['user']['counts']['followed_by'];
			$count_following = $insta_array['entry_data']['UserProfile'][0]['user']['counts']['follows'];
			$musername = $insta_array['entry_data']['UserProfile'][0]['user']['full_name'];
			$bio = $insta_array['entry_data']['UserProfile'][0]['user']['bio'];
			$website = $insta_array['entry_data']['UserProfile'][0]['user']['website'];


			$instagram = array();
			
			$instagram['info'] = array (
			'username'   => $musername,
			'avatar'   => $avatar,
			'count_posts'   => $count_posts,
			'count_followers'   => $count_followers,
			'count_following'   => $count_following,
			'bio'   => $bio,
			'website'   => $website,
			);

			foreach ($images as $image) {

				if ($image['user']['username'] == $username) {

					$image['link']                          = preg_replace( "/^http:/i", "", $image['link'] );
					$image['images']['low_resolution']           = preg_replace( "/^http:/i", "", $image['images']['low_resolution'] );
					$image['images']['thumbnail']           = preg_replace( "/^http:/i", "", $image['images']['thumbnail'] );
					$image['images']['standard_resolution'] = preg_replace( "/^http:/i", "", $image['images']['standard_resolution'] );
					

					$instagram['media'][] = array(
						'description'   => $image['caption']['text'],
						'link'          => $image['link'],
						'time'          => $image['created_time'],
						'comments'      => $image['comments']['count'],
						'likes'         => $image['likes']['count'],
						'thumbnail'     => $image['images']['thumbnail'],
						'large'         => $image['images']['standard_resolution'],
						'low_resolution'         => $image['images']['low_resolution'],
						'type'          => $image['type'],
					);
				}
			}

			$instagram = base64_encode( serialize( $instagram ) );
			set_transient('mninstagram-media-'.sanitize_title_with_dashes($username), $instagram, apply_filters('ggt_instagram_cache_time', 2)); //HOUR_IN_SECONDS*2
		}

		$instagram = unserialize( base64_decode( $instagram ) );

/* print_r('<pre>');print_r($instagram['media']);print_r('</pre>'); */

$sliced_array = array();  //setup the array you want with the sliced values.

foreach ($instagram as $key => $minstagram) {
   if ($key == 'media') $sliced_array['media'] = array_slice($minstagram, 0, $slice);
}

$sliced_array['info'] = $instagram['info'];

		return $sliced_array; 
}

	function images_only($media_item) {

		if ($media_item['type'] == 'image')
			return true;

		return false;
	}
}