<?php

add_action('widgets_init', 'timetable_load_widgets');


function timetable_load_widgets()
{
	register_widget('ggt_timetable_widget');
}

class ggt_timetable_widget extends WP_Widget {

	function ggt_timetable_widget()
	{
		$widget_ops = array('classname' => 'ggt_timetable_widget', 'description' => 'Timetable');

		$control_ops = array('id_base' => 'timetable-widget');

		$this->WP_Widget('timetable-widget', __('GGT Timetable'), $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
	
		  wp_enqueue_style( 'ggt_swidget_extend_style', plugins_url('/../assets/css/custom.css', __FILE__) );
	      wp_enqueue_style('thickbox');
	
	      wp_enqueue_script( 'ggt_swidget_extend_js', plugins_url('/../assets/js/custom.js', __FILE__), array('jquery') );
	      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
	      wp_enqueue_script( 'ggt_swidget_jquery-expand', plugins_url('/../assets/js/expand.min.js', __FILE__), array('jquery') );
	      wp_enqueue_style( 'ggt_swidget_font-ggtcontrols', plugins_url('/../assets/css/font-ggtcontrols.css', __FILE__) );


		extract($args);
		$title = apply_filters('widget_title', $instance['title']);

		$whataday_m = ((bool) $instance['m_close']) ? 'dayoff' : 'working';
		$whataday_t = ((bool) $instance['t_close']) ? 'dayoff' : 'working';
		$whataday_w = ((bool) $instance['w_close']) ? 'dayoff' : 'working';
		$whataday_th = ((bool) $instance['th_close']) ? 'dayoff' : 'working';
		$whataday_f = ((bool) $instance['f_close']) ? 'dayoff' : 'working';
		$whataday_s = ((bool) $instance['s_close']) ? 'dayoff' : 'working';
		$whataday_su = ((bool) $instance['su_close']) ? 'dayoff' : 'working';
		
		$working_day_color = ( ! empty( $instance['working_day_color'] )) ? $instance['working_day_color'] : '';
		$day_off_color = ( ! empty( $instance['day_off_color'] )) ? $instance['day_off_color'] : '';
		
		$m_b = ( ! empty( $instance['m_b'] )) ? esc_attr($instance['m_b']) : '';
		$m_e = ( ! empty( $instance['m_e'] )) ? esc_attr($instance['m_e']) : '';
		$t_b = ( ! empty( $instance['t_b'] )) ? esc_attr($instance['t_b']) : '';
		$t_e = ( ! empty( $instance['t_e'] )) ? esc_attr($instance['t_e']) : '';
		$w_b = ( ! empty( $instance['w_b'] )) ? esc_attr($instance['w_b']) : '';
		$w_e = ( ! empty( $instance['w_e'] )) ? esc_attr($instance['w_e']) : '';
		$th_b = ( ! empty( $instance['th_b'] )) ? esc_attr($instance['th_b']) : '';
		$th_e = ( ! empty( $instance['th_e'] )) ? esc_attr($instance['th_e']) : '';
		$f_b = ( ! empty( $instance['f_b'] )) ? esc_attr($instance['f_b']) : '';
		$f_e = ( ! empty( $instance['f_e'] )) ? esc_attr($instance['f_e']) : '';
		$s_b = ( ! empty( $instance['s_b'] )) ? esc_attr($instance['s_b']) : '';
		$s_e = ( ! empty( $instance['s_e'] )) ? esc_attr($instance['s_e']) : '';
		$su_b = ( ! empty( $instance['su_b'] )) ? esc_attr($instance['su_b']) : '';
		$su_e = ( ! empty( $instance['su_e'] )) ? esc_attr($instance['su_e']) : '';


		echo $before_widget;
		
			?>
			
			<style>
				.dayoff {
				background-color: <?php echo $day_off_color; ?>;
				}			
				
				.working {
				background-color: <?php echo $working_day_color; ?>;
				}		
			</style>
			
		<?php 
		
		if($title) {
			echo $before_title . esc_attr($title) . $after_title;
		} 
		
		?>
		
		
		<div class="timetable">
				<div class="row">
					<div class="dayname <?php echo $whataday_m; ?>">Mo</div>
					<div class=""><?php echo ($whataday_m == 'dayoff') ? 'Day off' : $m_b.' - '.$m_e; ?></div>
				</div>
				<div class="row">
					<div class="dayname <?php echo $whataday_t; ?>">Tu</div>
					<div class=""><?php echo ($whataday_t == 'dayoff') ? 'Day off' : $t_b.' - '.$t_e; ?></div>
				</div>
				<div class="row">
					<div class="dayname <?php echo $whataday_w; ?>">We</div>
					<div class=""><?php echo ($whataday_w == 'dayoff') ? 'Day off' : $w_b.' - '.$w_e; ?></div>
				</div>
				<div class="row">
					<div class="dayname <?php echo $whataday_th; ?>">Th</div>
					<div class=""><?php echo ($whataday_th == 'dayoff') ? 'Day off' : $th_b.' - '.$th_e; ?></div>
				</div>
				<div class="row">
					<div class="dayname <?php echo $whataday_f; ?>">Fr</div>
					<div class=""><?php echo ($whataday_f == 'dayoff') ? 'Day off' : $f_b.' - '.$f_e; ?></div>
				</div>
				<div class="row">
					<div class="dayname <?php echo $whataday_s; ?>">Sa</div>
					<div class=""><?php echo ($whataday_s == 'dayoff') ? 'Day off' : $s_b.' - '.$s_e; ?></div>
				</div>
				<div class="row">
					<div class="dayname <?php echo $whataday_su; ?>">Su</div>
					<div class=""><?php echo ($whataday_su == 'dayoff') ? 'Day off' : $su_b.' - '.$su_e; ?></div>
				</div>
		</div>
		
		<?php
					
		echo $after_widget;
	}
	

	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['m_b'] = strip_tags($new_instance['m_b']);
		$instance['m_e'] = strip_tags($new_instance['m_e']);
		$instance['t_b'] = strip_tags($new_instance['t_b']);
		$instance['t_e'] = strip_tags($new_instance['t_e']);
		$instance['w_b'] = strip_tags($new_instance['w_b']);
		$instance['w_e'] = strip_tags($new_instance['w_e']);
		$instance['th_b'] = strip_tags($new_instance['th_b']);
		$instance['th_e'] = strip_tags($new_instance['th_e']);
		$instance['f_b'] = strip_tags($new_instance['f_b']);
		$instance['f_e'] = strip_tags($new_instance['f_e']);
		$instance['s_b'] = strip_tags($new_instance['s_b']);
		$instance['s_e'] = strip_tags($new_instance['s_e']);
		$instance['su_b'] = strip_tags($new_instance['su_b']);
		$instance['su_e'] = strip_tags($new_instance['su_e']);

		$instance['working_day_color'] = $new_instance['working_day_color'];
		$instance['day_off_color'] = $new_instance['day_off_color'];

		$instance['m_close']     = isset( $new_instance['m_close'] ) ? (bool) $new_instance['m_close'] : false;
		$instance['t_close']     = isset( $new_instance['t_close'] ) ? (bool) $new_instance['t_close'] : false;
		$instance['w_close']     = isset( $new_instance['w_close'] ) ? (bool) $new_instance['w_close'] : false;
		$instance['th_close']     = isset( $new_instance['th_close'] ) ? (bool) $new_instance['th_close'] : false;
		$instance['f_close']     = isset( $new_instance['f_close'] ) ? (bool) $new_instance['f_close'] : false;
		$instance['s_close']     = isset( $new_instance['s_close'] ) ? (bool) $new_instance['s_close'] : false;
		$instance['su_close']     = isset( $new_instance['su_close'] ) ? (bool) $new_instance['su_close'] : false;


		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Timetable',
            'm_b' => '',
            'm_e' => '', 
            't_b' => '',
            't_e' => '',
            'w_b' => '',
            'w_e' => '',
            'th_b' => '',
            'th_e' => '',
            'f_b' => '',
            'f_e' => '',
            's_b' => '',
            's_e' => '',
            'su_b' => '',
            'su_e' => '',
            'working_day_color' => '',
            'day_off_color' => '',
    );
		$instance = wp_parse_args((array) $instance, $defaults);
		
		$m_close     = isset( $instance['m_close'] ) ? (bool) $instance['m_close'] : false;
		$t_close     = isset( $instance['t_close'] ) ? (bool) $instance['t_close'] : false;
		$w_close     = isset( $instance['w_close'] ) ? (bool) $instance['w_close'] : false;
		$th_close     = isset( $instance['th_close'] ) ? (bool) $instance['th_close'] : false;
		$f_close     = isset( $instance['f_close'] ) ? (bool) $instance['f_close'] : false;
		$s_close     = isset( $instance['s_close'] ) ? (bool) $instance['s_close'] : false;
		$su_close     = isset( $instance['su_close'] ) ? (bool) $instance['su_close'] : false;
		
		$time_format = get_option( 'time_format' );
		
		$find = array('H','h','i','a', 'A','G','g');
		$replace = array('HH','hh','mm','tt','TT','H','h');
				
		$timeform = (isset($time_format)) ? str_replace($find, $replace, $time_format) : '';
		
		
		 ?>
		
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title :</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		<p>
			<label style="width: 77px;">Monday :</label>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('m_b'); ?>" name="<?php echo $this->get_field_name('m_b'); ?>" value="<?php echo $instance['m_b']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('m_e'); ?>" name="<?php echo $this->get_field_name('m_e'); ?>" value="<?php echo $instance['m_e']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input id="<?php echo $this->get_field_id( 'm_close' ); ?>" class="checkbox" type="checkbox" <?php echo checked( $m_close ); ?> name="<?php echo $this->get_field_name( 'm_close' ); ?>" /> Close?
		</p>
		<p>
			<label>Tuesday :</label>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('t_b'); ?>" name="<?php echo $this->get_field_name('t_b'); ?>" value="<?php echo $instance['t_b']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('t_e'); ?>" name="<?php echo $this->get_field_name('t_e'); ?>" value="<?php echo $instance['t_e']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input id="<?php echo $this->get_field_id( 't_close' ); ?>" class="checkbox" type="checkbox" <?php echo checked( $t_close ); ?> name="<?php echo $this->get_field_name( 't_close' ); ?>" /> Close?
		</p>
		<p>
			<label>Wednesday :</label>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('w_b'); ?>" name="<?php echo $this->get_field_name('w_b'); ?>" value="<?php echo $instance['w_b']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('w_e'); ?>" name="<?php echo $this->get_field_name('w_e'); ?>" value="<?php echo $instance['w_e']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input id="<?php echo $this->get_field_id( 'w_close' ); ?>" class="checkbox" type="checkbox" <?php echo checked( $w_close ); ?> name="<?php echo $this->get_field_name( 'w_close' ); ?>" /> Close?
		</p>
		<p>
			<label>Thursday :</label>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('th_b'); ?>" name="<?php echo $this->get_field_name('th_b'); ?>" value="<?php echo $instance['th_b']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('th_e'); ?>" name="<?php echo $this->get_field_name('th_e'); ?>" value="<?php echo $instance['th_e']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input id="<?php echo $this->get_field_id( 'th_close' ); ?>" class="checkbox" type="checkbox" <?php echo checked( $th_close ); ?> name="<?php echo $this->get_field_name( 'th_close' ); ?>" /> Close?
		</p>
		<p>
			<label>Friday :</label>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('f_b'); ?>" name="<?php echo $this->get_field_name('f_b'); ?>" value="<?php echo $instance['f_b']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('f_e'); ?>" name="<?php echo $this->get_field_name('f_e'); ?>" value="<?php echo $instance['f_e']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input id="<?php echo $this->get_field_id( 'f_close' ); ?>" class="checkbox" type="checkbox" <?php echo checked( $f_close ); ?> name="<?php echo $this->get_field_name( 'f_close' ); ?>" /> Close?
		</p>
		<p>
			<label>Saturday :</label>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('s_b'); ?>" name="<?php echo $this->get_field_name('s_b'); ?>" value="<?php echo $instance['s_b']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('s_e'); ?>" name="<?php echo $this->get_field_name('s_e'); ?>" value="<?php echo $instance['s_e']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input id="<?php echo $this->get_field_id( 's_close' ); ?>" class="checkbox" type="checkbox" <?php echo checked( $s_close ); ?> name="<?php echo $this->get_field_name( 's_close' ); ?>" /> Close?
		</p>
		<p>
			<label>Sunday :</label>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('su_b'); ?>" name="<?php echo $this->get_field_name('su_b'); ?>" value="<?php echo $instance['su_b']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input class="widefat wtimepicker" style="width: 77px;" data-format="<?php echo $timeform; ?>" id="<?php echo $this->get_field_id('su_e'); ?>" name="<?php echo $this->get_field_name('su_e'); ?>" value="<?php echo $instance['su_e']; ?>" />
			 <span class="add-on"><i class="icon-time"></i></span>
			<input id="<?php echo $this->get_field_id( 'su_close' ); ?>" class="checkbox" type="checkbox" <?php echo checked( $su_close ); ?> name="<?php echo $this->get_field_name( 'su_close' ); ?>" /> Close?
		</p>

			<p><label for="<?php echo $this->get_field_id( 'working_day_color' ); ?>" style="display:block;"><?php _e( 'Working day color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'working_day_color' ); ?>" name="<?php echo $this->get_field_name( 'working_day_color' ); ?>" type="text" value="<?php echo $instance['working_day_color']; ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'day_off_color' ); ?>" style="display:block;"><?php _e( 'Day off color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'day_off_color' ); ?>" name="<?php echo $this->get_field_name( 'day_off_color' ); ?>" type="text" value="<?php echo $instance['day_off_color']; ?>" /></p>
		

	<?php
	}
}
?>