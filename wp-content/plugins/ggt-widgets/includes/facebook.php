<?php 

class ggt_facebook_widget extends WP_Widget{
    
	function ggt_facebook_widget()
	{
		global $ukey;
		$this->ukey = $ukey;
		$widget_ops = array('classname' => 'ggt-facebook-feed', 'description' => __('', $this->ukey) );
		$this->WP_Widget('ggt-fb-feed', __('GGT Social Widgets: Facebook', $this->ukey), $widget_ops);
	}
	
	    function widget($args, $instance) {
	    
	      wp_enqueue_style( 'ggt_swidget_extend_style', plugins_url('/../assets/css/custom.css', __FILE__) );
	      wp_enqueue_style( 'ggt_swidget_font-ggtcontrols', plugins_url('/../assets/css/font-ggtcontrols.css', __FILE__) );

	      wp_enqueue_style('thickbox');
	
	      wp_enqueue_script( 'ggt_swidget_extend_js', plugins_url('/../assets/js/custom.js', __FILE__), array('jquery') );
	      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
	      wp_enqueue_script( 'ggt_swidget_jquery-expand', plugins_url('/../assets/js/expand.min.js', __FILE__), array('jquery') );

	    
        extract($args, EXTR_SKIP);
        extract($instance);
       
		$title = empty($title) ? 'GGT Facebook Widget' : apply_filters('widget_title', $title);
		
        if(empty($fb_url)) $fb_url = "http://www.facebook.com/FacebookDevelopers";
        if(empty($width)) $width = "auto";
        if(empty($height)) $height = "600";
        if(empty($color_scheme)) $color_scheme = "0";
                        
        $face = ($face > 0) ? 'true' : 'false';
        $header = ($header > 0) ? 'true' : 'false';
        $border = ($border > 0) ? 'true' : 'false';
        $post = ($post > 0) ? 'true' : 'false';
        
        if(empty($backgroundColor)) $backgroundColor = "#1e9f75";
        if(empty($margin_css)) $margin_css = "10px 0px";
        if(empty($padding_css)) $padding_css = "0px";
        if(empty($border_css)) $border_css = "1px solid #c7c7c7";
        if(empty($box_shadow)) $box_shadow = "0px 0px 14px #000000";
        
        echo $before_widget;
        if(!empty($title)) { echo $before_title . $title . $after_title; };
            
            ?>

<style>
    
    .fb-like-box {
				background:<?php echo $backgroundColor; ?>;
				padding:<?php echo $padding_css; ?>;
				margin:<?php echo $margin_css; ?>;
				border:<?php echo $border_css; ?>;
				-webkit-box-shadow: <?php echo $box_shadow; ?>;
				-moz-box-shadow: <?php echo $box_shadow; ?>;
				box-shadow: <?php echo $box_shadow; ?>;
               }
</style>
  <div id="ggt-fb-root"></div>
	<script>
	jQuery(document).ready(function() { 
		<?php if($width == 'auto') { ?>
		jQuery(".ggt_fb_widget .fb-like-box").attr('data-width', parseFloat(jQuery(".ggt_fb_widget .fb-like-box").width()));
		<?php } ?>
	    if (typeof jQuery.fn.waypoint !== "undefined") {
			jQuery("#ggt-fb-root").waypoint(function () {
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.1&appId=312960125571795";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			}, {triggerOnce: true, offset: 'bottom-in-view'});
		}
	});
	</script>
        
         <div class="ggt_fb_widget">

	     <div class="fb-like-box"
		   
		   data-href="<?php echo $fb_url;?>"
           data-width="<?php echo ($width == 'auto') ? '': $width;?>" 
           data-height="<?php echo $height;?>"
           
          <?php if($color_scheme == '0') {?>
                data-colorscheme="light"
             <?php } else {?> 
                data-colorscheme="dark"
             <?php };?>
             
               data-show-faces="<?php echo $face; ?>"
               data-header="<?php echo $header; ?>" 
               data-show-border="<?php echo $border; ?>"
               data-stream="<?php echo $post; ?>" 
        </div>
	</div>

<?php
        echo $after_widget;
    }

    
    function form($instance) {
        extract($instance);
        
        ?>
<!-- here will put all widget configuration -->
<p>
    <label for="<?php echo $this->get_field_id('title');?>">Title : </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('title');?>"
	name="<?php echo $this->get_field_name('title');?>"
        value="<?php echo !empty($title) ? $title : "GGT Facebook Widget"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('fb_url');?>">Facebook Page URL : </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('fb_url');?>"
	name="<?php echo $this->get_field_name('fb_url');?>"
        value="<?php echo !empty($fb_url) ? $fb_url : "https://www.facebook.com/FacebookDevelopers"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('width');?>">Width (in px or 'auto') : </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('width');?>"
	name="<?php echo $this->get_field_name('width');?>"
        value="<?php echo !empty($width) ? $width : "auto"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('height');?>">Height (in px): </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('height');?>"
	name="<?php echo $this->get_field_name('height');?>"
        value="<?php echo !empty($height) ? $height : "600"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'color_scheme' ); ?>">Color Scheme:</label> 
    <select id="<?php echo $this->get_field_id( 'color_scheme' ); ?>"
        name="<?php echo $this->get_field_name( 'color_scheme' ); ?>"
        class="widefat" style="width:100%;">
            <option value="0" <?php if (isset($color_scheme) && $color_scheme == '0') echo 'selected="0"'; ?> >Light</option>
            <option value="1" <?php if (isset($color_scheme) && $color_scheme == '1') echo 'selected="1"'; ?> >Dark</option>	
    </select>
</p>

<p>
    <label for="<?php echo $this->get_field_id( 'face' ); ?>">Show Faces:</label>
    <input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('face'); ?>" <?php echo $nface = (isset($face) && $face) ? 'checked="checked"' : ''; ?> />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'header' ); ?>">Show Header:</label> 
    <input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('header'); ?>" <?php echo $nheader = (isset($header) && $header) ? 'checked="checked"' : ''; ?> />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'border' ); ?>">Show Border:</label> 
    <input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('border'); ?>" <?php echo $nborder = (isset($border) && $border) ? 'checked="checked"' : ''; ?> />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'post' ); ?>">Show Post:</label> 
    <input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('post'); ?>" <?php echo $npost = (isset($post) && $post) ? 'checked="checked"' : ''; ?> />
</p>
<div style="color:#fff; font-size:12px; font-weight:bold; padding:3px; margin:0; text-align:center; background:#33302e;">CSS Customization</div>
<p>
    <label for="<?php echo $this->get_field_id('backgroundColor');?>">Background Color: </label>
    <input
	class="widefat color-picker"
	id="<?php echo $this->get_field_id('backgroundColor');?>"
	name="<?php echo $this->get_field_name('backgroundColor');?>"
        value="<?php echo !empty($backgroundColor) ? $backgroundColor : "#1e9f75"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('margin_css');?>">Margin: </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('margin_css');?>"
	name="<?php echo $this->get_field_name('margin_css');?>"
        value="<?php echo !empty($margin_css) ? $margin_css : "10px 0px"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('padding_css');?>">Padding: </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('padding_css');?>"
	name="<?php echo $this->get_field_name('padding_css');?>"
        value="<?php echo !empty($padding_css) ? $padding_css : "0px"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('border_css');?>">Border CSS: </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('border_css');?>"
	name="<?php echo $this->get_field_name('border_css');?>"
        value="<?php echo !empty($border_css) ? $border_css : "1px solid #c7c7c7"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('box_shadow');?>">Box Shadow: </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('box_shadow');?>"
	name="<?php echo $this->get_field_name('box_shadow');?>"
        value="<?php echo !empty($box_shadow) ? $box_shadow : "0px 0px 14px #000000"; ?>" />
</p>
<?php
    }
    
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['fb_url'] = strip_tags($new_instance['fb_url']);
		$instance['width'] = (int)$new_instance['width'];
		$instance['height'] = (int)$new_instance['height'];
		$instance['face'] = $new_instance['face'] ? 1 : 0;
		$instance['header'] = $new_instance['header'] ? 1 : 0;
		$instance['border'] = $new_instance['border'] ? 1 : 0;
		$instance['post'] = $new_instance['post'] ? 1 : 0;
		$instance['backgroundColor'] = strip_tags($new_instance['backgroundColor']);
		$instance['margin_css'] = strip_tags($new_instance['margin_css']);
		$instance['padding_css'] = strip_tags($new_instance['padding_css']);
		$instance['border_css'] = strip_tags($new_instance['border_css']);
		$instance['box_shadow'] = strip_tags($new_instance['box_shadow']);
		
		if ( in_array( $new_instance['color_scheme'], array( 0, 1 ) ) ) {
			$instance['color_scheme'] = $new_instance['color_scheme'];
		} else {
			$instance['color_scheme'] = 0;
		}

		
		return $instance;
	}

}