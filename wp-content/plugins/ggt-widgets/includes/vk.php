<?php

class ggt_vk_widget extends WP_Widget
{
   

    function __construct()
    {
    
     global $ukey;
     
	   $plugin_domain = $ukey;
    
       // load_plugin_textdomain('', false, dirname(plugin_basename(__FILE__)) . '/lang/');
        $widget_ops = array(
            'classname' => 'ggt_widget_vkapi',
            'description' => __('Information about VKontakte group')
        );
        parent::WP_Widget(
            'ggt_vk_widget',
            $name = 'GGT Social Widgets: ' . __('VK Community Users'),
            $widget_ops
        );
    }

    function widget($args, $instance)
    {
    
          wp_enqueue_style( 'ggt_swidget_extend_style', plugins_url('/../assets/css/custom.css', __FILE__) );
          wp_enqueue_style( 'ggt_swidget_font-ggtcontrols', plugins_url('/../assets/css/font-ggtcontrols.css', __FILE__) );
	      wp_enqueue_style('thickbox');
	
	      wp_enqueue_script( 'ggt_swidget_extend_js', plugins_url('/../assets/js/custom.js', __FILE__), array('jquery') );
	      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
	      wp_enqueue_script( 'ggt_swidget_jquery-expand', plugins_url('/../assets/js/expand.min.js', __FILE__), array('jquery') );

        extract($args);
        $vkapi_divid = $args['widget_id'];
        $vkapi_mode = 2;
        $vkapi_gid = filter_var($instance['gid'], FILTER_SANITIZE_NUMBER_INT);
        $vkapi_width = $instance['width'];
        if ((int)$vkapi_width < 1) {
            $vkapi_width = 'width: "auto",';
        } else {
            $vkapi_width = "width: \"".(int)$vkapi_width."\",";
        }
        $vkapi_height = (int)$instance['height'];
        if ($instance['type'] == 'users') {
            $vkapi_mode = 0;
        }
        if ($instance['type'] == 'news') {
            $vkapi_mode = 2;
        }
        if ($instance['type'] == 'name') {
            $vkapi_mode = 1;
        }
                
        $backgroundColor = (empty($instance['backgroundColor'])) ? "#000000" : $instance['backgroundColor'];
        $textColor = (empty($instance['textColor'])) ? "#000000" : $instance['textColor'];
        $BttnColor = (empty($instance['BttnColor'])) ? "#cccccc" : $instance['BttnColor'];
        $margin_css = (empty($instance['margin_css'])) ? "10px 0px" : $instance['margin_css'];
        $padding_css = (empty($instance['padding_css'])) ? "0px" : $instance['padding_css'];
        $border_css = (empty($instance['border_css'])) ? "1px solid #c7c7c7" : $instance['border_css'];
        $box_shadow = (empty($instance['box_shadow'])) ? "0px 0px 14px #000000" : $instance['box_shadow'];
        
		$vkapi_divid_n = 0;

/*         print_r($backgroundColor); */

        /** @var $before_widget string */
        /** @var $before_title string */
        /** @var $after_title string */
        /** @var $after_widget string */
       
        echo $before_widget . $before_title . $instance['title'] . $after_title; 
        
        if($vkapi_gid > 0) {
        
         ?>
        <style>
    
    #<?php echo $vkapi_divid . '_wrapper'; ?> {
				padding:<?php echo $padding_css; ?>;
				margin:<?php echo $margin_css; ?>;
				border:<?php echo $border_css; ?>;
				-webkit-box-shadow: <?php echo $box_shadow; ?>;
				-moz-box-shadow: <?php echo $box_shadow; ?>;
				box-shadow: <?php echo $box_shadow; ?>;
               }
		</style>

<?php
        
        echo '<div id="' . $vkapi_divid . '_wrapper">';
        $vkapi_divid .= "_wrapper";
        $vkapi_divid_n = str_replace('-','',filter_var($vkapi_divid, FILTER_SANITIZE_NUMBER_INT));
                
        echo '</div>
		<script type="text/javascript">
		    function VK_Widgets_Group'.(int)$vkapi_divid_n.'() {
	            if (typeof VK != "undefined") {
					          VK.Widgets.Group("' . $vkapi_divid . '", {mode: ' . $vkapi_mode . ', ' . $vkapi_width . ' height: "' . $vkapi_height . '", color1: "' . rgba2html($backgroundColor) . '", color2: "' . rgba2html($textColor) . '", color3: "' . rgba2html($BttnColor) . '"}, ' . $vkapi_gid . ');
			    } else {
			        setTimeout(VK_Widgets_Group'.(int)$vkapi_divid_n.',1000);
			    }
			}
			jQuery(document).ready(function() { 
			    if (typeof jQuery.fn.waypoint !== "undefined") {
					jQuery("#'.$vkapi_divid.'").waypoint(function () {
						VK_Widgets_Group'.(int)$vkapi_divid_n.'();
					}, {triggerOnce: true, offset: \'bottom-in-view\'});
				}
			});
		</script>';
		}
        echo $after_widget;
    }

    function update($new_instance, $old_instance)
    {
        return $new_instance;
    }

    function form($instance)
    {
        $instance = wp_parse_args(
            (array)$instance,
            array('type' => 'users', 'title' => '', 'width' => '220', 'height' => '400', 'gid' => '1', 'backgroundColor' => '#000000', 'textColor' => '#000000', 'BttnColor' => '#cccccc', 'margin_css' => '10px 0px', 'padding_css' => '0px', 'border_css' => '1px solid #c7c7c7', 'box_shadow' => '0px 0px 14px #000000', )
        );
        $title = esc_attr($instance['title']);
        $gid = esc_attr($instance['gid']);
        $width = esc_attr($instance['width']);
        $height = esc_attr($instance['height']);
        $backgroundColor = esc_attr($instance['backgroundColor']);
        $textColor = esc_attr($instance['textColor']);
        $BttnColor = esc_attr($instance['BttnColor']);
        $margin_css = esc_attr($instance['margin_css']);
        $padding_css = esc_attr($instance['padding_css']);
        $border_css = esc_attr($instance['border_css']);
        $box_shadow = esc_attr($instance['box_shadow']);


        ?>

<!-- here will put all widget configuration -->

<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?>
            <input class="widefat"
                   id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>"
                   type="text"
                   value="<?php echo $title; ?>"/>
        </label></p>

        <p><label for="<?php echo $this->get_field_id('gid'); ?>"><?php _e(
                    'ID of group (can be seen by reference to statistics, only number):',
                    ''
                ); ?>
                <input class="widefat"
                       id="<?php echo $this->get_field_id('gid'); ?>"
                       name="<?php echo $this->get_field_name('gid'); ?>"
                       type="text"
                       value="<?php echo $gid; ?>"/>
            </label></p>

        <p><label for="<?php echo $this->get_field_id('width'); ?>"><?php _e('Width (in px or \'auto\'):'); ?>
                <input class="widefat"
                       id="<?php echo $this->get_field_id('width'); ?>"
                       name="<?php echo $this->get_field_name('width'); ?>"
                       type="text"
                       value="<?php echo $width; ?>"/>
            </label></p>

        <p><label for="<?php echo $this->get_field_id('height'); ?>"><?php _e('Height (in px or \'auto\'):'); ?>
                <input class="widefat"
                       id="<?php echo $this->get_field_id('height'); ?>"
                       name="<?php echo $this->get_field_name('height'); ?>"
                       type="text"
                       value="<?php echo $height; ?>"/>
            </label></p>

        <p>
            <label for="<?php echo $this->get_field_id('type'); ?>"><?php _e(
                    'Layout:',
                    ''
                ); ?></label>
            <select name="<?php echo $this->get_field_name('type'); ?>"
                    id="<?php echo $this->get_field_id('type'); ?>"
                    class="widefat">
                <option value="users"<?php selected($instance['type'], 'users'); ?>><?php _e(
                        'Members',
                        ''
                    ); ?></option>
                <option value="news"<?php selected($instance['type'], 'news'); ?>><?php _e(
                        'News',
                        ''
                    ); ?></option>
                <option value="name"<?php selected($instance['type'], 'name'); ?>><?php _e(
                        'Only Name',
                        ''
                    ); ?></option>
            </select>
        </p>
        
        <div style="color:#fff; font-size:12px; font-weight:bold; padding:3px; margin:0; text-align:center; background:#33302e;">CSS Customization</div>
<p>
    <label for="<?php echo $this->get_field_id('backgroundColor');?>" style="display:block;">Background Color: </label>
    <input
	class="widefat color-picker"
	id="<?php echo $this->get_field_id('backgroundColor');?>"
	name="<?php echo $this->get_field_name('backgroundColor');?>"
        value="<?php echo !empty($backgroundColor) ? $backgroundColor : "#1e9f75"; ?>" />
</p>

<p>
    <label for="<?php echo $this->get_field_id('textColor');?>" style="display:block;">Text Color: </label>
    <input
	class="widefat color-picker"
	id="<?php echo $this->get_field_id('textColor');?>"
	name="<?php echo $this->get_field_name('textColor');?>"
        value="<?php echo !empty($textColor) ? $textColor : "#000000"; ?>" />
</p>

<p>
    <label for="<?php echo $this->get_field_id('BttnColor');?>" style="display:block;">Buttons Color: </label>
    <input
	class="widefat color-picker"
	id="<?php echo $this->get_field_id('BttnColor');?>"
	name="<?php echo $this->get_field_name('BttnColor');?>"
        value="<?php echo !empty($BttnColor) ? $BttnColor : "#cccccc"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('margin_css');?>">Margin: </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('margin_css');?>"
	name="<?php echo $this->get_field_name('margin_css');?>"
        value="<?php echo !empty($margin_css) ? $margin_css : "10px 0px"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('padding_css');?>">Padding: </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('padding_css');?>"
	name="<?php echo $this->get_field_name('padding_css');?>"
        value="<?php echo !empty($padding_css) ? $padding_css : "0px"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('border_css');?>">Border CSS: </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('border_css');?>"
	name="<?php echo $this->get_field_name('border_css');?>"
        value="<?php echo !empty($border_css) ? $border_css : "1px solid #c7c7c7"; ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id('box_shadow');?>">Box Shadow: </label>
    <input
	class="widefat"
	id="<?php echo $this->get_field_id('box_shadow');?>"
	name="<?php echo $this->get_field_name('box_shadow');?>"
        value="<?php echo !empty($box_shadow) ? $box_shadow : "0px 0px 14px #000000"; ?>" />
</p>

    <?php
    }
}


function ggt_vk_script() {
      wp_enqueue_script( 'vkjs', 'http://vk.com/js/api/openapi.js', '' );
}

function rgba2html($rgbas)
{
$color = str_replace(array('rgb(', ')', ' '), '', $rgbas);
$arr = explode(',', $color);

$r = $g = $b = '';

$rgba = $arr;

    if (is_array($rgba) && sizeof($rgba) == 3) // ==4
        list($r, $g, $b) = $rgba; // , $a

    $r = intval($r);
    $g = intval($g);
    $b = intval($b);

    $r = dechex($r<0?0:($r>255?255:$r));
    $g = dechex($g<0?0:($g>255?255:$g));
    $b = dechex($b<0?0:($b>255?255:$b));

    $color = (strlen($r) < 2?'0':'').$r;
    $color .= (strlen($g) < 2?'0':'').$g;
    $color .= (strlen($b) < 2?'0':'').$b;
    return '#'.$color;
}

add_action( 'wp_enqueue_scripts', 'ggt_vk_script' );
    