<?php

class ggt_recentcomments_widget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'ggt_recentcomments_widget_entries', 'description' => __( "") );
		parent::__construct('ggt_recentcomments_widget', __('GGT Recent Comments'), $widget_ops);
		$this->alt_option_name = 'ggt_recentcomments_widget_entries';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
		add_action( 'wp_enqueue_scripts', array($this, 'addstyle') );

	}
	
	function addstyle() {
	  wp_enqueue_style( 'recentbox_style', plugins_url('../assets/css/recentbox.css', __FILE__ ));
	}


	function widget($args, $instance) {
		
      wp_enqueue_style( 'ggt_swidget_font-ggtcontrols', plugins_url('/../assets/css/font-ggtcontrols.css', __FILE__) );
      wp_enqueue_style( 'ggt_swidget_owlcss', plugins_url('/../assets/css/owl.carousel.css', __FILE__) );

      
      	  wp_enqueue_style( 'ggt_swidget_extend_style', plugins_url('/../assets/css/custom.css', __FILE__) );
	      wp_enqueue_style('thickbox');
	
	      wp_enqueue_script( 'ggt_swidget_extend_js', plugins_url('/../assets/js/custom.js', __FILE__), array('jquery') );
	      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
	      wp_enqueue_script( 'ggt_swidget_jquery-expand', plugins_url('/../assets/js/expand.min.js', __FILE__), array('jquery') );
	      wp_enqueue_script( 'ggt_swidget_owljs', plugins_url('/../assets/js/owl.carousel.js', __FILE__), array('jquery') );
	      wp_enqueue_script( 'ggt_swidget_carousel_js', plugins_url('/../assets/js/carousel.js', __FILE__), array('jquery', 'ggt_swidget_owljs', 'ggt_swidget_jquery-expand') );

		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'ggt_recentcomments_widget', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Comments' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
				
		$number = ( ! empty( $instance['number'] )) ? absint( $instance['number'] ) : 5;
		
		$slider     = ( ! empty( $instance['slider'] )) ? (bool) $instance['slider'] : false;
		$autoplay     = ( ! empty( $instance['autoplay'] )) ? (bool) $instance['autoplay'] : false;
		$scrolltimeout     = ( ! empty( $instance['scrolltimeout'] )) ? (int) $instance['scrolltimeout'] : 3000;
		$hide_nav_bttns     = ( ! empty( $instance['hide_nav_bttns'] )) ? (bool) $instance['hide_nav_bttns'] : false;

		$prev_bttn     = ( ! empty( $instance['prev_bttn'] )) ? esc_attr( $instance['prev_bttn'] ) : '';
		$next_bttn     = ( ! empty( $instance['next_bttn'] )) ? esc_attr( $instance['next_bttn'] ) : '';
		
		$show_thumb     = ( ! empty( $instance['show_thumb'] )) ? (bool) $instance['show_thumb'] : false;
		$show_date 		= ( ! empty( $instance['show_date'] )) ? (bool) $instance['show_date'] : false;
		$show_author     = ( ! empty( $instance['show_author'] )) ? (bool) $instance['show_author'] : false;
		$show_expand     = ( ! empty( $instance['show_expand'] )) ? (bool) $instance['show_expand'] : false;
		$expand_size     = ( ! empty( $instance['expand_size'] )) ? absint( $instance['expand_size'] ) : 150;
		$readmore_text     = ( ! empty( $instance['readmore_text'] )) ? esc_attr( $instance['readmore_text'] ) : 'More';
		$css_classes     = ( ! empty( $instance['css_classes'] )) ? esc_attr( $instance['css_classes'] ) : ''; 

		$skin = ( ! empty( $instance['skin'] )) ? esc_attr( $instance['skin'] ) : ''; 
		$title_color     = ( ! empty( $instance['title_color'] )) ? esc_attr( $instance['title_color'] ) : '';
		$custom_bg_color     = ( ! empty( $instance['custom_bg_color'] )) ? esc_attr( $instance['custom_bg_color'] ) : '';
		$custom_items_bg_color     = ( ! empty( $instance['custom_items_bg_color'] )) ? esc_attr( $instance['custom_items_bg_color'] ) : '';
		$custom_items_title_color     = ( ! empty( $instance['custom_items_title_color'] )) ? esc_attr( $instance['custom_items_title_color'] ) : '';
		$custom_items_title_hover_color = ( ! empty( $instance['custom_items_title_hover_color'] )) ? esc_attr( $instance['custom_items_title_hover_color'] ) : '';
		$custom_items_title_font_size     = ( ! empty( $instance['custom_items_title_font_size'] )) ? esc_attr( $instance['custom_items_title_font_size'] ) : '';
		$custom_items_title_font     = ( ! empty( $instance['custom_items_title_font'] )) ? esc_attr( $instance['custom_items_title_font'] ) : '';
		$custom_items_text_color     = ( ! empty( $instance['custom_items_text_color'] )) ? esc_attr( $instance['custom_items_text_color'] ) : '';
		$custom_items_text_font_size     = ( ! empty( $instance['custom_items_text_font_size'] )) ? esc_attr( $instance['custom_items_text_font_size'] ) : '';
		$custom_items_text_font_family   = ( ! empty( $instance['custom_items_text_font_family'] )) ? esc_attr( $instance['custom_items_text_font_family'] ) : '';
		$custom_items_bttn_bg_color     = ( ! empty( $instance['custom_items_bttn_bg_color'] )) ? esc_attr( $instance['custom_items_bttn_bg_color'] ) : '';
		$custom_items_bttn_hover_bg_color = ( ! empty( $instance['custom_items_bttn_hover_bg_color'] )) ? esc_attr( $instance['custom_items_bttn_hover_bg_color'] ) : '';
		$custom_items_bttn_link_color     = ( ! empty( $instance['custom_items_bttn_link_color'] )) ? esc_attr( $instance['custom_items_bttn_link_color'] ) : '';
		$custom_items_bttn_link_hover_color = ( ! empty( $instance['custom_items_bttn_link_hover_color'] )) ? esc_attr( $instance['custom_items_bttn_link_hover_color'] ) : '';
		$custom_items_border_style     = ( ! empty( $instance['custom_items_border_style'] )) ? esc_attr( $instance['custom_items_border_style'] ) : '';
		$custom_items_border_radius     = ( ! empty( $instance['custom_items_border_radius'] )) ? esc_attr( $instance['custom_items_border_radius'] ) : '';
		$custom_items_bttn_border_radius     = ( ! empty( $instance['custom_items_bttn_border_radius'] )) ? esc_attr( $instance['custom_items_bttn_border_radius'] ) : '';
		$custom_image_border_radius     = ( ! empty( $instance['custom_image_border_radius'] )) ? esc_attr( $instance['custom_image_border_radius'] ) : '';

		$slider_item_w     = ( ! empty( $instance['slider_item_w'] )) ? esc_attr( $instance['slider_item_w'] ) : '240';
		$slider_item_h     = ( ! empty( $instance['slider_item_h'] )) ? esc_attr( $instance['slider_item_h'] ) : '125';
		 
		/**
		 * Filter the arguments for the Recent Posts widget.
		 *
		 * @since 3.4.0
		 *
		 * @see WP_Query::get_posts()
		 *
		 * @param array $args An array of arguments used to retrieve the recent posts.
		 */
		 
		   $comments = get_comments( apply_filters( 'widget_comments_args', array(
			'number'      => $number,
			'status'      => 'approve',
			'post_status' => 'publish',
		) ) );

			$uid = uniqid();

		$add_class_att = $add_att = $mbefore = $mafter = '';
		
	   echo $before_widget;

	//	print_r('<pre>');print_r($comments);print_r('</pre>');
		if ($comments) :
		
		 if ($title) {
	            echo $before_title.'<span class="widget-title-ccolor"'.($title_color != '' ? ' style="color:'.$title_color.';"' : '').'>'. $title .'</span>'. $after_title;
	        }
		
				
		$add_class_att .= ' '.$css_classes.' '.$skin.''; 

		if ($slider) {

			$add_class_att .= ' inowlcarousel';
						
			$add_att = ' data-navtext="'.($hide_nav_bttns ? 1 : 0).'" '.($autoplay ? 'data-autoplay="1" data-scroll-timeout="'.$scrolltimeout.'"' : '').(($next_bttn != '') && ($prev_bttn != '') ? ' data-navprev="'.$prev_bttn.'" data-navnext="'.$next_bttn.'"' : '').'';
						
			$mbefore = '<div id="' . $uid . '">';
			$mafter = '</div>';
			
		}
		
		if ($skin == 'custom') { ?>
			<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery('#<?php echo $uid; ?>').css({
					'background' : '<?php echo $custom_bg_color; ?>',
					});
				jQuery('#<?php echo $uid; ?> li').css({
					'background-color' : '<?php echo $custom_items_bg_color; ?>',
					'border' : '<?php echo $custom_items_border_style; ?>',
				    'border-radius' : '<?php echo $custom_items_border_radius; ?>',
				    'border-width' : '1px',
					'color' : '<?php echo $custom_items_text_color; ?>',
					'font-size' : '<?php echo (int)$custom_items_text_font_size; ?>px',
					'font-family' : '<?php echo $custom_items_text_font_family; ?>',
					});
					
				jQuery('#<?php echo $uid; ?> .post-title').css({
					'color' : '<?php echo $custom_items_title_color; ?>',
					'font-size' : '<?php echo (int)$custom_items_title_font_size; ?>px',
				    'font-family' : '<?php echo $custom_items_title_font; ?>',
					});
				jQuery('#<?php echo $uid; ?> .post-title').hover(function(){
					jQuery(this).css({
						'color' : '<?php echo $custom_items_title_hover_color; ?>',
						'font-size' : '<?php echo $custom_items_title_font_size; ?>',
					    'font-family' : '<?php echo $custom_items_title_font; ?>',});
					 },function(){
					jQuery(this).css({
						'color' : '<?php echo $custom_items_title_color; ?>',
						'font-size' : '<?php echo $custom_items_title_font_size; ?>',
					    'font-family' : '<?php echo $custom_items_title_font; ?>',});
				  	});
				jQuery('#<?php echo $uid; ?> .post-readmore').css({
					'color' : '<?php echo $custom_items_bttn_link_color; ?>',
					'background' : '<?php echo $custom_items_bttn_bg_color; ?>',
				    'border-radius' : '<?php echo $custom_items_bttn_border_radius; ?>',
				    'border-width' : '1px',
					});
				jQuery('#<?php echo $uid; ?> .post-readmore').hover(function(){
					jQuery(this).css({
						'color' : '<?php echo $custom_items_bttn_link_color; ?>',
						'background' : '<?php echo $custom_items_bttn_bg_color; ?>',
					    'border-radius' : '<?php echo $custom_items_bttn_border_radius; ?>',
					    'border-width' : '1px',});
					 },function(){
					jQuery(this).css({
						'color' : '<?php echo $custom_items_bttn_hover_bg_color; ?>',
						'background' : '<?php echo $custom_items_bttn_link_hover_color; ?>',});
				  	});
				jQuery('#<?php echo $uid; ?> li a').css({
					'color' : '<?php echo $custom_items_text_color; ?>',
					'font-size' : '<?php echo $custom_items_text_font_size; ?>',
					'font-family' : '<?php echo $custom_items_text_font_family; ?>',
					});
				jQuery('#<?php echo $uid; ?> .ggtpost-thumb img').css({
				    'border-radius' : '<?php echo $custom_image_border_radius; ?>',
					});
			});
			</script>
			
			
		<?php }
				
		 ?>
		 
		 <?php echo $mbefore; ?>
		 
		<ul id="<?php echo $uid; ?>" class="recent-comments-cont <?php echo $add_class_att; ?>" <?php echo $add_att; ?>>
		
			<?php // Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
			$post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
			_prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );
			
			

		//print_r('<pre>');print_r($comments);print_r('</pre>');


			foreach ( (array) $comments as $comment) {
			
						 if ( $comment->user_id && $user = get_userdata( $comment->user_id ) ) {
	                        $author = $user->display_name;
	               } elseif (!empty( $comment->comment_author ) ) {$author =  $comment->comment_author;} else { $author = __('Anonymous');}
				   ?>

				<li class="recentcomments">
				
				<a class="post-title" href="<?php echo esc_url( get_comment_link($comment->comment_ID) ); ?>"><span><?php echo $comment->post_title; ?></span></a>
 				
                <?php if ( $show_author && $show_thumb ) : ?><span class="ggtpost-thumb"><?php echo get_avatar( $comment, 32 ); ?></span><?php endif; ?>
                
                <?php if ( $show_date ) : ?><span class="post-date"><?php echo human_time_diff( strtotime($comment->comment_date )).' '. __('ago', ''); ?></span><?php endif; ?>
                <?php if ( $show_author ) : ?><span class="post-author"><?php echo $author; ?></span><?php endif; ?>
                
                <?php 
                $expand_txt = expand ($comment->comment_content, $expand_size); 
                
/*                 print_r('<pre>');print_r($expand_txt);print_r('</pre>'); */
                
                if ( $show_expand && isset($expand_txt['full'])) { ?>
                <div class="post-comment-content"><?php echo  $expand_txt['short']; ?></div>
                <div class="post-comment-content post-expand-full"><?php echo  $expand_txt['full']; ?></div>
                <a href="#" class="post-readmore expander" data-expander-target=".post-expand-full"><?php echo  $readmore_text; ?></a>
                <?php } else { ?>
                <div class="post-comment-content"><?php echo $comment->comment_content; ?></div>
               <?php } ?>
								
				</li>
				

			<?php }

            ?>
		</ul>

<?php echo $mafter ?>
	
<?php endif; ?>

<?php  echo $after_widget; ?>

<?php
		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'ggt_recentcomments_widget', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		
		$instance['slider'] = isset( $new_instance['slider'] ) ? (bool) $new_instance['slider'] : false;
		$instance['autoplay'] = isset( $new_instance['autoplay'] ) ? (bool) $new_instance['autoplay'] : false;
		$instance['scrolltimeout'] = isset( $new_instance['scrolltimeout'] ) ? (int) $new_instance['scrolltimeout'] : 3000;
		$instance['hide_nav_bttns']     = isset( $new_instance['hide_nav_bttns'] ) ? (bool) $new_instance['hide_nav_bttns'] : false;
		$instance['prev_bttn'] = strip_tags($new_instance['prev_bttn']);
		$instance['next_bttn'] = strip_tags($new_instance['next_bttn']);

		$instance['show_thumb']     = isset( $new_instance['show_thumb'] ) ? (bool) $new_instance['show_thumb'] : false;
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		$instance['show_author'] = isset( $new_instance['show_author'] ) ? (bool) $new_instance['show_author'] : false;
		$instance['show_expand'] = isset( $new_instance['show_expand'] ) ? (bool) $new_instance['show_expand'] : false;
		$instance['expand_size'] = (int) $new_instance['expand_size'];
		$instance['readmore_text'] = strip_tags($new_instance['readmore_text']);
		$instance['css_classes'] = strip_tags($new_instance['css_classes']);
		$instance['skin'] = strip_tags($new_instance['skin']);
		$instance['custom_bg_color'] = strip_tags($new_instance['custom_bg_color']);
		$instance['title_color'] = strip_tags($new_instance['title_color']);
		$instance['custom_items_bg_color'] = strip_tags($new_instance['custom_items_bg_color']);
		$instance['custom_items_title_color'] = strip_tags($new_instance['custom_items_title_color']);
		$instance['custom_items_title_hover_color'] = strip_tags($new_instance['custom_items_title_hover_color']);
		$instance['custom_items_title_font_size'] = strip_tags($new_instance['custom_items_title_font_size']);
		$instance['custom_items_title_font'] = strip_tags($new_instance['custom_items_title_font']);
		$instance['custom_items_text_color'] = strip_tags($new_instance['custom_items_text_color']);
		$instance['custom_items_text_font_size'] = (int) $new_instance['custom_items_text_font_size'];
		$instance['custom_items_text_font_family'] = strip_tags($new_instance['custom_items_text_font_family']);
		$instance['custom_items_bttn_bg_color'] = strip_tags($new_instance['custom_items_bttn_bg_color']);
		$instance['custom_items_bttn_hover_bg_color'] = strip_tags($new_instance['custom_items_bttn_hover_bg_color']);
		$instance['custom_items_bttn_link_color'] = strip_tags($new_instance['custom_items_bttn_link_color']);
		$instance['custom_items_bttn_link_hover_color'] = strip_tags($new_instance['custom_items_bttn_link_hover_color']);
		$instance['custom_items_border_style'] = strip_tags($new_instance['custom_items_border_style']);
		$instance['custom_items_border_radius'] = strip_tags($new_instance['custom_items_border_radius']);
		$instance['custom_items_bttn_border_radius'] = strip_tags($new_instance['custom_items_bttn_border_radius']);
		$instance['custom_image_border_radius'] = strip_tags($new_instance['custom_image_border_radius']);
		$instance['slider_item_w'] = strip_tags($new_instance['slider_item_w']);
		$instance['slider_item_h'] = strip_tags($new_instance['slider_item_h']);
		
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['ggt_recentcomments_widget_entries']) )
			delete_option('ggt_recentcomments_widget_entries');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('ggt_recentcomments_widget', 'widget');
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$slider     = isset( $instance['slider'] ) ? (bool) $instance['slider'] : false;
		$autoplay     = isset( $instance['autoplay'] ) ? (bool) $instance['autoplay'] : false;
		$scrolltimeout     = isset( $instance['scrolltimeout'] ) ? (int) $instance['scrolltimeout'] : 3000;
		$hide_nav_bttns     = isset( $instance['hide_nav_bttns'] ) ? (bool) $instance['hide_nav_bttns'] : false;

		$prev_bttn     = isset( $instance['prev_bttn'] ) ? esc_attr( $instance['prev_bttn'] ) : '';
		$next_bttn     = isset( $instance['next_bttn'] ) ? esc_attr( $instance['next_bttn'] ) : '';
		
		$show_thumb     = isset( $instance['show_thumb'] ) ? (bool) $instance['show_thumb'] : false;
		$show_date 		= isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		$show_author     = isset( $instance['show_author'] ) ? (bool) $instance['show_author'] : false;
		$show_expand     = isset( $instance['show_expand'] ) ? (bool) $instance['show_expand'] : false;
		$expand_size     = isset( $instance['expand_size'] ) ? absint( $instance['expand_size'] ) : 150;
		$readmore_text     = isset( $instance['readmore_text'] ) ? esc_attr( $instance['readmore_text'] ) : '';
		$css_classes     = isset( $instance['css_classes'] ) ? esc_attr( $instance['css_classes'] ) : ''; 

		$custom_bg_color     = isset( $instance['custom_bg_color'] ) ? esc_attr( $instance['custom_bg_color'] ) : '';
		$title_color     = isset( $instance['title_color'] ) ? esc_attr( $instance['title_color'] ) : '';
		$custom_items_bg_color     = isset( $instance['custom_items_bg_color'] ) ? esc_attr( $instance['custom_items_bg_color'] ) : '';
		$custom_items_title_color     = isset( $instance['custom_items_title_color'] ) ? esc_attr( $instance['custom_items_title_color'] ) : '';
		$custom_items_title_hover_color = isset( $instance['custom_items_title_hover_color'] ) ? esc_attr( $instance['custom_items_title_hover_color'] ) : '';
		$custom_items_title_font_size     = isset( $instance['custom_items_title_font_size'] ) ? esc_attr( $instance['custom_items_title_font_size'] ) : '';
		$custom_items_title_font     = isset( $instance['custom_items_title_font'] ) ? esc_attr( $instance['custom_items_title_font'] ) : '';
		$custom_items_text_color     = isset( $instance['custom_items_text_color'] ) ? esc_attr( $instance['custom_items_text_color'] ) : '';
		$custom_items_text_font_size     = isset( $instance['custom_items_text_font_size'] ) ? esc_attr( $instance['custom_items_text_font_size'] ) : '';
		$custom_items_text_font_family   = isset( $instance['custom_items_text_font_family'] ) ? esc_attr( $instance['custom_items_text_font_family'] ) : '';
		$custom_items_bttn_bg_color     = isset( $instance['custom_items_bttn_bg_color'] ) ? esc_attr( $instance['custom_items_bttn_bg_color'] ) : '';
		$custom_items_bttn_hover_bg_color = isset( $instance['custom_items_bttn_hover_bg_color'] ) ? esc_attr( $instance['custom_items_bttn_hover_bg_color'] ) : '';
		$custom_items_bttn_link_color     = isset( $instance['custom_items_bttn_link_color'] ) ? esc_attr( $instance['custom_items_bttn_link_color'] ) : '';
		$custom_items_bttn_link_hover_color = isset( $instance['custom_items_bttn_link_hover_color'] ) ? esc_attr( $instance['custom_items_bttn_link_hover_color'] ) : '';
		$custom_items_border_style     = isset( $instance['custom_items_border_style'] ) ? esc_attr( $instance['custom_items_border_style'] ) : '';
		$custom_items_border_radius     = isset( $instance['custom_items_border_radius'] ) ? esc_attr( $instance['custom_items_border_radius'] ) : '';
		$custom_items_bttn_border_radius     = isset( $instance['custom_items_bttn_border_radius'] ) ? esc_attr( $instance['custom_items_bttn_border_radius'] ) : '';
		$custom_image_border_radius     = isset( $instance['custom_image_border_radius'] ) ? esc_attr( $instance['custom_image_border_radius'] ) : '';
		$slider_item_h   = isset( $instance['slider_item_h'] ) ? esc_attr( $instance['slider_item_h'] ) : '';
		$slider_item_w   = isset( $instance['slider_item_w'] ) ? esc_attr( $instance['slider_item_w'] ) : '';
		
?>

		<p class=""><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id( 'title_color' ); ?>" style="display:block;"><?php _e( 'Title color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'title_color' ); ?>" name="<?php echo $this->get_field_name( 'title_color' ); ?>" type="text" value="<?php echo esc_attr( $title_color ); ?>" /></p>


		<p class=""><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number to display:' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

        <div style="color:#fff; font-size:12px; font-weight:bold; padding:3px; margin:0; text-align:center; background:#33302e;">Slider options</div>
		
		<p><label for="<?php echo $this->get_field_id( 'slider' ); ?>"><?php _e('Slider?', ''); ?>: </label><input id="<?php echo $this->get_field_id( 'slider' ); ?>" class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('slider'); ?>" <?php checked( $slider ); ?> /></p>
		
		<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'slider' ); ?>:true"><label for="<?php echo $this->get_field_id( 'autoplay' ); ?>"><?php _e('AutoPlay?', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'autoplay' ); ?>" name="<?php echo $this->get_field_name('autoplay'); ?>" <?php checked( $autoplay ); ?> /></p>


		<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'slider' ); ?>:true;<?php echo $this->get_field_id( 'autoplay' ); ?>:true"><label for="<?php echo $this->get_field_id( 'scrolltimeout' ); ?>"><?php _e('Slider auto-scroll timeout', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'scrolltimeout' ); ?>" name="<?php echo $this->get_field_name( 'scrolltimeout' ); ?>" type="text" value="<?php echo $scrolltimeout; ?>" /></p>

<!--
			<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'slider' ); ?>:true"><label for="<?php echo $this->get_field_id( 'slider_item_h' ); ?>"><?php _e('Slider Item Height (in px):', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'slider_item_h' ); ?>" name="<?php echo $this->get_field_name( 'slider_item_h' ); ?>" type="text" value="<?php echo $slider_item_h; ?>" /></p>

			<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'slider' ); ?>:true"><label for="<?php echo $this->get_field_id( 'slider_item_w' ); ?>"><?php _e('Slider Item Width (in px):', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'slider_item_w' ); ?>" name="<?php echo $this->get_field_name( 'slider_item_w' ); ?>" type="text" value="<?php echo $slider_item_w; ?>" /></p>
-->

		<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'slider' ); ?>:true"><label for="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>"><?php _e('Hide Prev/Next buttons', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>" name="<?php echo $this->get_field_name('hide_nav_bttns'); ?>" <?php checked( $hide_nav_bttns ); ?> /></p>

<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>:false;<?php echo $this->get_field_id( 'slider' ); ?>:true">
            <label for="<?php echo $this->get_field_name( 'prev_bttn' ); ?>"><?php _e( 'Prev Button Image:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'prev_bttn' ); ?>" id="<?php echo $this->get_field_id( 'prev_bttn' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $prev_bttn ); ?>" />
            <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
        </p>


<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'hide_nav_bttns' ); ?>:false;<?php echo $this->get_field_id( 'slider' ); ?>:true">
            <label for="<?php echo $this->get_field_name( 'next_bttn' ); ?>"><?php _e( 'Next Button Image:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'next_bttn' ); ?>" id="<?php echo $this->get_field_id( 'next_bttn' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $next_bttn ); ?>" />
            <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
        </p>

        <div style="color:#fff; font-size:12px; font-weight:bold; padding:3px; margin:0; text-align:center; background:#33302e;">Widget Customization</div>
        
				
		<p><label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Show Date' ); ?>: </label><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
		</p>
				
				<p><label for="<?php echo $this->get_field_id( 'show_author' ); ?>"><?php _e('Show Author', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'show_author' ); ?>" name="<?php echo $this->get_field_name('show_author'); ?>" <?php checked( $show_author ); ?> /></p>
				
				<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'show_author' ); ?>:true"><label for="<?php echo $this->get_field_id( 'show_thumb' ); ?>"><?php _e('Show Author Avatar', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'show_thumb' ); ?>" name="<?php echo $this->get_field_name('show_thumb'); ?>" <?php checked( $show_thumb ); ?> /></p>


				<p><label for="<?php echo $this->get_field_id( 'show_expand' ); ?>"><?php _e('Show Expand', ''); ?>: </label><input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id( 'show_expand' ); ?>" name="<?php echo $this->get_field_name('show_expand'); ?>" <?php checked( $show_expand ); ?> /></p>
				
				<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'show_expand' ); ?>:true"><label for="<?php echo $this->get_field_id( 'expand_size' ); ?>"><?php _e('Words limit for Expand', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'expand_size' ); ?>" name="<?php echo $this->get_field_name( 'expand_size' ); ?>" type="text" value="<?php echo $expand_size; ?>" /></p>
				
				<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'show_expand' ); ?>:true"><label for="<?php echo $this->get_field_id( 'readmore_text' ); ?>"><?php _e('Expand link text', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'readmore_text' ); ?>" name="<?php echo $this->get_field_name( 'readmore_text' ); ?>" type="text" value="<?php echo $readmore_text; ?>" /></p>


		<p><label for="<?php echo $this->get_field_id( 'css_classes' ); ?>"><?php _e('CSS classes', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'css_classes' ); ?>" name="<?php echo $this->get_field_name( 'css_classes' ); ?>" type="text" value="<?php echo $css_classes; ?>" /></p>


        <p>
			<label for="<?php echo $this->get_field_id('skin'); ?>"><?php _e( 'Skin: ' ); ?></label>
			<select name="<?php echo $this->get_field_name('skin'); ?>" id="<?php echo $this->get_field_id('skin'); ?>" class="widefat">
				<option value="dark" <?php if (isset($instance['skin'])) selected( $instance['skin'], 'dark' ); ?>><?php _e('Dark'); ?></option>
				<option value="light" <?php if (isset($instance['skin'])) selected( $instance['skin'], 'light' ); ?>><?php _e('Light'); ?></option>
				<option value="custom" <?php if (isset($instance['skin'])) selected( $instance['skin'], 'custom' ); ?>><?php _e('Custom colors'); ?></option>
			</select>
		</p>
		
<div class="dependence" data-dependence="<?php echo $this->get_field_id( 'skin' ); ?>:custom" >		
		<p><label for="<?php echo $this->get_field_id( 'custom_bg_color' ); ?>" style="display:block;"><?php _e( 'Widget Background color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_bg_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_bg_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_bg_color ); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'custom_items_bg_color' ); ?>" style="display:block;"><?php _e( 'Items Background color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_bg_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bg_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_bg_color ); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'custom_items_title_color' ); ?>" style="display:block;"><?php _e( 'Items Title color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_title_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_title_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_title_color ); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'custom_items_title_hover_color' ); ?>" style="display:block;"><?php _e( 'Items Title hover color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_title_hover_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_title_hover_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_title_hover_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_title_font_size' ); ?>"><?php _e('Items Title Font size (in px)', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_title_font_size' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_title_font_size' ); ?>" type="text" value="<?php echo $custom_items_title_font_size; ?>" /></p>
		
			<p><label for="<?php echo $this->get_field_id( 'custom_items_title_font' ); ?>"><?php _e('Items Title Font-Family', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_title_font' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_title_font' ); ?>" type="text" value="<?php echo $custom_items_title_font; ?>" /></p>
				
			<p><label for="<?php echo $this->get_field_id( 'custom_items_title_hover_color' ); ?>" style="display:block;"><?php _e( 'Items Text color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_text_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_text_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_text_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_text_font_size' ); ?>"><?php _e('Items Text Font size (in px)', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_text_font_size' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_text_font_size' ); ?>" type="text" value="<?php echo $custom_items_text_font_size; ?>" /></p>
		
			<p><label for="<?php echo $this->get_field_id( 'custom_items_text_font_family' ); ?>"><?php _e('Items Text Font-Family', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_text_font_family' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_text_font_family' ); ?>" type="text" value="<?php echo $custom_items_text_font_family; ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_bttn_bg_color' ); ?>" style="display:block;"><?php _e( 'Items Expand Button background color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_bttn_bg_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bttn_bg_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_bttn_bg_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_bttn_hover_bg_color' ); ?>" style="display:block;"><?php _e( 'Items Expand Button hover background color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_bttn_hover_bg_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bttn_hover_bg_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_bttn_hover_bg_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_bttn_link_color' ); ?>" style="display:block;"><?php _e( 'Items Expand text color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_bttn_link_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bttn_link_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_bttn_link_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_bttn_link_hover_color' ); ?>" style="display:block;"><?php _e( 'Items Expand text link hover color: ' ); ?></label> 
    <input class="widefat color-picker" id="<?php echo $this->get_field_id( 'custom_items_bttn_link_hover_color' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bttn_link_hover_color' ); ?>" type="text" value="<?php echo esc_attr( $custom_items_bttn_link_hover_color ); ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_border_style' ); ?>"><?php _e('Items Border style', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_border_style' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_border_style' ); ?>" type="text" value="<?php echo $custom_items_border_style; ?>" /></p>
		
			<p><label for="<?php echo $this->get_field_id( 'custom_items_border_radius' ); ?>"><?php _e('Items Border radius', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_border_radius' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_border_radius' ); ?>" type="text" value="<?php echo $custom_items_border_radius; ?>" /></p>

			<p><label for="<?php echo $this->get_field_id( 'custom_items_bttn_border_radius' ); ?>"><?php _e('Items Button border radius', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_items_bttn_border_radius' ); ?>" name="<?php echo $this->get_field_name( 'custom_items_bttn_border_radius' ); ?>" type="text" value="<?php echo $custom_items_bttn_border_radius; ?>" /></p>
		
			<p><label for="<?php echo $this->get_field_id( 'custom_image_border_radius' ); ?>"><?php _e('Image Border radius', ''); ?>: </label><input class="widefat" id="<?php echo $this->get_field_id( 'custom_image_border_radius' ); ?>" name="<?php echo $this->get_field_name( 'custom_image_border_radius' ); ?>" type="text" value="<?php echo $custom_image_border_radius; ?>" /></p>
</div>

<?php
	}	
	
}

function expand($text, $counttext = 10, $sep = ' ') {
	$artext = array();
    $words = explode($sep, $text);
    if ( count($words) > $counttext ) {
        $artext['short'] = join($sep, array_slice($words, 0, $counttext));
        $artext['full'] = join($sep, array_slice($words, $counttext));
        } else {$artext['short'] = $text;}
    return $artext;
}

add_action( 'widgets_init', create_function( '', 'register_widget("ggt_recentcomments_widget");' ) );