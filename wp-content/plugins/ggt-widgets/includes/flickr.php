<?php

require_once('flickrclass/phpFlickr.php');

class ggt_flickr_widget extends WP_Widget {

	function ggt_flickr_widget() {
		global $ukey;
		$this->ukey = $ukey;
		$widget_ops = array('classname' => 'ggt-flickr-feed', 'description' => __('Displays your latest Flickr photos', $this->ukey) );
		$this->WP_Widget('ggt-flickr-feed', __('GGT Social Widgets: Flickr', $this->ukey), $widget_ops);
	}

	function widget($args, $instance) {
	
		  wp_enqueue_style( 'ggt_swidget_extend_style', plugins_url('/../assets/css/custom.css', __FILE__) );
	      wp_enqueue_style( 'ggt_swidget_owlcss', plugins_url('/../assets/css/owl.carousel.css', __FILE__) );
	      wp_enqueue_style( 'ggt_swidget_font-ggtcontrols', plugins_url('/../assets/css/font-ggtcontrols.css', __FILE__) );

	      wp_enqueue_style('thickbox');
	
	      wp_enqueue_script( 'ggt_swidget_waypoint', plugins_url('/../assets/js/waypoints.min.js', __FILE__), array('jquery') );
	      wp_enqueue_script( 'ggt_swidget_extend_js', plugins_url('/../assets/js/custom.js', __FILE__), array('jquery') );
	      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
	      wp_enqueue_script( 'ggt_swidget_jquery-expand', plugins_url('/../assets/js/expand.min.js', __FILE__), array('jquery') );
	
	      wp_enqueue_script( 'ggt_swidget_owljs', plugins_url('/../assets/js/owl.carousel.js', __FILE__), array('jquery') );
	      wp_enqueue_script( 'ggt_swidget_carousel_js', plugins_url('/../assets/js/carousel.js', __FILE__), array('jquery', 'ggt_swidget_owljs') );


		extract($args, EXTR_SKIP);

		$title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
		$userid = empty($instance['userid']) ? '' : esc_attr(trim($instance['userid']));
		$apikey = empty($instance['apikey']) ? '' : esc_attr(trim($instance['apikey']));
		$limit = empty($instance['numdisp']) ? 1 : $instance['numdisp'];
		
		$size = empty($instance['size']) ? 'thumbnail' : $instance['size'];
		$target = empty($instance['target']) ? '_self' : $instance['target'];
		$link = empty($instance['link']) ? '' : $instance['link'];
		$dc_posts = ! empty($instance['dc_posts']) ? '1' : '0';
		$d_useravatar = ! empty($instance['d_useravatar']) ? '1' : '0';
		$d_followbttn = ! empty($instance['d_followbttn']) ? '1' : '0';
		$d_website = ! empty($instance['d_website']) ? '1' : '0';
		$d_captions = ! empty($instance['d_captions']) ? '1' : '0';
		$autoplay = ! empty($instance['autoplay']) ? 'true' : 'false';
		$layout = empty( $instance['layout'] ) ? 'grid' : $instance['layout'];
		$widtype = empty( $instance['widtype'] ) ? 'profile' : $instance['widtype'];
		$mproc = 100 / 4;
		
		$add_att = 'class="finstagram-pics"';
		
		if ($widtype == 'profile') $limit = 12;
		
		

		echo $before_widget;
		if(!empty($title)) { echo $before_title . $title . $after_title; };

		do_action( 'ggt_before_widget', $instance );

		if ($userid != '') {

			$media_array = $this->scrape_flickr($apikey, $userid, $limit);
			
						//print_r('<pre>');print_r($media_array);print_r('</pre>');


			if ( is_wp_error($media_array) ) {

			   echo $media_array->get_error_message();

			} else {

				// filter for images only?
				
					
					if (($widtype == 'gallery') && isset($media_array['media'])) {
						$cel = (count($media_array['media']) > 3) ? floor(count($media_array['media']) / 4) : count($media_array['media']) / 4;
						$sliced_array = array();
						
						if ($cel < 1) {
							$proc = 100 / count($media_array['media']);
						} else {
						
						foreach ($media_array as $key => $minstagram) {
						   if ($key == 'media') $sliced_array = array_slice($minstagram, $cel * 4, null, true);
						}
							$proc = 100 / 4;
							$del = (count($sliced_array) > 0) ? count($sliced_array) : 1;
							$procost = 100 / $del;
						}
						
						$add_att = ($layout == 'grid') ? 'class="finstagram-pics"' : 'class="inowlcarousel finstagram-pics" data-autoplay="'.$autoplay.'"';

					}

//print_r('<pre>');print_r($cel);print_r('</pre>');


				?>
				
				
				<ul <?php echo $add_att; ?>><?php
				if (isset($media_array['media'])) foreach ($media_array['media'] as $key=>$item) {
				
				if ($widtype == 'gallery') {
				
						if ($layout == 'grid') {
						
						$mproc = (isset($sliced_array[$key])) ? $procost : $proc;
						
							echo '<li style="width:'.$mproc.'%"><a href="'. esc_url( $item['link'] ) .'" target="'. esc_attr( $target ) .'"><div class="fimg" style="background-image: url('. esc_url($item['low_resolution']) .');"></div></a></li>';
							
						} else {
						
							echo '<li><a href="'. esc_url( $item['link'] ) .'" target="'. esc_attr( $target ) .'"><img src="'. esc_url($item['low_resolution']) .'"  alt="'. esc_attr( $item['description'] ) .'" title="'. esc_attr( $item['description'] ).'"  /></a></li>'; 						
						}
					
					} else {
						
						echo '<li style="width:'.$mproc.'%"><a href="'. esc_url( $item['link'] ) .'" target="'. esc_attr( $target ) .'"><div class="fimg" style="background-image: url('. esc_url($item['low_resolution']) .');"></div></a></li>';
					}
				
				}
				?>
				
				</ul>
				
				<script>
				(function($){
                    $(document).ready(function(){
                        var $items = $(".ggt-flickr-feed li");
                        $items.each(function(i){
                        	$(this).height($(this).width());
                        	$(this).find('.fimg').height($(this).width());
                        });
                    })
                })(jQuery);
				</script>
								
				<!-- clearfix --><div style="clear: left;"></div><!-- clearfix -->
				
				
				<?php if ($d_useravatar > 0) { ?>
				<div class="instaprof_avatar" style="background: url(<?php echo $media_array['info']['avatar']; ?>) no-repeat center center;"></div>	
				<?php } ?>
							
				<?php if (($d_captions == 0) && isset($media_array['info']['username']) && (trim($media_array['info']['username']) !== '')) { ?><h3 class="instaprof_username"><?php echo $media_array['info']['username']; ?></h3><?php } ?>	
				
				<?php if (($d_website > 0) && isset($media_array['info']['website']) && (trim($media_array['info']['website']) !== '')) { ?><div class="instaprof_website"><a href="<?php echo trim($media_array['info']['website']); ?>"><?php echo trim($media_array['info']['website']); ?></a></div><?php } ?>			

				<?php if (($d_captions == 0) && isset($media_array['info']['bio']) && (trim($media_array['info']['bio']) !== '')) { ?><div class="instaprof_bio"><?php echo $media_array['info']['bio']; ?></div><?php } ?>			
				
				<?php if ($dc_posts > 0) echo '<div class="instaprof_counters">'.__("Posts", $this->ukey).': <span>'.(count($media_array['info']['count_posts']) > 0 ? $media_array['info']['count_posts'] : '0').'</span></div>'; ?>
				
								
				
				<?php
				
			}
		}

		if (($link != '') && ($d_followbttn > 0)) {
			?><div class="follow_button"><a href="<?php echo $media_array['info']['website']; ?>" rel="me" target="<?php echo esc_attr( $target ); ?>"><?php echo $link; ?></a></div><?php
		}

		do_action( 'ggt_before_widget', $instance );

		echo $after_widget;
	}

	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array(  'numdisp' => 1, 'layout' => 'grid', 'widtype' => 'profile', 'title' => __('Flickr', $this->ukey), 'userid' => '', 'apikey' => '', 'link' => __('Follow Us', $this->ukey), 'number' => 12, 'size' => 'thumbnail', 'target' => '_self', 'dc_posts' => 0, 'dc_followers' => 0, 'dc_following' => 0, 'd_useravatar' => 0, 'd_website' => 0, 'd_captions' => 0, 'd_followbttn' => 0, 'autoplay' => 0,) );
		$title = esc_attr($instance['title']);
		$userid = esc_attr($instance['userid']);
		$apikey = esc_attr($instance['apikey']);
		$numdisp = absint($instance['numdisp']);
		$size = esc_attr($instance['size']);
		$target = esc_attr($instance['target']);
		$link = esc_attr($instance['link']);
		$dc_posts = $instance['dc_posts'] ? 'checked="checked"' : '';
		$dc_followers = $instance['dc_followers'] ? 'checked="checked"' : '';
		$dc_following = $instance['dc_following'] ? 'checked="checked"' : '';
		$d_useravatar = $instance['d_useravatar'] ? 'checked="checked"' : '';
		$d_website = $instance['d_website'] ? 'checked="checked"' : '';
		$d_captions = $instance['d_captions'] ? 'checked="checked"' : '';
		$d_followbttn = $instance['d_followbttn'] ? 'checked="checked"' : '';
		$autoplay = $instance['autoplay'] ? 'checked="checked"' : '';
		
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', $this->ukey); ?>: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
		<p><label for="<?php echo $this->get_field_id('userid'); ?>"><?php _e('Flickr UserID (e.g. 00000000@N00)', $this->ukey); ?>: <input class="widefat" id="<?php echo $this->get_field_id('userid'); ?>" name="<?php echo $this->get_field_name('userid'); ?>" type="text" value="<?php echo $userid; ?>" /></label></p>
		<p><label for="<?php echo $this->get_field_id('apikey'); ?>"><?php _e('Flickr API Key', $this->ukey); ?>: <input class="widefat" id="<?php echo $this->get_field_id('apikey'); ?>" name="<?php echo $this->get_field_name('apikey'); ?>" type="text" value="<?php echo $apikey; ?>" /></label></p>
		
		<p>
			<label for="<?php echo $this->get_field_id('widtype'); ?>"><?php _e( 'Widget type:' ); ?></label>
			<select name="<?php echo $this->get_field_name('widtype'); ?>" id="<?php echo $this->get_field_id('widtype'); ?>" class="widefat">
				<option value="profile"<?php selected( $instance['widtype'], 'profile' ); ?>><?php _e('Profile'); ?></option>
				<option value="gallery"<?php selected( $instance['widtype'], 'gallery' ); ?>><?php _e('Gallery'); ?></option>
			</select>
		</p>
		
		<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'widtype' ); ?>:gallery" >
			<label for="<?php echo $this->get_field_id('layout'); ?>"><?php _e( 'Gallery layout:' ); ?></label>
			<select name="<?php echo $this->get_field_name('layout'); ?>" id="<?php echo $this->get_field_id('layout'); ?>" class="widefat">
				<option value="grid"<?php selected( $instance['layout'], 'grid' ); ?>><?php _e('Grid'); ?></option>
				<option value="slideshow"<?php selected( $instance['layout'], 'slideshow' ); ?>><?php _e('Slideshow'); ?></option>
			</select>
		</p>
		

		<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'widtype' ); ?>:gallery" >
			<label for="<?php echo $this->get_field_id('numdisp'); ?>"><?php _e( 'Number of Photos to Display:' ); ?></label>
			<select name="<?php echo $this->get_field_name('numdisp'); ?>" id="<?php echo $this->get_field_id('numdisp'); ?>" class="widefat">
			<?php for($i=1;$i<11;$i++) { ?>
				<option value="<?php echo $i; ?>"<?php selected( $instance['numdisp'], $i ); ?>><?php echo $i; ?></option>
			<?php } ?>
			</select>
		</p>
		
<p class="dependence" data-dependence="<?php echo $this->get_field_id( 'widtype' ); ?>:gallery;<?php echo $this->get_field_id( 'layout' ); ?>:slideshow" ><label><?php _e('AutoPlay', $this->ukey); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('autoplay'); ?>" <?php echo $autoplay; ?> /></p>
		
		
		
<p><label><?php _e('Display counters', $this->ukey); ?>: </label><br><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('dc_posts'); ?>" <?php echo $dc_posts; ?> /><?php _e('Posts', $this->ukey); ?></p>
<p><label><?php _e('Display user avatar', $this->ukey); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('d_useravatar'); ?>" <?php echo $d_useravatar; ?> /></p>
<p><label><?php _e('Display site url', $this->ukey); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('d_website'); ?>" <?php echo $d_website; ?> /></p>
<p><label><?php _e('Hide captions', $this->ukey); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('d_captions'); ?>" <?php echo $d_captions; ?> /></p>
<p><label><?php _e('Display  Follow button', $this->ukey); ?>: </label><input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('d_followbttn'); ?>" <?php echo $d_followbttn; ?> /></p>
		
		<p><label for="<?php echo $this->get_field_id('link'); ?>"><?php _e('Follow button text', $this->ukey); ?>: <input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo $link; ?>" /></label></p>

		<?php

	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['userid'] = trim(strip_tags($new_instance['userid']));
		$instance['apikey'] = trim(strip_tags($new_instance['apikey']));
		$instance['number'] = !absint($new_instance['number']) ? 12 : $new_instance['number'];
		$instance['size'] = (($new_instance['size'] == 'thumbnail' || $new_instance['size'] == 'large') ? $new_instance['size'] : 'thumbnail');
		$instance['target'] = (($new_instance['target'] == '_self' || $new_instance['target'] == '_blank') ? $new_instance['target'] : '_self');
		$instance['link'] = strip_tags($new_instance['link']);
		$instance['dc_posts'] = $new_instance['dc_posts'] ? 1 : 0;
		$instance['dc_followers'] = $new_instance['dc_followers'] ? 1 : 0;
		$instance['dc_following'] = $new_instance['dc_following'] ? 1 : 0;
		$instance['d_useravatar'] = $new_instance['d_useravatar'] ? 1 : 0;
		$instance['d_website'] = $new_instance['d_website'] ? 1 : 0;
		$instance['d_captions'] = $new_instance['d_captions'] ? 1 : 0;
		$instance['d_followbttn'] = $new_instance['d_followbttn'] ? 1 : 0;
		$instance['autoplay'] = $new_instance['autoplay'] ? 1 : 0;
		
		
		if ( in_array( $new_instance['widtype'], array( 'profile', 'gallery' ) ) ) {
			$instance['widtype'] = $new_instance['widtype'];
		} else {
			$instance['widtype'] = 'profile';
		}

		if ( in_array( $new_instance['layout'], array( 'grid', 'slideshow' ) ) ) {
			$instance['layout'] = $new_instance['layout'];
		} else {
			$instance['layout'] = 'grid';
		}

		if ( in_array( $new_instance['numdisp'], array( 1,2,3,4,5,6,7,8,9,10 ) ) ) {
			$instance['numdisp'] = $new_instance['numdisp'];
		} else {
			$instance['numdisp'] = 1;
		}
		
		return $instance;
	}

	function scrape_flickr($api_key, $userid, $slice = 1) {

		if (false === ($flickr = get_transient('flickr-media-'.sanitize_title_with_dashes($userid.$slice)))) {
		
		$f = new phpFlickr($api_key);

		$recent = $f->photos_search(array('user_id' => $userid, 'per_page' => $slice));
		
/*
			if (is_wp_error($recent))
	  			return new WP_Error('site_down', __('Unable to communicate with Flickr.', $this->ukey));

  			if ( 200 != wp_remote_retrieve_response_code( $recent ) )
  				return new WP_Error('invalid_response', __('Flickr did not return a 200.', $this->ukey));
*/

			$owner = $f->people_getInfo($userid);
			
			$images = $recent['photo'];
			
			$avatar = ($owner['iconfarm'] == 0) ? 'https://www.flickr.com/images/buddyicon.gif' : 'http://farm'.$owner['iconfarm'].'.staticflickr.com/'.$owner['iconserver'].'/buddyicons/'.$owner['nsid'].'.jpg';
						
/* 			print_r('<pre>');print_r($owner);print_r('</pre>'); */


			$flickr = array();
			
			$flickr['info'] = array (
			'username'   => $owner['realname']['_content'],
			'avatar'   => $avatar,
			'count_posts'   => $owner['photos']['count']['_content'],
			'bio'   => $owner['description']['_content'],
			'website'   => $owner['profileurl']['_content'],
			);
			
			if (count($images) > 0) {

				foreach ($images as $image) {
						
	
						$flickr['media'][] = array(
							'description'   => '',
							'link'          => "http://www.flickr.com/photos/" . $image['owner'] . "/" . $image['id'] . "",
							'time'          => '',
							'comments'      => '',
							'likes'         => '',
							'thumbnail'     => '',
							'large'         => '',
							'low_resolution'         => 'https://farm'. $image['farm'].'.staticflickr.com/'. $image['server'].'/'. $image['id'].'_'. $image['secret'].'_m.jpg',
							'type'          => '',
						);
				}
			
			}

			$flickr = base64_encode( serialize( $flickr ) );
			set_transient('flickr-media-'.sanitize_title_with_dashes($userid.$slice), $flickr, apply_filters('ggt_flickr_cache_time', 2)); //HOUR_IN_SECONDS*2
		}

		$flickr = unserialize( base64_decode( $flickr ) );


		return $flickr; 
}

}