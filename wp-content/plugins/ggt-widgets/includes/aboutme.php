<?php
/*
Plugin Name: Future Posts
*/
?>
<?

class FuturePosts extends WP_Widget {
function FuturePosts() {
parent::__construct(false, $name = 'About me');
}
//class FuturePosts extends WP_Widget {
//function FuturePosts() {
//parent::WP_Widget(false, $name = 'About me');
//}

function widget($args, $instance) {
extract( $args );
?>
<?php 
$aboutme_first_name = get_the_author_meta('first_name');
$aboutme_last_name = get_the_author_meta('last_name');
$aboutme_description = get_the_author_meta('description');
$aboutme_ava = get_avatar_url('gravatar_default');

echo '<aside class="widget"><div class="widget-aboutme-wrap">';
echo '<div class="widget-aboutme-background">';
echo '</div>';
echo '<div class="widget-aboutme-photo">';
$aboutme_img = '<img alt="" src="'. $aboutme_ava .'">';
echo $aboutme_img;
echo '</div>';
echo '<div class="aboutme-firstname"> '. $aboutme_first_name .'</div>';
echo '<div class="aboutme-lastname">'. $aboutme_last_name .'</div>';
echo '<div class="aboutme-description">'. $aboutme_description .'</div>';

echo '<div class="widget-aboutme-socials">';
get_template_part("part-social-footer"); 
echo '</div>';
echo '</div></aside>';
?>


<?php
}

function update($new_instance, $old_instance) {
return $new_instance;
}

function form($instance) {
$title = esc_attr($instance['title']);
?>

<p>
<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?>
<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
</label>
</p>

<?php
}

}
add_action('widgets_init', create_function('', 'return register_widget("FuturePosts");'));

?>
