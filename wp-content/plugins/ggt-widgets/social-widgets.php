<?php

/*
Plugin Name: GGT Widgets
Plugin URI: 
Description: 
Version: 1.0
Author: GoGetThemes.com
Author URI: http://gogetthemes.com
License:
License URI:
*/

include('includes/instagram.php');
//include('includes/twitter.php');

include('includes/flickr.php');
include('includes/facebook.php');
include('includes/vk.php');
include('includes/aboutme.php');
include('includes/socials-buttons.php');
include('includes/share.php');
include('includes/recent-posts.php');
include('includes/recent-comments.php');
include('includes/recent-works.php');
//include('includes/contacts.php');
//include('includes/timetable.php');




function loadJsCss() {
      wp_register_style( 'ggt_swidget_extend_style', plugins_url('assets/css/custom.css', __FILE__) );
      wp_register_style( 'ggt_swidget_owlcss', plugins_url('assets/css/posts-share.css', __FILE__) );

      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
}


function loadJsCssAdmin() {
      wp_register_style( 'ggt_swidget_extend_style', plugins_url('assets/css/custom.css', __FILE__) );
      
      
      
/*       //cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.css */
      
      wp_enqueue_script( 'ggt_swidget_extend_js', plugins_url('assets/js/custom.js', __FILE__), array('jquery', 'jquery-ui-datepicker') );
      wp_enqueue_script( 'ggt_swidget_spectrum', plugins_url('assets/js/spectrum.js', __FILE__), array('jquery') );
      wp_localize_script('ggt_swidget_extend_js', "adminAjax", admin_url('admin-ajax.php'));
      wp_enqueue_script('media-upload');
      wp_enqueue_script('thickbox');
      wp_enqueue_script('ggt_swidget_upload_media_helper', plugin_dir_url(__FILE__) . 'assets/js/upload-media.js', array('jquery'));
      wp_enqueue_script( 'ggt_swidget_timepicker', plugins_url('assets/js/timepicker.js', __FILE__), array('jquery') );

/*       wp_enqueue_script('jui', '//code.jquery.com/ui/1.11.1/jquery-ui.js', array('jquery')); */
      
      
}




// load is_plugin_active function if required
if (!function_exists('is_plugin_inactive')) require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

// registers widget
if (is_plugin_inactive('ggt-social-widgets/social-widgets.php') && function_exists('null_get_extensions')) {

	// register
	register_widget('ggt_instagram_widget');

	register_widget('ggt_twitter_widget');
	register_widget('ggt_flickr_widget');
	register_widget('ggt_facebook_widget');
	register_widget('ggt_vk_widget');
	register_widget('ggt_recentposts_widget');
	register_widget('ggt_recentcomments_widget');
	register_widget('ggt_recentworks_widget');
	register_widget('ggt_contacts_widget');
	register_widget('ggt_timetable_widget');
	
/* 	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) ); */
	
	add_post_type_support( 'page', '');
	add_post_type_support( 'post', '');

	// text domain
	$ukey = 'null';
}

// register widget for standalone
if (is_plugin_active('ggt-social-widgets/social-widgets.php')) {


	$ukey = 'ggt';

	//load_plugin_textdomain($ukey, false, dirname(WP_INSTAGRAM_WIDGET_BASE) . '/assets/languages/');

	// register
	add_action('widgets_init', 'ggt_widget');
        // Register CSS and JS    
    if (strstr($_SERVER['REQUEST_URI'], 'widgets.php')){
    add_action( 'admin_enqueue_scripts', 'loadJsCssAdmin' );
    }
    
/*     print_r('<pre>');print_r($_SERVER);print_r('</pre>'); */
    
/*     add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) ); */

	add_post_type_support( 'page', '');
	add_post_type_support( 'post', '');


}

function ggt_widget() {
	register_widget('ggt_instagram_widget');
	register_widget('ggt_twitter_widget');
	register_widget('ggt_flickr_widget');
	register_widget('ggt_facebook_widget');
	register_widget('ggt_vk_widget');
	register_widget('ggt_aboutme');
	register_widget('ggt_share');
	register_widget('ggt_recentposts_widget');
	register_widget('ggt_recentcomments_widget');
	register_widget('ggt_recentworks_widget');
	register_widget('ggt_contacts_widget');
	register_widget('ggt_timetable_widget');
}

