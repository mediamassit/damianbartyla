<?php
/*
  Plugin Name: MM plugin
  Description: Zapewnia strukturę oraz funkcjonalności serwisu.
  Author: mediamass.pl
  Author URI: http://mediamass.pl
  Text Domain: mm-plugin
  Domain Path: /languages/
  Version: 1.0.0
*/
require 'structure/index.php';

function mm_load_custom_wp_admin_style() {
	wp_register_style('mm-plugin', plugin_dir_url( __FILE__ ) . 'assets/style.css', false, '1.0.0');
	wp_enqueue_style('mm-plugin');
}
add_action('admin_enqueue_scripts', 'mm_load_custom_wp_admin_style', 500, 2);