<?php

function mm_taxonomy_employment() {
	$labels = array(
		'name'              => _x( 'Zatrudnienia', '' ),
		'singular_name'     => _x( 'Zatrudnienie', '' ),
		'search_items'      => __( 'Szukaj' ),
		'all_items'         => __( 'Wszystkie zatrudnienia' ),
		'parent_item'       => __( 'Rodzic' ),
		'parent_item_colon' => __( 'Rodzic:' ),
		'edit_item'         => __( 'Edytuj' ),
		'update_item'       => __( 'Zaaktualizuj' ),
		'add_new_item'      => __( 'Dodaj nowe zatrudnienie' ),
		'new_item_name'     => __( 'Nowe zatrudnienie' ),
		'menu_name'         => __( 'Zatrudnienia' ),
	);
	$rewrite = array(
		'slug'                       => 'zatrudnienie',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite
	);
	register_taxonomy( 'offert_employment',  array('offert'), $args );
}
add_action( 'init', 'mm_taxonomy_employment', 0 );