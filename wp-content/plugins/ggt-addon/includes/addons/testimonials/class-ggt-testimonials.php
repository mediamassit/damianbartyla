<?php

/*
Widget Name: GoGetThemes Testimonials
Description: Display testimonials from your clients/customers in a multi-column grid.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/


class GGT_Testimonials {

    protected $_per_line;
    protected $_testimonial_item_text_color;
    protected $_testimonial_item_title_color;
    protected $_testimonial_item_url_color;

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_testimonials', array($this, 'shortcode_func'));

        add_shortcode('GGT_testimonial', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_style('ggt-testimonials', plugin_dir_url(__FILE__) . 'css/style.css', array(), GGT_VERSION);

    }

    public function shortcode_func($atts, $content = null, $tag) {

        $per_line =  '';

        extract(shortcode_atts(array(
            'per_line' => '3',
            'style' => 'style1',


        ), $atts));

        $this->_per_line = $per_line;

        ob_start();

        ?>

        <div class="ggt-testimonials ggt-container ggt-<?php echo $style; ?>">

            <?php

            do_shortcode($content);

            ?>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $author = $testimonial_item_text_color = $testimonial_item_title_color = $testimonial_item_url_color = $credentials = $author_image = '';
        extract(shortcode_atts(array(
            'author' => '',
            'credentials' => '',
            'author_image' => '',
            'testimonial_item_text_color' => '',
            'testimonial_item_title_color' => '',
            'testimonial_item_url_color' => ''

        ), $atts));

        $this->_testimonial_item_text_color = $testimonial_item_text_color;
        $this->_testimonial_item_title_color = $testimonial_item_title_color;
        $this->_testimonial_item_url_color = $testimonial_item_url_color;

        $column_style = GGT_get_column_class(intval($this->_per_line));

        $testimonial_item_text_color = ' style="color:' . esc_attr($this->_testimonial_item_text_color) . '"';
        $testimonial_item_title_color = ' style="color:' . esc_attr($this->_testimonial_item_title_color) . '"';
        $testimonial_item_url_color = ' style="color:' . esc_attr($this->_testimonial_item_url_color) . '"';



        if (function_exists('wpb_js_remove_wpautop'))
            $content = wpb_js_remove_wpautop($content); // fix unclosed/unwanted paragraph tags in $content

        ?>

        <div class="ggt-testimonial <?php echo $column_style; ?>">

            <div class="ggt-testimonial-text" <?php echo $testimonial_item_text_color; ?>>
                <?php echo wp_kses_post($content) ?>
            </div>

            <div class="ggt-testimonial-user">

                <div class="ggt-image-wrapper">
                    <?php echo wp_get_attachment_image($author_image, 'thumbnail', false, array('class' => 'ggt-image full')); ?>
                </div>

                <div class="ggt-text">
                    <h4 class="ggt-author-name" <?php echo $testimonial_item_title_color; ?>><?php echo esc_html($author) ?></h4>
                    <div class="ggt-author-credentials" <?php echo $testimonial_item_url_color; ?>><?php echo wp_kses_post($credentials); ?></div>
                </div>

            </div>

        </div>

    <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Testimonials", "ggt-vc-addons"),
                "base" => "GGT_testimonials",
                "as_parent" => array('only' => 'GGT_testimonial'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Display testimonials in a multi-column grid.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-testimonials',
                "params" => array(
                    array(
                        "type" => "GGT_number",
                        "param_name" => "per_line",
                        "value" => 3,
                        "min" => 1,
                        "max" => 5,
                        "suffix" => '',
                        "heading" => __("Columns per row", "ggt-vc-addons"),
                        "description" => __("The number of testimonials members to display per row of the testimonials", "ggt-vc-addons")
                    ),
                    array(
                        "type" => "dropdown",
                        "param_name" => "style",
                        "heading" => __("Choose Style", "ggt-vc-addons"),
                        "description" => __("Choose the particular style of services you need", "ggt-vc-addons"),
                        'value' => array(
                            __('Style 1', 'ggt-vc-addons') => 'style1',
                            __('Style 2', 'ggt-vc-addons') => 'style2',
                            __('Style 3', 'ggt-vc-addons') => 'style3',
                        ),
                        'std' => 'style1',
                    ),
                ),
            ));


        }
    }


    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Testimonials Item", "ggt-vc-addons"),
                    "base" => "GGT_testimonial",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_testimonials'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-testimonial',
                    "category" => __('Testimonials', 'ggt-vc-addons'),
                    "params" => array(
                        // add params same as with any other content element
                        array(
                            'type' => 'textfield',
                            'param_name' => 'author',
                            "admin_label" => true,
                            'heading' => __('Name', 'ggt-vc-addons'),
                            'description' => __('The author of the testimonial', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'textfield',
                            'param_name' => 'credentials',
                            'heading' => __('Author Details', 'ggt-vc-addons'),
                            'description' => __('The details of the author like company name, position held, company URL etc.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'attach_image',
                            'param_name' => 'author_image',
                            'heading' => __('Author Image', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'textarea_html',
                            'param_name' => 'content',
                            'heading' => __('Text', 'ggt-vc-addons'),
                            'description' => __('What your client/customer has to say', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'testimonial_item_text_color',
                            'heading' => __('Testimonials text color', 'ggt-vc-addons'),
                            'value' => 'transparent',
                            'group' => __('Custom color' , 'ggt-vc-addons')
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'testimonial_item_title_color',
                            'heading' => __('Testimonials text color', 'ggt-vc-addons'),
                            'value' => 'transparent',
                            'group' => __('Custom color' , 'ggt-vc-addons')
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'testimonial_item_url_color',
                            'heading' => __('Testimonials text color', 'ggt-vc-addons'),
                            'value' => 'transparent',
                            'group' => __('Custom color' , 'ggt-vc-addons')
                        ),
                    )
                )

            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_testimonials extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_testimonial extends WPBakeryShortCode {
    }
}