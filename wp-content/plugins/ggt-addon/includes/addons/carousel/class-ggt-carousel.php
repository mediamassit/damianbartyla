<?php

/*
Widget Name: GoGetThemes Carousel
Description: Display a list of custom HTML content as a carousel.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/


class GGT_Carousel {

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_carousel', array($this, 'shortcode_func'));

        add_shortcode('GGT_carousel_item', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_script('ggt-slick-carousel', GGT_PLUGIN_URL . 'assets/js/slick' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        wp_enqueue_style('ggt-slick', GGT_PLUGIN_URL . 'assets/css/slick.css', array(), GGT_VERSION);

        wp_enqueue_style('ggt-carousel', plugin_dir_url(__FILE__) . 'css/style.min.css', array(), GGT_VERSION);

    }

    public function shortcode_func($atts, $content = null, $tag) {

        $defaults = GGT_get_default_atts_carousel();

        $settings = shortcode_atts($defaults, $atts);

        $uniqueid = uniqid();

        ob_start();

        ?>

        <div id="ggt-carousel-<?php echo $uniqueid; ?>" class="ggt-carousel ggt-container"<?php foreach ($settings as $key => $val) {
            if (!empty($val)) {
                echo ' data-' . $key . '="' . esc_attr($val) . '"';
            }
        } ?>>

            <?php

            do_shortcode($content);

            ?>
        </div>
        <style type="text/css">
            #ggt-carousel-<?php echo $uniqueid; ?>.ggt-carousel .ggt-carousel-item {
                padding: <?php echo $settings['gutter']; ?>px;
                }

            @media screen and (max-width: <?php echo $settings['tablet_width']; ?>) {
                #ggt-carousel-<?php echo $uniqueid; ?>.ggt-carousel .ggt-carousel-item {
                    padding: <?php echo $settings['tablet_gutter']; ?>px;
                    }
                }

            @media screen and (max-width: <?php echo $settings['mobile_width']; ?>) {
                #ggt-carousel-<?php echo $uniqueid; ?>.ggt-carousel .ggt-carousel-item {
                    padding: <?php echo $settings['mobile_gutter']; ?>px;
                    }
                }
        </style>
        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        if (function_exists('wpb_js_remove_wpautop'))
            $content = wpb_js_remove_wpautop($content); // fix unclosed/unwanted paragraph tags in $content

        ?>
        <div class="ggt-carousel-item">

            <?php echo do_shortcode(wp_kses_post($content)); ?>

        </div>

    <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            $carousel_params = GGT_get_vc_map_carousel_options();

            $carousel_params = array_merge($carousel_params, GGT_get_vc_map_carousel_display_options());

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Carousel", "ggt-vc-addons"),
                "base" => "GGT_carousel",
                "as_parent" => array('only' => 'GGT_carousel_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Display a carousel of html elements.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-carousel',
                "params" => $carousel_params
            ));


        }
    }


    function map_child_vc_element() {
        if (function_exists("vc_map")) {

            vc_map(array(
                    "name" => __("Carousel", "ggt-vc-addons"),
                    "base" => "GGT_carousel_item",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_carousel'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-carousel-item',
                    "category" => __('Carousel', 'ggt-vc-addons'),
                    "params" => array(
                        array(
                            'type' => 'textfield',
                            'param_name' => 'name',
                            'heading' => __('Name', 'ggt-vc-addons'),
                            'description' => __('The title to identify the HTML element. Will not be output to the frontend.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textarea_html',
                            'param_name' => 'content',
                            'heading' => __('HTML element', 'ggt-vc-addons'),
                            'description' => __('The HTML content for the carousel item. Custom CSS for presentation of the HTML elements should be entered by the user in Settings->Custom CSS panel in VC or in the theme files.', 'ggt-vc-addons'),
                        ),
                    )

                )
            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_carousel extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_carousel_item extends WPBakeryShortCode {
    }
}