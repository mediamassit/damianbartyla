<?php

/*
Widget Name: GoGetThemes Stats Bars
Description: Display multiple stats bars that talk about skills or other percentage stats.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/

class GGT_Stats_Bars {


    protected $_default_bar_color;
    protected $_default_text_color;

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_statsbars', array($this, 'shortcode_func'));

        add_shortcode('GGT_statsbar_item', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_script('ggt-waypoints', GGT_PLUGIN_URL . 'assets/js/jquery.waypoints' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

       // wp_enqueue_script('ggt-stats-bar', plugin_dir_url(__FILE__) . 'js/stats-bar' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);
        wp_enqueue_script('ggt-stats-bar', plugin_dir_url(__FILE__) . 'js/stats-bar.min.js', array('jquery'), GGT_VERSION);

        wp_enqueue_style('ggt-stats-bar', plugin_dir_url(__FILE__) . 'css/style.css', array(), GGT_VERSION);
    }

    public function shortcode_func($atts, $content = null, $tag) {

        $default_bar_color = $default_text_color = '';

        extract(shortcode_atts(array(
            'default_bar_color' => '#fe5000',
            'default_text_color' => '#221c24',
        ), $atts));

        $this->_default_bar_color = $default_bar_color;
        $this->_default_text_color = $default_text_color;

        ob_start();

        ?>

        <div class="ggt-stats-bars">

            <?php

            do_shortcode($content);

            ?>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $bar_color = $text_color = $stats_title = $percentage = '';
        extract(shortcode_atts(array(
            'stats_title' => '',
            'percentage' => 50,
            'bar_color' => false,
            'text_color' => false

        ), $atts));

        if (!empty($bar_color))
            $color_style = ' style="background:' . esc_attr($bar_color) . ';"';
        else
            $color_style = ' style="background:' . esc_attr($this->_default_bar_color) . ';"';


        if (!empty($text_color))
            $text_color_style = ' style="color:' . esc_attr($text_color) . ';"';
        else
            $text_color_style = ' style="color:' . esc_attr($this->_default_text_color) . ';"';
        ?>

        <div class="ggt-stats-bar">

            <div class="ggt-stats-title" <?php echo $text_color_style; ?>>
                <?php echo esc_html($stats_title) ?>
            </div>

            <div class="ggt-stats-bar-wrap">

                <div <?php echo $color_style; ?> class="ggt-stats-bar-content" data-perc="<?php echo esc_attr($percentage); ?>"><span><?php echo esc_attr($percentage); ?>%</span></div>

                <div class="ggt-stats-bar-bg"></div>

            </div>

        </div>

    <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Progress Bars", "ggt-vc-addons"),
                "base" => "GGT_statsbars",
                "as_parent" => array('only' => 'GGT_statsbar_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => false,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Display stats bars of skills or percentage stats.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-statsbars',
                "params" => array(

                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'default_bar_color',
                        'heading' => __('Default Bar color', 'ggt-vc-addons'),
                        'description' => __('The default bar color to be used if none specified for individual stats bars', 'ggt-vc-addons'),
                        'value' => '#fe5000'
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'default_text_color',
                        'heading' => __('Text color', 'ggt-vc-addons'),
                        'description' => __('The default text color', 'ggt-vc-addons'),
                        'value' => '#221c24'
                    ),
                ),
            ));


        }
    }


    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Prgoress Bar Item", "my-text-domain"),
                    "base" => "GGT_statsbar_item",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_statsbars'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-statsbar',
                    "params" => array(
                        // add params same as with any other content element
                        array(
                            'type' => 'textfield',
                            'param_name' => 'stats_title',
                            "admin_label" => true,
                            'heading' => __('Stats Title', 'ggt-vc-addons'),
                            'description' => __('Title for the stats bar.', 'ggt-vc-addons'),
                        ),
                        array(
                            "type" => "GGT_number",
                            "param_name" => "percentage",
                            "value" => 50,
                            "min" => 0,
                            "max" => 100,
                            "suffix" => '%',
                            "heading" => __("Percentage Value", "ggt-vc-addons"),
                            "description" => __("The percentage value for the stats.", "ggt-vc-addons")
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'bar_color',
                            'heading' => __('Bar color', 'ggt-vc-addons'),
                        ),

                    )
                )

            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_statsbars extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_statsbar_item extends WPBakeryShortCode {
    }
}