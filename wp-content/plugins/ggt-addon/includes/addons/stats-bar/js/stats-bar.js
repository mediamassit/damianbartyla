jQuery(function ($) {


    $('.ggt-stats-bars').waypoint(function (direction) {

        $(this.element).find('.ggt-stats-bar-content').each(function () {

            var dataperc = $(this).attr('data-perc');
           // console.log(dataperc);
            $(this).animate({ "width": dataperc + "%"}, dataperc * 20);

        });

    }, { offset: $.waypoints('viewportHeight') - 150,
        triggerOnce: true});


});