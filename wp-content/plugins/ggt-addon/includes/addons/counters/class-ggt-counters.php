<?php

/*
Widget Name: GoGetThemes Counters
Description: Display Counter statistics in a multi-column grid.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/

class GGT_Counters {

    private $_per_line;
    private $_counter_prefix_color;
    private $_counter_value_color;
    private $_counter_suffix_color;
    private $_counter_icon_color;
    private $_counter_title_color;
    private $_counter_array;

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_counters', array($this, 'shortcode_func'));

        add_shortcode('GGT_counter_item', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_script('ggt-waypoints', GGT_PLUGIN_URL . 'assets/js/jquery.waypoints' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        wp_enqueue_script('ggt-stats', GGT_PLUGIN_URL . 'assets/js/jquery.stats' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

//        wp_enqueue_script('ggt-counters', plugin_dir_url(__FILE__) . 'js/counter.js' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);
        wp_enqueue_script('ggt-counters', plugin_dir_url(__FILE__) . 'js/odometer.min.js', array('jquery'), GGT_VERSION);

        wp_enqueue_style('ggt-counters', plugin_dir_url(__FILE__) . 'css/style.min.css', array(), GGT_VERSION);

    }

    public function shortcode_func($atts, $content = null, $tag) {

        $per_line = $counter_padding = $counter_prefix_color = $counter_value_color = $counter_suffix_color = $counter_icon_color = $counter_title_color = $style = '';

        extract(shortcode_atts(array(
            'per_line' => '4',
            'counter_padding' => '',
            'counter_prefix_color' => '',
            'counter_value_color' => '',
            'counter_suffix_color' => '',
            'counter_icon_color' => '',
            'counter_title_color' => ''

        ), $atts));


//var_dump($atts);


        $this->_counter_padding = $counter_padding;
        $this->_counter_prefix_color = $counter_prefix_color;
        $this->_counter_value_color = $counter_value_color;
        $this->_counter_suffix_color = $counter_suffix_color;
        $this->_counter_icon_color = $counter_icon_color;
        $this->_counter_title_color = $counter_title_color;



        $this->_per_line = $per_line;

        $counter_prefix_color = ' style="color:' . esc_attr($this->_counter_prefix_color) . '"';
        $counter_value_color = ' style="color:' . esc_attr($this->_counter_value_color) . '"';
        $counter_suffix_color = ' style="color:' . esc_attr($this->_counter_suffix_color) . '"';
        $counter_icon_color = ' style="color:' . esc_attr($this->_counter_icon_color) . '"';
        $counter_title_color = ' style="color:' . esc_attr($this->_counter_title_color) . '"';


        ob_start();

        ?>

        <div class="ggt-odometers ggt-container <?php echo(!empty($counter_padding) ? ' responsive-padding' : ''); ?>">

            <?php

            do_shortcode($content);

            ?>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $stats_title =  $start_value = $stop_value = $prefix = $suffix = $icon_image = $icon_type = $icon_family = '';
        extract(shortcode_atts(array(
            'icon_type' => 'icon',
            'icon_image' => '',
            'icon_family' => 'fontawesome',
            "icon_fontawesome" => '',
            "icon_openiconic" => '',
            "icon_typicons" => '',
            "icon_entypo" => '',
            "icon_linecons" => '',
            'stats_title' => '',
            'start_value' => '',
            'stop_value' => '',
            'suffix' => '',
            'prefix' => '',
            'counter_prefix_color' => '',
            'counter_value_color' => '',
            'counter_suffix_color' => '',
            'counter_icon_color' => '',
            'counter_title_color' => ''

        ), $atts));


       // var_dump($counter_prefix_color);
       // var_dump($suffix);
        //var_dump($counter_suffix_color);
       // var_dump($counter_title_color);

        //var_dump($params);
        //var_dump($atts);

        /*$this->_counter_array['counter_prefix_color'] = $counter_prefix_color;
        $this->_counter_array['counter_value_color'] = $counter_value_color;
        $this->_counter_array['counter_suffix_color'] = $counter_suffix_color;
        $this->_counter_array['counter_icon_color'] = $counter_icon_color;
        $this->_counter_array['counter_title_color'] = $counter_title_color;*/

        /*$this->_counter_prefix_color = $this->_counter_array['counter_prefix_color'];
        $this->_counter_value_color = $this->_counter_array['counter_value_color'];
        $this->_counter_suffix_color = $this->_counter_array['counter_suffix_color'];
        $this->_counter_icon_color = $this->_counter_array['counter_icon_color'];
        $this->_counter_title_color = $this->_counter_array['counter_title_color'];*/

        $counter_prefix_color = ' style="color:' . esc_attr($this->_counter_prefix_color) . '"';
        $counter_value_color = ' style="color:' . esc_attr($this->_counter_value_color) . '"';
        $counter_suffix_color = ' style="color:' . esc_attr($this->_counter_suffix_color) . '"';
        $counter_icon_color = ' style="color:' . esc_attr($this->_counter_icon_color) . '"';
        $counter_title_color = ' style="color:' . esc_attr($this->_counter_title_color) . '"';







        $column_style = GGT_get_column_class(intval($this->_per_line));

        $icon_type = esc_html($icon_type);

        if ($icon_type == 'icon' && !empty(${'icon_' . $icon_family}) && function_exists('vc_icon_element_fonts_enqueue'))
            vc_icon_element_fonts_enqueue($icon_family);

        //$prefix = (!empty ($prefix)) ? '<span class="prefix">' . $prefix . '</span>' : '';
        $prefix = (!empty ($prefix)) ?  $prefix  : '';
        $suffix = (!empty ($suffix)) ?  $suffix  : '';
       // $suffix = (!empty ($suffix)) ? '<span class="suffix">' . $suffix . '</span>' : '';

        ?>

        <div class="ggt-odometer <?php echo $column_style; ?> ">
            <!--    11<?php echo $counter_prefix_color; ?>22-->
<!--            --><?php
//            $counter_suffix_color = ' style="color:' . esc_attr($this->_counter_suffix_color) . '"';
//            var_dump($counter_prefix_color);
//            var_dump($prefix);
//            var_dump($counter_suffix_color);
//            var_dump($column_style);
//            //var_dump( vc_map());
//            ?>
            <?php echo (!empty ($prefix)) ? '<span class="ggt-prefix" '.$counter_prefix_color. '> '.$prefix.'</span>' : ''; ?>

            <div class="ggt-number odometer " <?php echo $counter_value_color; ?> data-stop="<?php echo intval($stop_value); ?>">

                <?php echo intval($start_value); ?>

            </div>

            <?php echo (!empty ($suffix)) ? '<span class="ggt-suffix" '.$counter_suffix_color.'> ' . $suffix . '</span>' : ''; ?>

            <?php $icon_type = esc_html($icon_type); ?>

            <?php $icon_html = ''; ?>

            <?php if ($icon_type == 'icon_image') : ?>

                <?php $icon_html = '<span class="ggt-image-wrapper">' . wp_get_attachment_image($icon_image, 'full', false, array('class' => 'ggt-image full')) . '</span>'; ?>

            <?php elseif (!empty(${'icon_' . $icon_family})): ?>

                <?php $icon_html = '<div class="ggt-icon-wrapper" '.$counter_icon_color.'>' . GGT_get_icon(${'icon_' . $icon_family}) . '</div>'; ?>

            <?php endif; ?>

            <div class="ggt-stats-title-wrap">

                <div class="ggt-stats-title" <?php echo $counter_title_color; ?>><?php echo $icon_html . '<h4>' . esc_html($stats_title); ?></h4></div>

            </div>

        </div>

    <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Counter", "ggt-vc-addons"),
                "base" => "GGT_counters",
                "as_parent" => array('only' => 'GGT_counter_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Display counters in a multi-column grid.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-counters',
                "params" => array(

                    array(
                        "type" => "GGT_number",
                        "param_name" => "per_line",
                        "value" => 4,
                        "min" => 1,
                        "max" => 5,
                        "suffix" => '',
                        "heading" => __("Columns per row", "ggt-vc-addons"),
                        "description" => __("The number of columns to display per row of the counters", "ggt-vc-addons")
                    ),
                    array(
                        'type' => 'checkbox',
                        'param_name' => 'counter_padding',
                        'heading' => __('Responsive Padding', 'ggt-vc-addons'),
                        'description' => __('Add Large Responsive Padding', 'ggt-vc-addons'),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'counter_prefix_color',
                        'heading' => __('Prefix color', 'ggt-vc-addons'),
                        'value' => '#999999',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'counter_value_color',
                        'heading' => __('Value color', 'ggt-vc-addons'),
                        'value' => '#221c24',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'counter_suffix_color',
                        'heading' => __('Suffix color', 'ggt-vc-addons'),
                        'value' => '#999999',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'counter_icon_color',
                        'heading' => __('Icon color', 'ggt-vc-addons'),
                        'value' => '#425cbb',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'counter_title_color',
                        'heading' => __('Title color', 'ggt-vc-addons'),
                        'value' => '#221c24',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                ),
            ));


        }
    }


    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Counter Item", "ggt-vc-addons"),
                    "base" => "GGT_counter_item",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_Counters'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-counters-item',
                    "params" => array(
                        // add params same as with any other content element
                        array(
                            'type' => 'textfield',
                            'param_name' => 'stats_title',
                            "admin_label" => true,
                            'heading' => __('Title', 'ggt-vc-addons'),
                            'description' => __('Title for the odometer stats.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'GGT_number',
                            'param_name' => 'start_value',
                            'heading' => __('Start Value', 'ggt-vc-addons'),
                            'description' => __('The start value for the odometer stats.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'GGT_number',
                            'param_name' => 'stop_value',
                            'heading' => __('Stop Value', 'ggt-vc-addons'),
                            'description' => __('The stop value for the odometer stats.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textfield',
                            'param_name' => 'prefix',
                            'heading' => __('Prefix', 'ggt-vc-addons'),
                            'description' => __('The prefix string for the odometer stats. Examples include currency symbols like $ to indicate a monetary value.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textfield',
                            'param_name' => 'suffix',
                            'heading' => __('Suffix', 'ggt-vc-addons'),
                            'description' => __('The suffix string for the odometer stats. Examples include strings like hr for hours or m for million.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'dropdown',
                            'param_name' => 'icon_type',
                            'heading' => __('Choose Icon Type', 'ggt-vc-addons'),
                            'std' => 'icon',
                            'value' => array(
                                __('Icon', 'ggt-vc-addons') => 'icon',
                                __('Icon Image', 'ggt-vc-addons') => 'icon_image',
                            )
                        ),

                        array(
                            'type' => 'attach_image',
                            'param_name' => 'icon_image',
                            'heading' => __('Counter Image.', 'ggt-vc-addons'),
                            "dependency" => array('element' => "icon_type", 'value' => 'icon_image'),
                        ),

                        array(
                            'type' => 'dropdown',
                            'heading' => __('Icon library', 'ggt-vc-addons'),
                            'value' => array(
                                __('Font Awesome', 'ggt-vc-addons') => 'fontawesome',
                                __('Open Iconic', 'ggt-vc-addons') => 'openiconic',
                                __('Typicons', 'ggt-vc-addons') => 'typicons',
                                __('Entypo', 'ggt-vc-addons') => 'entypo',
                                __('Linecons', 'ggt-vc-addons') => 'linecons',
                            ),
                            'std' => 'fontawesome',
                            'param_name' => 'icon_family',
                            'description' => __('Select icon library.', 'ggt-vc-addons'),
                            "dependency" => array('element' => "icon_type", 'value' => 'icon'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_fontawesome',
                            'value' => 'fa fa-info-circle',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'fontawesome',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_openiconic',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'openiconic',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'openiconic',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_typicons',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'typicons',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'typicons',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_entypo',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'entypo',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'entypo',
                            ),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_linecons',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'linecons',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'linecons',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),
                    )
                )

            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_counters extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_counter_item extends WPBakeryShortCode {
    }
}