<?php

/*
Widget Name: GoGetThemes Services
Description: Capture services in a multi-column grid.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/

class GGT_Services {

    protected $_per_line;
    protected $_service_icon_color;
    protected $_service_title_color;
    protected $_service_text_color;
    protected $_service_hoverbg_color;

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_services', array($this, 'shortcode_func'));

        add_shortcode('GGT_service_item', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_style('ggt-services', plugin_dir_url(__FILE__) . 'css/style.css', array(), GGT_VERSION);

    }

    public function shortcode_func($atts, $content = null, $tag) {

        $per_line = $service_icon_color = $service_title_color = $service_text_color = $service_hoverbg_color  = $style = '';

        extract(shortcode_atts(array(
            'per_line' => 3,
            'service_icon_color' => '',
            'service_title_color' => '',
            'service_text_color' => '',
            'service_hoverbg_color' => '',
            'style' => 'style1',

        ), $atts));

        $this->_per_line = $per_line;
        $this->_service_icon_color = $service_icon_color;
        $this->_service_title_color = $service_title_color;
        $this->_service_text_color = $service_text_color;
        $this->_service_hoverbg_color = $service_hoverbg_color;


        ob_start();

        ?>

        <div class="ggt-services ggt-<?php echo $style; ?> ggt-container">

            <?php

            do_shortcode($content);

            ?>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $title = $excerpt = $icon_image = $icon_type = $icon_family = $service_highlight = '';
        extract(shortcode_atts(array(
            'icon_type' => 'icon',
            'icon_image' => '',
            'icon_family' => 'fontawesome',
            "icon_fontawesome" => '',
            "icon_openiconic" => '',
            "icon_typicons" => '',
            "icon_entypo" => '',
            "icon_linecons" => '',
            'title' => '',
            'service_highlight' => '',
            'excerpt' => ''

        ), $atts));

        $column_style = GGT_get_column_class(intval($this->_per_line));

        $service_icon_color = ' style="color:' . esc_attr($this->_service_icon_color) . '"';
        $service_title_color = ' style="color:' . esc_attr($this->_service_title_color) . '"';
        $service_text_color = ' style="color:' . esc_attr($this->_service_text_color) . '"';
        $service_hoverbg_color = ' style="background-color:' . esc_attr($this->_service_hoverbg_color) . '"';

        $icon_type = esc_html($icon_type);

        if ($icon_type == 'icon' && !empty(${'icon_' . $icon_family}) && function_exists('vc_icon_element_fonts_enqueue'))
            vc_icon_element_fonts_enqueue($icon_family); ?>

        <div class="ggt-service-wrapper  <?php echo(!empty($service_highlight) ? ' service-highlight' : ''); ?> <?php echo $column_style; ?>">

            <div class="ggt-service">

                <?php if ($icon_type == 'icon_image') : ?>

                    <div class="ggt-image-wrapper">

                        <?php echo wp_get_attachment_image($icon_image, 'full', false, array('class' => 'ggt-image full')); ?>

                    </div>

                <?php else : ?>

                    <?php if (!empty(${'icon_' . $icon_family})): ?>

                        <div class="ggt-icon-wrapper" <?php echo $service_icon_color; ?>>

                            <?php echo GGT_get_icon(${'icon_' . $icon_family}); ?>

                        </div>

                    <?php endif; ?>

                <?php endif; ?>

                <div class="ggt-service-text">

                    <h4 class="ggt-title" <?php echo $service_title_color; ?>><?php echo esc_html($title) ?></h4>

                    <?php if (!empty($content)): ?>

                        <div class="ggt-service-details" <?php echo $service_text_color; ?>><?php echo wp_kses_post($content); ?></div>

                    <?php else: ?>

                        <div class="ggt-service-details" <?php echo $service_text_color; ?>><?php echo wp_kses_post($excerpt); ?></div>

                    <?php endif; ?>

                </div>
            <div class="service-hover" <?php echo $service_hoverbg_color; ?>></div>
            </div>

        </div>

        <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Services", "ggt-vc-addons"),
                "base" => "GGT_services",
                "as_parent" => array('only' => 'GGT_service_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Display services in a column grid.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-services',
                "params" => array(
                    // add params same as with any other content element
                    array(
                        "type" => "dropdown",
                        "param_name" => "style",
                        "heading" => __("Choose Style", "ggt-vc-addons"),
                        "description" => __("Choose the particular style of services you need", "ggt-vc-addons"),
                        'value' => array(
                            __('Style 1', 'ggt-vc-addons') => 'style1',
                            __('Style 2', 'ggt-vc-addons') => 'style2',
                            __('Style 3', 'ggt-vc-addons') => 'style3',
                        ),
                        'std' => 'style1',
                    ),
                    array(
                        "type" => "GGT_number",
                        "param_name" => "per_line",
                        "value" => 3,
                        "min" => 1,
                        "max" => 5,
                        "suffix" => '',
                        "heading" => __("Columns per row", "ggt-vc-addons"),
                        "description" => __("The number of columns to display per row of the services", "ggt-vc-addons")
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'service_icon_color',
                        'heading' => __('Icon color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')

                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'service_title_color',
                        'heading' => __('Title color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')

                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'service_text_color',
                        'heading' => __('Text color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'service_hoverbg_color',
                        'heading' => __('Hover Background color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                ),
            ));


        }
    }


    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Service Item", "my-text-domain"),
                    "base" => "GGT_service_item",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_services'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-service',
                    "params" => array(
                        // add params same as with any other content element
                        array(
                            'type' => 'textfield',
                            'param_name' => 'title',
                            "admin_label" => true,
                            'heading' => __('Title', 'ggt-vc-addons'),
                            'description' => __('Title of the service.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'dropdown',
                            'param_name' => 'icon_type',
                            'heading' => __('Choose Icon Type', 'ggt-vc-addons'),
                            'std' => 'icon',
                            'value' => array(
                                __('Icon', 'ggt-vc-addons') => 'icon',
                                __('Icon Image', 'ggt-vc-addons') => 'icon_image',
                            )
                        ),

                        array(
                            'type' => 'attach_image',
                            'param_name' => 'icon_image',
                            'heading' => __('Service Image.', 'ggt-vc-addons'),
                            "dependency" => array('element' => "icon_type", 'value' => 'icon_image'),
                        ),

                        array(
                            'type' => 'dropdown',
                            'heading' => __('Icon library', 'ggt-vc-addons'),
                            'value' => array(
                                __('Font Awesome', 'ggt-vc-addons') => 'fontawesome',
                                __('Open Iconic', 'ggt-vc-addons') => 'openiconic',
                                __('Typicons', 'ggt-vc-addons') => 'typicons',
                                __('Entypo', 'ggt-vc-addons') => 'entypo',
                                __('Linecons', 'ggt-vc-addons') => 'linecons',
                            ),
                            'std' => 'fontawesome',
                            'param_name' => 'icon_family',
                            'description' => __('Select icon library.', 'ggt-vc-addons'),
                            "dependency" => array('element' => "icon_type", 'value' => 'icon'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_fontawesome',
                            'value' => 'fa fa-info-circle',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'fontawesome',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_openiconic',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'openiconic',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'openiconic',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_typicons',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'typicons',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'typicons',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_entypo',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'entypo',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'entypo',
                            ),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_linecons',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'linecons',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'linecons',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textarea_html',
                            'param_name' => 'content',
                            'heading' => __('Service description', 'ggt-vc-addons'),
                            'description' => __('Provide a short description for the service', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'checkbox',
                            'param_name' => 'service_highlight',
                            'heading' => __('Highlight Service Item', 'ggt-vc-addons'),
                            'description' => __('If you want to highlight the service item.', 'ggt-vc-addons'),
                        ),

                    )
                )

            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_services extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_service_item extends WPBakeryShortCode {
    }
}