<?php

/*
Widget Name: GoGetThemes Portfolio
Description: Display portfolio items from Jetpack custom post types in multi-column grid.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/

class GGT_Portfolio {

    private $_taxonomy_filter;

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_portfolio', array($this, 'shortcode_func'));

        // Do it as late as possible so that all taxonomies are registered
        add_action('init', array($this, 'map_vc_element'), 9999);

    }

    private function get_chosen_terms($query_args) {

        $chosen_terms = array();

        if (empty($query_args))
            return $chosen_terms;

        if (!empty($query_args['cat'])) {
            $this->_taxonomy_filter = 'category';
            $term_ids = explode(',', $query_args['cat']);
            foreach ($term_ids as $term_id) {
                $chosen_terms[] = get_term($term_id, 'category');
            }
        }
        elseif (!empty($query_args['tag__in'])) {
            $this->_taxonomy_filter = 'post_tag';
            $term_ids = $query_args['tag__in'];
            foreach ($term_ids as $term_id) {
                $chosen_terms[] = get_term($term_id, 'post_tag');
            }
        }
        elseif (!empty($query_args['tax_query'])) {
            $terms_query = $query_args['tax_query'];
            foreach ($terms_query as $term_query) {
                if (is_array($term_query) && ($term_query['field'] == 'term_id')) {
                    $chosen_terms[] = get_term($term_query['terms'], $term_query['taxonomy']);
                    $this->_taxonomy_filter = $term_query['taxonomy'];
                }
            }
        }
        return $chosen_terms;
    }

    function load_scripts() {

        wp_enqueue_script('ggt-isotope', GGT_PLUGIN_URL . 'assets/js/isotope.pkgd' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        wp_enqueue_script('ggt-imagesloaded', GGT_PLUGIN_URL . 'assets/js/imagesloaded.pkgd' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        wp_enqueue_script('ggt-portfolio', plugin_dir_url(__FILE__) . 'js/portfolio' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        wp_enqueue_style('ggt-portfolio', plugin_dir_url(__FILE__) . 'css/style.css', array(), GGT_VERSION);

    }

    public function shortcode_func($atts, $content = null, $tag) {

        $defaults = array_merge(
            array(
//                'heading' => '',
                'posts_query' => '',
                'display_title' => '',
                'display_author' => '',
                'display_post_date' => '',
                'display_taxonomy' => '',
                'display_summary' => '',
                'image_linkable' => '',
                'filterable' => '',
                'post_type' => 'jetpack-portfolio',
                'taxonomy_filter' => 'jetpack-portfolio-type',
                'per_line' => 4,
                'layout_mode' => 'fitRows',
                'packed' => '',
                'gutter' => 20,
                'tablet_gutter' => 10,
                'tablet_width' => 800,
                'mobile_gutter' => 10,
                'mobile_width' => 480
            )

        );

        $settings = shortcode_atts($defaults, $atts);

        $current_page = get_queried_object_id();

        $posts_query = $settings['posts_query'];

        if (is_array($posts_query)) {
            $posts_query['post_status'] = 'publish';
        }
        else {
            $posts_query .= '|post_status:publish';
        }
        if (function_exists('vc_build_loop_query')) {
            list($query_args, $loop) = vc_build_loop_query($posts_query);
        }
        else {
            $query_args = array();
            // just display first 10 portfolio items if the user came directly to this shortcode
            $loop = new WP_Query(array('posts_per_page' => 8, 'post_type' => $settings['post_type']));
        }

        $output = '';

        // Loop through the posts and do something with them.
        if ($loop->have_posts()) :

            ob_start(); ?>

            <div class="ggt-portfolio-wrap ggt-container">

                <?php
                // Check if any taxonomy filter has been applied
                $chosen_terms = $this->get_chosen_terms($query_args);
                if (empty($chosen_terms))
                    $this->_taxonomy_filter = $settings['taxonomy_filter'];
                ?>

                <?php $column_style = GGT_get_column_class(intval($settings['per_line'])); ?>

                <div class="ggt-portfolio-header">

                    <?php if (!empty($settings['heading'])) ?>

<!--                    <h3 class="ggt-heading">--><?php //echo wp_kses_post($settings['heading']); ?><!--</h3>-->

                    <?php

                    if ($settings['filterable'])
                        echo GGT_get_taxonomy_terms_filter($this->_taxonomy_filter, $chosen_terms);

                    ?>

                </div>

                <?php $uniqueid = uniqid(); ?>

                <div id="ggt-portfolio-<?php echo $uniqueid; ?>"
                     class="ggt-portfolio js-isotope ggt-<?php echo $settings['layout_mode']; ?>"
                     data-gutter="<?php echo $settings['gutter']; ?>"
                     data-tablet_gutter="<?php echo $settings['tablet_gutter']; ?>"
                     data-tablet_width="<?php echo $settings['tablet_width']; ?>"
                     data-mobile_gutter="<?php echo $settings['mobile_gutter']; ?>"
                     data-mobile_width="<?php echo $settings['mobile_width']; ?>"
                     data-isotope-options='{ "itemSelector": ".ggt-portfolio-item", "layoutMode": "<?php echo esc_attr($settings['layout_mode']); ?>" }'>

                    <?php while ($loop->have_posts()) : $loop->the_post(); ?>

                        <?php
                        if (get_the_ID() === $current_page)
                            continue; // skip the current page since they can run into infinite loop when users choose All option in build query
                        ?>

                        <?php
                        $style = '';
                        $terms = get_the_terms(get_the_ID(), $this->_taxonomy_filter);
                        if (!empty($terms) && !is_wp_error($terms)) {
                            foreach ($terms as $term) {
                                $style .= ' term-' . $term->term_id;
                            }
                        }
                        ?>

                        <div data-id="id-<?php the_ID(); ?>"
                             class="ggt-portfolio-item <?php echo $style; ?> <?php echo $column_style; ?> ggt-zero-margin">

                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                <?php if ($thumbnail_exists = has_post_thumbnail()): ?>


                                    <div class="ggt-project-image">

                                        <?php if ($settings['image_linkable']): ?>

                                            <a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail('large'); ?> </a>

                                        <?php else: ?>

                                            <?php the_post_thumbnail('large'); ?>

                                        <?php endif; ?>

                                        <div class="ggt-image-info">
                                            <a href="<?php the_permalink(); ?>"><div class="ggt-portfolio-url"></div></a>
                                            <div class="ggt-entry-info">

                                                <?php the_title('<h5 class="ggt-post-title">
                                                    <a href="' . get_permalink() . '" title="' . get_the_title() . '" rel="bookmark">', '<i class="ggctrl-link"></i> </a></h5>'); ?>

                                                <?php echo GGT_get_taxonomy_info($this->_taxonomy_filter); ?>

                                            </div>

                                        </div>

                                        <a href="<?php the_permalink(); ?>">
                                            <div class="ggt-image-overlay"></div>
                                            <div class="overlay-btn"></div>
                                        </a>

                                        <?php if ($settings['display_post_date']) : ?>
                                            <?php if ($settings['display_post_date']): ?>

                                                <?php echo GGT_entry_published(); ?>

                                            <?php endif; ?>
                                        <?php endif; ?>


                                    </div>


                                <?php endif; ?>

                                <?php if ($settings['display_title'] || $settings['display_summary']) : ?>

                                    <div class="ggt-entry-text-wrap <?php echo($thumbnail_exists ? '' : ' nothumbnail'); ?>">



                                        <?php if ($settings['display_title']) : ?>

                                            <?php the_title('<h4 class="entry-title"><a href="' . get_permalink() . '" title="' . get_the_title() . '"
                                               rel="bookmark">', '</a></h4>'); ?>

                                        <?php endif; ?>
                                        <?php echo getPostLikeLink(get_the_ID());?>
                                        <div class="post-view">
                                            <?php echo getPostViews(get_the_ID()); ?>
                                        </div>
                                        <?php if ($settings['display_summary']) : ?>

                                            <div class="entry-summary">

                                                <?php echo get_the_excerpt(); ?>

                                            </div>

                                        <?php endif; ?>

                                        <?php if ($settings['display_author'] || $settings['display_taxonomy']) : ?>

                                            <div class="ggt-entry-meta">

                                                <?php if ($settings['display_author']): ?>

                                                    <?php echo GGT_entry_author(); ?>

                                                <?php endif; ?>


                                                <?php if ($settings['display_taxonomy']): ?>

                                                    <?php echo GGT_get_taxonomy_info($this->_taxonomy_filter); ?>

                                                <?php endif; ?>

                                            </div>

                                        <?php endif; ?>


                                    </div>

                                <?php endif; ?>
                                <?php setPostViews(get_the_ID()); ?>

                            </article>
                            <!-- .hentry -->

                        </div><!--Isotope element -->

                    <?php endwhile; ?>

                    <?php wp_reset_postdata(); ?>

                </div>
                <!-- Isotope items -->

            </div>

            <?php  $output = ob_get_clean();

        endif;

        return $output;
    }


    function map_vc_element() {
        if (function_exists("vc_map")) {

            $general_params = array(

//                array(
//                    'type' => 'textfield',
//                    'param_name' => 'heading',
//                    'heading' => __('Heading for the portfolio/blog', 'ggt-vc-addons'),
//                ),

                array(
                    'type' => 'loop',
                    'param_name' => 'posts_query',
                    'heading' => __('Posts query', 'ggt-vc-addons'),
                    'value' => 'size:10|order_by:date',
                    'settings' => array(
                        'size' => array(
                            'hidden' => false,
                            'value' => 10,
                        ),
                        'order_by' => array('value' => 'date'),
                        'post_type' => array(
                            'hidden' => false,
                            'value' => 'jetpack-portfolio',
                        ),
                    ),
                    'description' => __('Create WordPress loop, to populate content from your site. After you build the query, make sure you choose the right taxonomy below to display for your posts and filter on, based on the post type selected during build query.', 'ggt-vc-addons'),
                    'admin_label' => true
                ),

                array(
                    'type' => 'checkbox',
                    'param_name' => 'image_linkable',
                    'heading' => __('Link Images to Posts?', 'ggt-vc-addons'),
                    "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                ),

                array(
                    'type' => 'dropdown',
                    'param_name' => 'taxonomy_filter',
                    'heading' => __('Choose the taxonomy to display and filter on.', 'ggt-vc-addons'),
                    'description' => __('Choose the taxonomy information to display for posts/portfolio and the taxonomy that is used to filter the portfolio/post. Takes effect only if no query category/tag/taxonomy filters are specified when building query.', 'ggt-vc-addons'),
                    'value' => GGT_get_taxonomies_map(),
                    'std' => 'jetpack-portfolio-type',
                    'group' => 'Options'
                ),

                array(
                    'type' => 'checkbox',
                    'param_name' => 'display_title',
                    'heading' => __('Display posts title below the post/portfolio item?', 'ggt-vc-addons'),
                    "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    'group' => __('Post Info', 'ggt-vc-addons'),
                ),

                array(
                    'type' => 'checkbox',
                    'param_name' => 'display_author',
                    'heading' => __('Display post author info below the post item?', 'ggt-vc-addons'),
                    "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    'group' => __('Post Info', 'ggt-vc-addons'),
                ),

                array(
                    'type' => 'checkbox',
                    'param_name' => 'display_post_date',
                    'heading' => __('Display post date info below the post item?', 'ggt-vc-addons'),
                    "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    'group' => __('Post Info', 'ggt-vc-addons'),
                ),


                array(
                    'type' => 'checkbox',
                    'param_name' => 'display_taxonomy',
                    'heading' => __('Display taxonomy info below the post/portfolio item?', 'ggt-vc-addons'),
                    "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    'group' => __('Post Info', 'ggt-vc-addons'),
                ),

                array(
                    'type' => 'checkbox',
                    'param_name' => 'display_summary',
                    'heading' => __('Display post excerpt/summary below the post/portfolio item?', 'ggt-vc-addons'),
                    "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    'group' => __('Post Info', 'ggt-vc-addons'),
                ),
            );

            $display_params = array(

                array(
                    'type' => 'checkbox',
                    'param_name' => 'filterable',
                    'heading' => __('Filterable?', 'ggt-vc-addons'),
                    "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    'group' => 'Options'
                ),

                array(
                    'type' => 'dropdown',
                    'param_name' => 'layout_mode',
                    'heading' => __('Choose a layout for the portfolio/blog', 'ggt-vc-addons'),
                    'value' => array(
                        __('Fit Rows', 'ggt-vc-addons') => 'fitRows',
                        __('Masonry', 'ggt-vc-addons') => 'masonry',
                    ),
                    'std' => 'fitRows',
                    'group' => 'Options'
                ),

                array(
                    'type' => 'GGT_number',
                    'param_name' => 'per_line',
                    'heading' => __('Columns per row', 'ggt-vc-addons'),
                    'min' => 1,
                    'max' => 5,
                    'integer' => true,
                    'value' => 3
                ),

                array(
                    'type' => 'GGT_number',
                    'param_name' => 'gutter',
                    'heading' => __('Gutter', 'ggt-vc-addons'),
                    'description' => __('Space between columns.', 'ggt-vc-addons'),
                    'value' => 20,
                    'group' => 'Options'
                ),
            );

            $responsive_params = array(

                array(
                    'type' => 'GGT_number',
                    'param_name' => 'tablet_gutter',
                    'heading' => __('Gutter in Tablets', 'ggt-vc-addons'),
                    'description' => __('Space between columns in tablets.', 'ggt-vc-addons'),
                    'value' => 10,
                    'group' => 'Responsive'
                ),

                array(
                    'type' => 'textfield',
                    'param_name' => 'tablet_width',
                    'heading' => __('Tablet Resolution', 'ggt-vc-addons'),
                    'description' => __('The resolution to treat as a tablet resolution.', 'ggt-vc-addons'),
                    'std' => 800,
                    'sanitize' => 'intval',
                    'group' => 'Responsive'
                ),

                array(
                    'type' => 'GGT_number',
                    'param_name' => 'mobile_gutter',
                    'heading' => __('Gutter in Mobiles', 'ggt-vc-addons'),
                    'description' => __('Space between columns in mobiles.', 'ggt-vc-addons'),
                    'value' => 10,
                    'group' => 'Responsive'
                ),

                array(
                    'type' => 'textfield',
                    'param_name' => 'mobile_width',
                    'heading' => __('Mobile Resolution', 'ggt-vc-addons'),
                    'description' => __('The resolution to treat as a mobile resolution.', 'ggt-vc-addons'),
                    'std' => 480,
                    'sanitize' => 'intval',
                    'group' => 'Responsive'
                )
            );

            $params = array_merge($general_params, $display_params, $responsive_params);

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Grid", "ggt-vc-addons"),
                "base" => "GGT_portfolio",
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                'description' => __('Display work or posts with a filterable grid.', 'ggt-vc-addons'),
                "icon" => 'icon-ggt-portfolio',
                "params" => $params
            ));


        }
    }

}

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_portfolio extends WPBakeryShortCode {
    }
}