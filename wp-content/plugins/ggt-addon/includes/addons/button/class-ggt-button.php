<?php

/*
Widget Name: GoGetThemes Pricing Table
Description: Display pricing plans in a multi-column grid.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/

class GGT_Button {

    protected $_per_line;
    protected $_button_text_align;
    protected $_button_text_color;
    protected $_button_bg_color;
    protected $_button_border_color;



    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_button_item', array($this, 'pricing_item_shortcode'));

        add_shortcode('GGT_button', array($this, 'shortcode_func'));

        add_shortcode('GGT_button_single', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_style('ggt-button', plugin_dir_url(__FILE__) . 'css/style.css', array(), GGT_VERSION);

    }



    public function shortcode_func($atts, $content = null, $tag) {

        $per_line = $style = '';

        extract(shortcode_atts(array(
            'per_line' => '4',

        ), $atts));

        $this->_per_line = $per_line;

        ob_start();

        ?>

        <div class="ggt-container buttons-wrap">

            <?php

            do_shortcode($content);

            ?>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $button_text_align = $button_text_color = $button_bg_color = $button_border_color = $button_text = $button_url = $button_new_window = '';
        extract(shortcode_atts(array(
            'text_align' => 'text-align-left',
            "button_text" => '',
            "button_url" => '#',
            "button_new_window" => '',
            "button_text_color" => '',
            "button_bg_color" => '',
            "button_border_color" => '',


        ), $atts));

        $this->_text_align = $text_align;
        $this->_button_text_color = $button_text_color;
        $this->_button_bg_color = $button_bg_color;
        $this->_button_border_color = $button_border_color;

        $button_text_color = '' . esc_attr($this->_button_text_color) . ';';
        $button_bg_color = '' . esc_attr($this->_button_bg_color) . ';';
        $button_border_color = '' . esc_attr($this->_button_border_color) . ';';
        $text_align = '' . esc_attr($this->_text_align) . '';


        if (function_exists('vc_build_link')) {
            $button_url = vc_build_link($button_url);
            $button_button = '<a class="ggt-button" style="color:' . $button_text_color. ' background:' . $button_bg_color . ' border-color:' . $button_border_color . ' " href="' . $button_url['url'] . '" title="' . $button_url['title'] . '" target="' . $button_url['target'] . '">' . $button_text . '</a>';
        }
        else {
            $button_button = '<a class="ggt-button" style="color:' . $button_text_color. ' background:' . $button_bg_color . ' border-color:' . $button_border_color . ' "  href="' . $button_url . '" title="' . $button_title . '" target="_blank">' . $button_text . '</a>';
        }

        ?>

        <div class="button-line <?php echo $text_align; ?>">
            <?php echo $button_button; ?>
        </div>


    <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Button", "ggt-vc-addons"),
                "base" => "GGT_button",
                "as_parent" => array('only' => 'GGT_button_single'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => false,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Display button or buttons.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-button',
                "params" => array(
                    // add params same as with any other content element
                    array(
                        "type" => "text",
                        "param_name" => "add_button",
                        "heading" => __("Add a button as a child", "ggt-vc-addons"),
                        "description" => __("Settings are available in Single button", "ggt-vc-addons"),
                    ),
                ),
            ));


        }
    }


    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Button Item", "ggt-vc-addons"),
                    "base" => "GGT_button_single",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_pricing_table'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-button-add',
                    "params" => array(
                        // add params same as with any other content element

                        array(
                            "type" => "dropdown",
                            "param_name" => "text_align",
                            "heading" => __("Text Align", "ggt-vc-addons"),
                            "description" => __("Choose Text Align", "ggt-vc-addons"),
                            'value' => array(
                                __('Left', 'ggt-vc-addons') => 'text-align-left',
                                __('Center', 'ggt-vc-addons') => 'text-align-center',
                                __('Right', 'ggt-vc-addons') => 'text-align-right',
                            ),
                            'std' => 'text-align-left',
                        ),
                        array(
                            'type' => 'textfield',
                            'param_name' => 'button_text',
                            'heading' => __('Text for Pricing Link/Button', 'ggt-vc-addons'),
                            'description' => __('Provide the text for the link or the button shown for this pricing plan.', 'ggt-vc-addons'),
                            'group' => 'Pricing Link'
                        ),

                        array(
                            'type' => 'vc_link',
                            'param_name' => 'button_url',
                            'heading' => __('URL for the Pricing link/button', 'ggt-vc-addons'),
                            'description' => __('Provide the target URL for the link or the button shown for this pricing plan.', 'ggt-vc-addons'),
                            'group' => 'Pricing Link'
                        ),

                        array(
                            'type' => 'checkbox',
                            'param_name' => 'button_new_window',
                            'heading' => __('Open Button URL in a new window', 'ggt-vc-addons'),
                            'group' => 'Pricing Link'
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'button_text_color',
                            'heading' => __('Button Text color', 'ggt-vc-addons'),
                            'value' => '#ffffff',
                            'group' => __('Custom color' , 'ggt-vc-addons')
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'button_bg_color',
                            'heading' => __('Button Background color', 'ggt-vc-addons'),
                            'value' => '#000000',
                            'group' => __('Custom color' , 'ggt-vc-addons')
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'button_border_color',
                            'heading' => __('Button Border color', 'ggt-vc-addons'),
                            'value' => '#000000',
                            'group' => __('Custom color' , 'ggt-vc-addons')
                        ),

                    )
                )

            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_button extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_button_single extends WPBakeryShortCode {
    }
}