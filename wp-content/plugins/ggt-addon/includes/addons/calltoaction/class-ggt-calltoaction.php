<?php

/*
Widget Name: GoGetThemes Heading
Description: Create heading for display on the top of a section.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/

class GGT_Calltoaction {

    protected $_heading_text_align;
    protected $_short_text;
    protected $_heading_title_color;
    protected $_heading_subtitle_color;
    protected $_heading_description_color;
    protected $_button_text_color;
    protected $_button_bg_color;


    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_calltoaction', array($this, 'shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_style('ggt-calltoaction', plugin_dir_url(__FILE__) . 'css/style.min.css', array(), GGT_VERSION);

    }

    public function shortcode_func($atts, $content = null, $tag) {

        $heading = $style = $subtitle = $heading_text_align = $heading_title_color = $heading_subtitle_color = $heading_description_color = $button_text_color = $button_bg_color = $calltoaction_padding = $button_text = $button_url = $short_text = '';


        extract(shortcode_atts(array(

            'style' => 'style1',
            'heading' => '',
            'subtitle' => false,
            'short_text' => false,
            'text_align' => 'text-align-left',
            'heading_title_color' => '',
            'heading_subtitle_color' => '',
            'heading_description_color' => '',
            'button_text_color' => '',
            'button_bg_color' => '',
            'calltoaction_padding' => '',
            "button_text" => '',
            "button_url" => '#'

        ), $atts));



        $this->_text_align = $text_align;
        $this->_short_text = $short_text;
        $this->_heading_title_color = $heading_title_color;
        $this->_heading_subtitle_color = $heading_subtitle_color;
        $this->_heading_description_color = $heading_description_color;
        $this->_button_text_color = $button_text_color;
        $this->_button_bg_color = $button_bg_color;

        $heading_title_color = ' style="color:' . esc_attr($this->_heading_title_color) . '"';
        $heading_subtitle_color = ' style="color:' . esc_attr($this->_heading_subtitle_color) . '"';
        $heading_description_color = ' style="color:' . esc_attr($this->_heading_description_color) . '"';

        ob_start();




        if (function_exists('vc_build_link')) {
            $button_url = vc_build_link($button_url);
            $button_button = '<a class="ggt-button" style="color:' . $button_text_color . '; background:' . $button_bg_color . '; " href="' . $button_url['url'] . '" title="' . $button_url['title'] . '" target="' . $button_url['target'] . '">' . $button_text . '</a>';
        }
        else {
            $button_button = '<a class="ggt-button" style="color:' . $button_text_color . '; background:' . $button_bg_color . '; "  href="' . $button_url . '"  target="_blank">' . $button_text . '</a>';
        }

        ?>
        <div class="<?php echo $text_align; ?>">
            <div class="ggt-calltoaction ggt-<?php echo $style; ?>  <?php echo(!empty($calltoaction_padding) ? ' responsive-padding' : ''); ?>">
                <div class="ggt-calltoaction-subtitle" <?php echo $heading_subtitle_color; ?>><?php echo esc_html($subtitle); ?></div>
                <h3 class="ggt-calltoaction-title" <?php echo $heading_title_color; ?>><?php echo wp_kses_post($heading); ?></h3>
                <span class="ggt-calltoaction-text" <?php echo $heading_description_color; ?>><?php echo wp_kses_post($short_text); ?></span>
            </div>
            <div class="ggt-calltoaction-btn ggt-container"><?php echo $button_button; ?></div>
        </div>
        <?php

        $output = ob_get_clean();

        return $output;
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Calltoaction", "ggt-vc-addons"),
                "base" => "GGT_calltoaction",
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                'description' => __('Create Call to Action for a section.', 'ggt-vc-addons'),
                "icon" => 'icon-ggt-calltoaction',
                "params" => array(
                    // add params same as with any other content element
                    array(
                        "type" => "dropdown",
                        "param_name" => "text_align",
                        "heading" => __("Text Align", "ggt-vc-addons"),
                        "description" => __("Choose Text Align", "ggt-vc-addons"),
                        'value' => array(
                            __('Left', 'ggt-vc-addons') => 'text-align-left',
                            __('Center', 'ggt-vc-addons') => 'text-align-center',
                            __('Right', 'ggt-vc-addons') => 'text-align-right',
                        ),
                        'std' => 'text-align-left',
                    ),
//                    array(
//                        "type" => "dropdown",
//                        "param_name" => "style",
//                        "heading" => __("Choose Style", "ggt-vc-addons"),
//                        "description" => __("Choose the particular style of heading you need", "ggt-vc-addons"),
//                        'value' => array(
//                            __('Title + Description', 'ggt-vc-addons') => 'style1',
//                            __('Subtitle + Title + Description + Button', 'ggt-vc-addons') => 'style2',
//                            __('Title + Separator', 'ggt-vc-addons') => 'style3',
//                        ),
//                        'std' => 'style1',
//                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'heading',
                        "admin_label" => true,
                        'heading' => __('Title', 'ggt-vc-addons'),
                        'description' => __('Title for the heading.', 'ggt-vc-addons'),
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'subtitle',
                        'heading' => __('Subheading or Subtitle', 'ggt-vc-addons'),
                        'description' => __('A subtitle displayed above the title heading.', 'ggt-vc-addons'),
//                        'dependency' => array(
//                            'element' => 'style',
//                            'value' => 'style2',
//                        ),
                    ),
                    array(
                        'type' => 'textarea',
                        'param_name' => 'short_text',
                        'heading' => __('Short Text', 'ggt-vc-addons'),
                        'description' => __('Short text generally displayed below the heading title.', 'ggt-vc-addons'),

                    ),
                    array(
                        'type' => 'checkbox',
                        'param_name' => 'calltoaction_padding',
                        'heading' => __('Responsive Padding', 'ggt-vc-addons'),
                        'description' => __('Add Large Responsive Padding', 'ggt-vc-addons'),
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'button_text',
                        'heading' => __('Text for Button', 'ggt-vc-addons'),
                        'description' => __('Provide the text for the link or the button shown', 'ggt-vc-addons'),
//                        'dependency' => array(
//                            'element' => 'style',
//                            'value' => array('style2'),
//                        'group' => 'Link'

//                        ),
                    ),
                    array(
                        'type' => 'vc_link',
                        'param_name' => 'button_url',
                        'heading' => __('URL for button', 'ggt-vc-addons'),
                        'description' => __('Provide the target URL for the link or the button shown', 'ggt-vc-addons'),
//                        'dependency' => array(
//                            'element' => 'style',
//                            'value' => array('style2'),
//                            'group' => 'Link'
//                    ),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'heading_title_color',
                        'heading' => __('Title color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons'),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'heading_subtitle_color',
                        'heading' => __('Subtitle color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'heading_description_color',
                        'heading' => __('Description color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'button_text_color',
                        'heading' => __('Button Text color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'button_bg_color',
                        'heading' => __('Button Background color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')

                ),

            )
            ));



        }
    }

}

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_calltoaction extends WPBakeryShortCode {
    }
}