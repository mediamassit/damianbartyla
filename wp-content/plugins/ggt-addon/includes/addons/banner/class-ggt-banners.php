<?php

/*
Widget Name: GoGetThemes Banners
Description: Display list of your banners in a multi-column grid.
Author: GoGetThemes.com
Author URI: http://www.gogetthemes.com
*/

class GGT_Banners {

    protected $_per_line;
    protected $_banner_title_color;

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_banners', array($this, 'shortcode_func'));

        add_shortcode('GGT_single_banner', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_style('ggt-banners', plugin_dir_url(__FILE__) . 'css/style.min.css', array(), GGT_VERSION);

    }

    public function shortcode_func($atts, $content = null, $tag) {

        $per_line = '';

        extract(shortcode_atts(array(
            'per_line' => '4',

        ), $atts));

        $this->_per_line = $per_line;

        ob_start();

        ?>

        <div class="ggt-banners ggt-container">

            <?php

            do_shortcode($content);

            ?>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $banner_name = $banner_subtitle = $banner_title_color = $banner_description = $banner_image = $banner_url = '';
        extract(shortcode_atts(array(
            'banner_name' => '',
            'banner_subtitle' => '',
            'banner_description' => '',
            'banner_url' => false,
            'banner_title_color' => '#ffffff',
            'banner_image' => ''

        ), $atts));

        $column_style = GGT_get_column_class(intval($this->_per_line));
        $this->_banner_title_color = $banner_title_color;

        ?>

        <div class="ggt-banner <?php echo $column_style; ?> ggt-zero-margin">

            <?php echo wp_get_attachment_image($banner_image, 'full', false, array('class' => 'ggt-image full', 'alt' => $banner_name)); ?>


            <?php if (!empty($banner_url) && function_exists('vc_build_link')): ?>
                <?php $banner_url = vc_build_link($banner_url); ?>
                <a href="<?php echo esc_url($banner_url['url']); ?>"  title="<?php echo esc_html($banner_url['title']); ?>" target="<?php echo $banner_url['target']; ?>">

                    <div class="ggt-banner-name" style="color:<?php echo esc_html($banner_title_color); ?>;">
                        <div class="banner-title"><h3><?php echo esc_html($banner_name); ?></h3></div>
                        <div class="banner-subtitle"><?php echo esc_html($banner_subtitle); ?></div>
                    </div>
                    <div class="ggt-banner-description">
                        <span><?php echo esc_html($banner_description); ?></span>
                    </div>
                    <div class="ggt-image-overlay"></div>
                </a>
                <?php else: ?>
            <?php endif; ?>
        </div>

    <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Banners", "ggt-vc-addons"),
                "base" => "GGT_banners",
                "as_parent" => array('only' => 'GGT_single_banner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Display banners in a multi-column grid.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-banners',
                "params" => array(

                    array(
                        "type" => "GGT_number",
                        "param_name" => "per_line",
                        "value" => 4,
                        "min" => 1,
                        "max" => 6,
                        "suffix" => '',
                        "heading" => __("Bannerss per row", "ggt-vc-addons"),
                        "description" => __("The number of columns to display per row of the banners", "ggt-vc-addons")
                    ),
                ),
            ));


        }
    }


    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Banner item", "ggt-vc-addons"),
                    "base" => "GGT_single_banner",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_banners'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-banner',
                    "params" => array(
                        // add params same as with any other content element
                        array(
                            'type' => 'textfield',
                            'param_name' => 'banner_name',
                            "admin_label" => true,
                            'heading' => __('Title', 'ggt-vc-addons'),
                            'description' => __('Name of the banner.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'textfield',
                            'param_name' => 'banner_subtitle',
                            "admin_label" => true,
                            'heading' => __('Subtitle', 'ggt-vc-addons'),
                            'description' => __('Banner Subtitle', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'textfield',
                            'param_name' => 'banner_description',
                            "admin_label" => true,
                            'heading' => __('Description', 'ggt-vc-addons'),
                            'description' => __('Banner Description.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'vc_link',
                            'param_name' => 'banner_url',
                            'heading' => __('Banner URL', 'ggt-vc-addons'),
                            'description' => __('The website of the banner', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'attach_image',
                            'param_name' => 'banner_image',
                            'heading' => __('Banner Logo.', 'ggt-vc-addons'),
                            'description' => __('The logo image for the banner', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'banner_title_color',
                            'heading' => __('Banner Title color', 'ggt-vc-addons'),
                            'value' => '#ffffff',
                            'group' => __('Custom color' , 'ggt-vc-addons')
                        ),

                    )
                )

            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_banners extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_single_banner extends WPBakeryShortCode {
    }
}