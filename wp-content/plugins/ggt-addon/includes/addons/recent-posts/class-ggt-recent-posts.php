<?php

/*
Widget Name: GoGetThemes Recent Posts
Description: Add spacer between rows and elements that changes based on device resolution.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/

class GGT_Recent_posts {

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_recent_posts', array($this, 'shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_script('ggt-spacer', plugin_dir_url(__FILE__) . 'js/spacer' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);
    }

    public function shortcode_func($atts, $content = null, $tag) {

        $settings = shortcode_atts(array(
            'desktop_spacing' => 50,
            'tablet_spacing' => 30,
            'tablet_width' => 960,
            'mobile_spacing' => 10,
            'mobile_width' => 480
        ), $atts);

        $uniqueid = uniqid();

        ob_start();

        ?>
11111
        <ul>
            <?php
            $args = array( 'numberposts' => '5', 'tax_query' => array(
                array(
                    'taxonomy' => 'post_format',
                    'field' => 'slug',
                    'terms' => 'post-format-aside',
                    'operator' => 'NOT IN'
                ),
                array(
                    'taxonomy' => 'post_format',
                    'field' => 'slug',
                    'terms' => 'post-format-image',
                    'operator' => 'NOT IN'
                )
            ) );
            $recent_posts = wp_get_recent_posts( $args );
            foreach( $recent_posts as $recent ){
                echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   ( __($recent["post_title"])).'</a> </li> ';
            }
            wp_reset_query();
            ?>
        </ul>


--------
        <?php
        query_posts('order=ASC&post_standart=future');

while (have_posts()) : the_post(); ?>

    <li><?php the_title();?>
        <?php the_category( ' ' ); ?>
        <?php the_excerpt(); ?>
        <?php the_post_thumbnail(); ?></li>

<?php endwhile; ?>


222222
        <?php

        $output = ob_get_clean();

        return $output;
    }

   

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Recent Posts", "ggt-vc-addons"),
                "base" => "GGT_recent_posts",
                "as_parent" => array('only' => 'GGT_statsbar_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => false,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Add spacer between rows and elements.', 'ggt-vc-addons'),
                "icon" => 'icon-ggt-spacer',
                "params" => array(

                    array(
                        "type" => "GGT_number",
                        "heading" => __("Desktop Spacing", "ggt-vc-addons"),
                        "description" => __("Space in pixels in Desktop Resolution", "ggt-vc-addons"),
                        "param_name" => "desktop_spacing",
                        "admin_label" => true,
                        "value" => 50,
                        "min" => 1,
                        "max" => 500,
                        "suffix" => "px",
                    ),
                    array(
                        "type" => "GGT_number",
                        "heading" => __("Tablet Resolution", "ggt-vc-addons"),
                        "description" => __("The resolution to treat as a tablet resolution", "ggt-vc-addons"),
                        "param_name" => "tablet_width",
                        "admin_label" => true,
                        "value" => 960,
                        "min" => 600,
                        "max" => 1400,
                        "suffix" => "px",
                        "edit_field_class" => "vc_col-sm-6 vc_column"
                    ),
                    array(
                        "type" => "GGT_number",
                        "heading" => __("Tablet Spacing", "ggt-vc-addons"),
                        "description" => __("Space in Tablet Resolution", "ggt-vc-addons"),
                        "param_name" => "tablet_spacing",
                        "admin_label" => true,
                        "value" => 30,
                        "min" => 1,
                        "max" => 400,
                        "suffix" => "px",
                        "edit_field_class" => "vc_col-sm-6 vc_column"
                    ),
                    array(
                        "type" => "GGT_number",
                        "heading" => __("Mobile Resolution", "ggt-vc-addons"),
                        "description" => __("The resolution to treat as a mobile resolution", "ggt-vc-addons"),
                        "param_name" => "mobile_width",
                        "admin_label" => true,
                        "value" => 480,
                        "min" => 320,
                        "max" => 800,
                        "suffix" => "px",
                        "edit_field_class" => "vc_col-sm-6 vc_column"
                    ),
                    array(
                        "type" => "GGT_number",
                        "heading" => __("Mobile Spacing", "ggt-vc-addons"),
                        "description" => __("Space in Mobile Resolution", "ggt-vc-addons"),
                        "param_name" => "mobile_spacing",
                        "admin_label" => true,
                        "value" => 10,
                        "min" => 1,
                        "max" => 300,
                        "suffix" => "px",
                        "edit_field_class" => "vc_col-sm-6 vc_column"
                    ),
                ),
            ));


        }
    }

}

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_recent_posts extends WPBakeryShortCode {
    }
}