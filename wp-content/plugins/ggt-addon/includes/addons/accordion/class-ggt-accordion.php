<?php

/*
Widget Name: GoGetThemes Accordion
Description: Displays collapsible content panels to help display information when space is limited.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/


class GGT_Accordion {

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_accordion', array($this, 'shortcode_func'));

        add_shortcode('GGT_panel', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_script('ggt-accordion', plugin_dir_url(__FILE__) . 'js/accordion' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        wp_enqueue_style('ggt-accordion', plugin_dir_url(__FILE__) . 'css/style.min.css', array(), GGT_VERSION);
    }

    public function shortcode_func($atts, $content = null, $tag) {

        $style = $toggle = '';

        extract(shortcode_atts(array(
            'style' => 'style1',
            'toggle' => false
        ), $atts));

        ob_start();

        ?>

        <div class="ggt-accordion ggt-<?php echo $style; ?>" data-toggle="<?php echo ($toggle ? "true" : "false"); ?>">

            <?php

            do_shortcode($content);

            ?>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $panel_title = $percentage = '';
        extract(shortcode_atts(array(
            'panel_title' => ''

        ), $atts));

        ?>

        <div class="ggt-panel">

            <div class="ggt-panel-title"><?php echo esc_html($panel_title); ?></div>

            <div class="ggt-panel-content"><?php echo do_shortcode($content) ?></div>

        </div>

        <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Accordion", "ggt-vc-addons"),
                "base" => "GGT_accordion",
                "as_parent" => array('only' => 'GGT_panel'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => false,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Display collapsible content panels.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-accordion',
                "params" => array(


                    array(
                        "type" => "dropdown",
                        "param_name" => "style",
                        "heading" => __("Choose Accordion Style", "ggt-vc-addons"),
                        "description" => __("Choose the particular style of accordion you need", "ggt-vc-addons"),
                        'value' => array(
                            __('Style 1', 'ggt-vc-addons') => 'style1',
                            __('Style 2', 'ggt-vc-addons') => 'style2',
                            __('Style 3', 'ggt-vc-addons') => 'style3',
                        ),
                        'std' => 'style1',
                    ),

                    array(
                        'type' => 'checkbox',
                        'param_name' => 'toggle',
                        'heading' => __('Allow to function like toggle?', 'ggt-vc-addons'),
                        "description" => __("Check if multiple elements can be open at the same time.", "ggt-vc-addons"),
                        "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    ),
                ),
            ));


        }
    }


    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Accordion Item", "ggt-vc-addons"),
                    "base" => "GGT_panel",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_accordion'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-panel',
                    "params" => array(
                        // add params same as with any other content element
                        array(
                            'type' => 'textfield',
                            'param_name' => 'panel_title',
                            "admin_label" => true,
                            'heading' => __('Title', 'ggt-vc-addons'),
                            'description' => __('Title for Accordion Item.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'textarea_html',
                            'param_name' => 'content',
                            'heading' => __('Content', 'ggt-vc-addons'),
                            'description' => __('The collapsible content in the accordion.', 'ggt-vc-addons'),
                        ),

                    )
                )

            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_accordion extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_panel extends WPBakeryShortCode {
    }
}