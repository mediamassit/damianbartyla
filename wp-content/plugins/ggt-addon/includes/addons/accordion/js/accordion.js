
jQuery(function ($) {

    $('.ggt-accordion').each(function () {

        var accordion = $(this);

        new GGT_Accordion(accordion);

    });

});

var GGT_Accordion = function (accordion) {

    // toggle elems
    this.panels = accordion.find('.ggt-panel');

    this.toggle = false;

    if (accordion.data('toggle') == true)
        this.toggle = true;

    this.current = null;

    // init events
    this.initEvents();
};

GGT_Accordion.prototype.show = function (panel) {

    if (this.toggle) {
        if (panel.hasClass('ggt-active')) {
            this.close(panel);
        }
        else {
            this.open(panel);
        }
    }
    else {
        // if the panel is already open, close it else open it after closing existing one
        if (panel.hasClass('ggt-active')) {
            this.close(panel);
            this.current = null;
        }
        else {
            this.close(this.current);
            this.open(panel);
            this.current = panel;
        }
    }

};

GGT_Accordion.prototype.close = function (panel) {

    if (panel !== null) {
        panel.children('.ggt-panel-content').slideUp(300);
        panel.removeClass('ggt-active');
    }

};

GGT_Accordion.prototype.open = function (panel) {

    if (panel !== null) {
        panel.children('.ggt-panel-content').slideDown(300);
        panel.addClass('ggt-active');
    }

};


GGT_Accordion.prototype.initEvents = function () {

    var self = this;

    this.panels.find('.ggt-panel-title').click(function (event) {

        event.preventDefault();

        var panel = jQuery(this).parent();

        self.show(panel);
    });
};

