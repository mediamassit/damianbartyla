<?php

/*
Widget Name: GoGetThemes Heading
Description: Create heading for display on the top of a section.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/

class GGT_Heading {

    protected $_heading_text_align;
    protected $_heading_title_color;
    protected $_heading_subtitle_color;
    protected $_heading_description_color;


    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_heading', array($this, 'shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_style('ggt-heading', plugin_dir_url(__FILE__) . 'css/style.css', array(), GGT_VERSION);

    }

    public function shortcode_func($atts, $content = null, $tag) {

        $heading = $style = $subtitle = $heading_text_align = $heading_title_color = $heading_subtitle_color = $heading_description_color = $short_text = '';


        extract(shortcode_atts(array(

            'style' => 'style1',
            'heading' => '',
            'subtitle' => false,
            'short_text' => false,
            'text_align' => 'text-align-left',
            'heading_title_color' => '',
            'heading_subtitle_color' => '',
            'heading_description_color' => ''

        ), $atts));



        $this->_text_align = $text_align;
        $this->_heading_title_color = $heading_title_color;
        $this->_heading_subtitle_color = $heading_subtitle_color;
        $this->_heading_description_color = $heading_description_color;

        $heading_title_color = ' style="color:' . esc_attr($this->_heading_title_color) . '"';
        $heading_subtitle_color = ' style="color:' . esc_attr($this->_heading_subtitle_color) . '"';
        $heading_description_color = ' style="color:' . esc_attr($this->_heading_description_color) . '"';

        ob_start();




        ?>

        <div class="ggt-heading ggt-<?php echo $style; ?> <?php echo $text_align; ?>">

            <?php if ($style == 'style2' && !empty($subtitle)): ?>

                <div class="ggt-subtitle" <?php echo $heading_subtitle_color; ?>><?php echo esc_html($subtitle); ?></div>

            <?php endif; ?>

            <h2 class="ggt-title" <?php echo $heading_title_color; ?>><?php echo wp_kses_post($heading); ?></h2>

            <?php if ($style != 'style3' && !empty($short_text)): ?>

                <p class="ggt-text" <?php echo $heading_description_color; ?>><?php echo wp_kses_post($short_text); ?></p>

            <?php endif; ?>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Heading", "ggt-vc-addons"),
                "base" => "GGT_heading",
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                'description' => __('Create heading for a section.', 'ggt-vc-addons'),
                "icon" => 'icon-ggt-heading',
                "params" => array(
                    // add params same as with any other content element
                    array(
                        "type" => "dropdown",
                        "param_name" => "text_align",
                        "heading" => __("Text Align", "ggt-vc-addons"),
                        "description" => __("Choose Text Align", "ggt-vc-addons"),
                        'value' => array(
                            __('Left', 'ggt-vc-addons') => 'text-align-left',
                            __('Center', 'ggt-vc-addons') => 'text-align-center',
                            __('Right', 'ggt-vc-addons') => 'text-align-right',
                        ),
                        'std' => 'text-align-left',
                    ),
                    array(
                        "type" => "dropdown",
                        "param_name" => "style",
                        "heading" => __("Choose Style", "ggt-vc-addons"),
                        "description" => __("Choose the particular style of heading you need", "ggt-vc-addons"),
                        'value' => array(
                            __('Title + Description', 'ggt-vc-addons') => 'style1',
                            __('Subtitle + Title + Description', 'ggt-vc-addons') => 'style2',
                            __('Title + Separator', 'ggt-vc-addons') => 'style3',
                        ),
                        'std' => 'style1',
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'heading',
                        "admin_label" => true,
                        'heading' => __('Title', 'ggt-vc-addons'),
                        'description' => __('Title for the heading.', 'ggt-vc-addons'),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'heading_title_color',
                        'heading' => __('Title color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'heading_subtitle_color',
                        'heading' => __('Subtitle color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'heading_description_color',
                        'heading' => __('Description color', 'ggt-vc-addons'),
                        'value' => 'transparent',
                        'group' => __('Custom color' , 'ggt-vc-addons')
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'subtitle',
                        'heading' => __('Subheading or Subtitle', 'ggt-vc-addons'),
                        'description' => __('A subtitle displayed above the title heading.', 'ggt-vc-addons'),
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style2',
                        ),
                    ),
                    array(
                        'type' => 'textarea',
                        'param_name' => 'short_text',
                        'heading' => __('Short Text', 'ggt-vc-addons'),
                        'description' => __('Short text generally displayed below the heading title.', 'ggt-vc-addons'),
                        'dependency' => array(
                            'element' => 'style',
                            'value' => array('style1' , 'style2'),
                        ),
                    ),
                ),
            ));


        }
    }

}

if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_heading extends WPBakeryShortCode {
    }
}