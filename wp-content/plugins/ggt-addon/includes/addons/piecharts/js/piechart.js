jQuery(function ($) {

    $('.ggt-piecharts').waypoint(function (direction) {

        //var cc = jQuery(parent);
        //console.log(this);
       // var $this = $('.container');

        $(this.element).find('.ggt-piechart .ggt-percentage').each(function () {

            var track_color = $(this).data('track-color');
            var bar_color = $(this).data('bar-color');

            $(this).easyPieChart({
                animate: 2000,
                lineWidth: 10,
                barColor: bar_color,
                trackColor: track_color,
                scaleColor: false,
                lineCap: 'round',
                size: 180

            });

        });

    }, { offset: $.waypoints('viewportHeight') - 100,
        triggerOnce: true});


});