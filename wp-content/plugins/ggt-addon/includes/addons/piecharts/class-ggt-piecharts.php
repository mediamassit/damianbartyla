<?php

/*
Widget Name: GoGetThemes Piecharts
Description: Display one or more piecharts depicting a percentage value in a multi-column grid.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/

class GGT_Piecharts {

    protected $_per_line;
    protected $_bar_color;
    protected $_track_color;

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_piecharts', array($this, 'shortcode_func'));

        add_shortcode('GGT_piechart_item', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_script('ggt-waypoints', GGT_PLUGIN_URL . 'assets/js/jquery.waypoints' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        wp_enqueue_script('ggt-stats', GGT_PLUGIN_URL . 'assets/js/jquery.stats' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        //wp_enqueue_script('ggt-piecharts', plugin_dir_url(__FILE__) . 'js/piechart' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);
        wp_enqueue_script('ggt-piecharts', plugin_dir_url(__FILE__) . 'js/piechart.min.js', array('jquery'), GGT_VERSION);

        wp_enqueue_style('ggt-piecharts', plugin_dir_url(__FILE__) . 'css/style.css', array(), GGT_VERSION);

    }

    public function shortcode_func($atts, $content = null, $tag) {

        $per_line = $bar_color = $track_color = '';

        extract(shortcode_atts(array(
            'per_line' => '2',
            'bar_color' => '#f94213',
            'track_color' => '#dddddd',

        ), $atts));

        $this->_per_line = $per_line;
        $this->_bar_color = $bar_color;
        $this->_track_color = $track_color;

        ob_start();

        ?>

        <div class="ggt-piecharts ggt-container">

            <?php

            do_shortcode($content);

            ?>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $stats_title = $percentage = '';
        extract(shortcode_atts(array(
            'stats_title' => '',
            'percentage' => 50

        ), $atts));

        $column_style = GGT_get_column_class(intval($this->_per_line));

        $bar_color = ' data-bar-color="' . esc_attr($this->_bar_color) . '"';
        $track_color = ' data-track-color="' . esc_attr($this->_track_color) . '"';

        ?>

        <div class="ggt-piechart <?php echo $column_style; ?>">

            <div class="ggt-percentage" <?php echo $bar_color; ?> <?php echo $track_color; ?>
                 data-percent="<?php echo intval($percentage); ?>">

                <span><?php echo intval($percentage); ?><sup>%</sup></span>

            </div>

            <div class="ggt-label"><?php echo esc_html($stats_title); ?></div>

        </div>

    <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Piecharts", "ggt-vc-addons"),
                "base" => "GGT_piecharts",
                "as_parent" => array('only' => 'GGT_piechart_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Display piecharts in a multi-column grid.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-piecharts',
                "params" => array(

                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'bar_color',
                        'heading' => __('Bar color', 'ggt-vc-addons'),
                        'value' => '#f94213'
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'track_color',
                        'heading' => __('Track color', 'ggt-vc-addons'),
                        'value' => '#dddddd'
                    ),

                    array(
                        "type" => "GGT_number",
                        "param_name" => "per_line",
                        "value" => 4,
                        "min" => 1,
                        "max" => 5,
                        "suffix" => '',
                        "heading" => __("Piecharts per row", "ggt-vc-addons"),
                        "description" => __("The number of columns to display per row of the piecharts", "ggt-vc-addons")
                    ),
                ),
            ));


        }
    }


    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Pierchart Item", "my-text-domain"),
                    "base" => "GGT_piechart_item",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_piecharts'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-piechart',
                    "params" => array(
                        // add params same as with any other content element
                        array(
                            'type' => 'textfield',
                            'param_name' => 'stats_title',
                            "admin_label" => true,
                            'heading' => __('Stats Title', 'ggt-vc-addons'),
                            'description' => __('Title for the piechart.', 'ggt-vc-addons'),
                        ),
                        array(
                            "type" => "GGT_number",
                            "param_name" => "percentage",
                            "value" => 50,
                            "min" => 0,
                            "max" => 100,
                            "suffix" => '%',
                            "heading" => __("Percentage Value", "ggt-vc-addons"),
                            "description" => __("The percentage value for the piechart.", "ggt-vc-addons")
                        ),

                    )
                )

            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_piecharts extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_piechart_item extends WPBakeryShortCode {
    }
}