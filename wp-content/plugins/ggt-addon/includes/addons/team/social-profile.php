<?php

?>

<div class="ggt-social-wrap">
    <div class="ggt-social-btn"></div>
    <div class="ggt-social-list-wrap">
        <div class="ggt-social-list">
            <div class="ggt-social-list-icon">
            <?php
            if ($member_email)
                echo '<div class="ggt-social-list-item"><a class="" href="mailto:' . $member_email . '" title="' . __("Contact Us", 'ggt-vc-addons') . '"><i class="ggtctrl-mail"></i></a></div>';
            if ($facebook_url)
                echo '<div class="ggt-social-list-item"><a class="" href="' . $facebook_url . '" target="_blank" title="' . __("Follow on Facebook", 'ggt-vc-addons') . '"><i class="ggtctrl-facebook"></i></a></div>';
            if ($twitter_url)
                echo '<div class="ggt-social-list-item"><a class="" href="' . $twitter_url . '" target="_blank" title="' . __("Subscribe to Twitter Feed", 'ggt-vc-addons') . '"><i class="ggtctrl-twitter"></i></a></div>';
            if ($linkedin_url)
                echo '<div class="ggt-social-list-item"><a class="" href="' . $linkedin_url . '" target="_blank" title="' . __("View LinkedIn Profile", 'ggt-vc-addons') . '"><i class="ggtctrl-linkedin"></i></a></div>';
            if ($googleplus_url)
                echo '<div class="ggt-social-list-item"><a class="" href="' . $googleplus_url . '" target="_blank" title="' . __("Follow on Google Plus", 'ggt-vc-addons') . '"><i class="ggtctrl-googleplus"></i></a></div>';
            if ($instagram_url)
                echo '<div class="ggt-social-list-item"><a class="" href="' . $instagram_url . '" target="_blank" title="' . __("View Instagram Feed", 'ggt-vc-addons') . '"><i class="ggtctrl-instagram"></i></a></div>';
            if ($pinterest_url)
                echo '<div class="ggt-social-list-item"><a class="" href="' . $pinterest_url . '" target="_blank" title="' . __("Subscribe to Pinterest Feed", 'ggt-vc-addons') . '"><i class="ggtctrl-pinterest"></i></a></div>';
            if ($dribbble_url)
                echo '<div class="ggt-social-list-item"><a class="" href="' . $dribbble_url . '" target="_blank" title="' . __("View Dribbble Portfolio", 'ggt-vc-addons') . '"><i class="ggtctrl-dribbble"></i></a></div>';

            ?>
            </div>
        </div>
    </div>
</div>
