<?php

/*
Widget Name: GoGetThemes Team Members
Description: Display a list of your team members optionally in a multi-column grid.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/


class GGT_Team {

    protected $_per_line;
    protected $_style;

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_team', array($this, 'shortcode_func'));

        add_shortcode('GGT_team_member', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_style('ggt-team-members', plugin_dir_url(__FILE__) . 'css/style.css', array(), GGT_VERSION);

    }

    public function shortcode_func($atts, $content = null, $tag) {

        $per_line = $style = '';

        extract(shortcode_atts(array(
            'per_line' => '3',
            'style' => 'style1',

        ), $atts));

        $this->_per_line = $per_line;
        $this->_style = $style;

        ob_start();

        ?>

        <div class="ggt-team-members ggt-<?php echo $style; ?> ggt-container">

            <?php

            do_shortcode($content);

            ?>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $member_name = $member_image = $member_details = $member_position = $member_email = '';
        extract(shortcode_atts(array(
            'member_name' => '',
            'member_image' => '',
            'member_details' => '',
            "member_position" => '',
            'member_email' => false,
            'facebook_url' => false,
            'twitter_url' => false,
            'flickr_url' => false,
            'youtube_url' => false,
            'linkedin_url' => false,
            'googleplus_url' => false,
            'vimeo_url' => false,
            'instagram_url' => false,
            'behance_url' => false,
            'pinterest_url' => false,
            'skype_url' => false,
            'dribbble_url' => false,

        ), $atts));

        $style = $this->_style;

        $column_style = '';

        if ($style == 'style1') {
            $column_style = GGT_get_column_class(intval($this->_per_line));
        }
        
        ?>

        <div class="ggt-team-member-wrapper <?php echo $column_style; ?>">

            <div class="ggt-team-member">

                <div class="ggt-image-wrapper">

                    <?php echo wp_get_attachment_image($member_image, 'full', false, array('class' => 'ggt-image full')); ?>

                    <?php if ($style == 'style1'): ?>

                        <?php include 'social-profile.php'; ?>

                    <?php endif; ?>

                </div>

                <div class="ggt-team-member-text">

                    <h4 class="ggt-title"><?php echo esc_html($member_name) ?></h4>

                    <div class="ggt-team-member-position">

                        <?php echo esc_html($member_position) ?>

                    </div>

                    <div class="ggt-team-member-details">

                        <?php echo wp_kses_post($member_details) ?>

                    </div>

                    <?php if ($style == 'style2'): ?>

                        <?php include 'social-profile.php'; ?>

                    <?php endif; ?>

                </div>

            </div>

        </div>

    <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Team members", "ggt-vc-addons"),
                "base" => "GGT_team",
                "as_parent" => array('only' => 'GGT_team_member'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Create team members.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-team',
                "params" => array(
                    // add params same as with any other content element
                    array(
                        "type" => "dropdown",
                        "param_name" => "style",
                        "heading" => __("Choose Style", "ggt-vc-addons"),
                        "description" => __("Choose the particular style of team you need", "ggt-vc-addons"),
                        'value' => array(
                            __('Style 1', 'ggt-vc-addons') => 'style1',
                            __('Style 2', 'ggt-vc-addons') => 'style2',
                        ),
                        'std' => 'style1',
                    ),
                    array(
                        "type" => "GGT_number",
                        "param_name" => "per_line",
                        "value" => 3,
                        "min" => 1,
                        "max" => 5,
                        "suffix" => '',
                        "heading" => __("Columns per row", "ggt-vc-addons"),
                        "description" => __("The number of team members to display per row of the team", "ggt-vc-addons"),
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style1',
                        ),
                    ),
                ),
            ));


        }
    }


    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Team Member Item", "my-text-domain"),
                    "base" => "GGT_team_member",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_team'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-team-member',
                    "category" => __('Team', 'ggt-vc-addons'),
                    "params" => array(
                        // add params same as with any other content element
                        array(
                            'type' => 'textfield',
                            'param_name' => 'member_name',
                            "admin_label" => true,
                            'heading' => __('Team Member Name', 'ggt-vc-addons'),
                            'description' => __('Name of the team member.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'attach_image',
                            'param_name' => 'member_image',
                            'heading' => __('Team Member Image.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'textfield',
                            'param_name' => 'member_position',
                            'heading' => __('Position', 'ggt-vc-addons'),
                            'description' => __('Specify the position/title of the team member.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textarea',
                            'param_name' => 'member_details',
                            'heading' => __('Short details', 'ggt-vc-addons'),
                            'description' => __('Provide a short writeup for the team member', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textfield',
                            'param_name' => 'member_email',
                            'group' => __('Social Profile', 'ggt-vc-addons'),
                            'heading' => __('Email Address', 'ggt-vc-addons'),
                            'description' => __('Enter the email address of the team member.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textfield',
                            'param_name' => 'facebook_url',
                            'group' => __('Social Profile', 'ggt-vc-addons'),
                            'heading' => __('Facebook Page URL', 'ggt-vc-addons'),
                            'description' => __('URL of the Facebook page of the team member.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textfield',
                            'param_name' => 'twitter_url',
                            'group' => __('Social Profile', 'ggt-vc-addons'),
                            'heading' => __('Twitter Profile URL', 'ggt-vc-addons'),
                            'description' => __('URL of the Twitter page of the team member.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textfield',
                            'param_name' => 'linkedin_url',
                            'group' => __('Social Profile', 'ggt-vc-addons'),
                            'heading' => __('LinkedIn Page URL', 'ggt-vc-addons'),
                            'description' => __('URL of the LinkedIn profile of the team member.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textfield',
                            'param_name' => 'pinterest_url',
                            'group' => __('Social Profile', 'ggt-vc-addons'),
                            'heading' => __('Pinterest Page URL', 'ggt-vc-addons'),
                            'description' => __('URL of the Pinterest page for the team member.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textfield',
                            'param_name' => 'dribbble_url',
                            'group' => __('Social Profile', 'ggt-vc-addons'),
                            'heading' => __('Dribbble Profile URL', 'ggt-vc-addons'),
                            'description' => __('URL of the Dribbble profile of the team member.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textfield',
                            'param_name' => 'google_plus_url',
                            'group' => __('Social Profile', 'ggt-vc-addons'),
                            'heading' => __('GooglePlus Page URL', 'ggt-vc-addons'),
                            'description' => __('URL of the Google Plus page of the team member.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textfield',
                            'param_name' => 'instagram_url',
                            'group' => __('Social Profile', 'ggt-vc-addons'),
                            'heading' => __('Instagram Page URL', 'ggt-vc-addons'),
                            'description' => __('URL of the Instagram feed for the team member.', 'ggt-vc-addons'),
                        ),
                    )
                )

            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_team extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_team_member extends WPBakeryShortCode {
    }
}