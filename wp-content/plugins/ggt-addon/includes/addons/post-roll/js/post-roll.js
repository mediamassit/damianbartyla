jQuery(function ($) {

    if ($().isotope === undefined) {
        return;
    }

    var custom_css = '';
    $('.ggt-portfolio-wrap1').each(function () {

        var html_content = $(this).find('.js-isotope');
        // layout Isotope again after all images have loaded
        html_content.imagesLoaded(function () {
            html_content.isotope('layout');
        });

        var container = $(this).find('.ggt-portfolio');
        if (container.length === 0) {
            return;
        }

        $(this).find('.ggt-taxonomy-filter .ggt-filter-item a').on('click', function (e) {
            e.preventDefault();

            var selector = $(this).attr('data-value');
            container.isotope({ filter: selector });
            $(this).closest('.ggt-taxonomy-filter').children().removeClass('ggt-active');
            $(this).closest('.ggt-filter-item').addClass('ggt-active');
            return false;
        });
        
        /* --------- Custom CSS Generation --------------- */

        portfolio_elem = $(this).find('.ggt-portfolio');

        var id_selector = '#' + portfolio_elem.attr('id');

        var desktop_gutter = (typeof portfolio_elem.data('gutter') !== 'undefined') ? portfolio_elem.data('gutter') : 10;

        var tablet_gutter = (typeof portfolio_elem.data('tablet_gutter') !== 'undefined') ? portfolio_elem.data('tablet_gutter') : 10;

        var tablet_width = portfolio_elem.data('tablet_width') || 800;

        var mobile_gutter = (typeof portfolio_elem.data('mobile_gutter') !== 'undefined') ? portfolio_elem.data('mobile_gutter') : 10;

        var mobile_width = portfolio_elem.data('mobile_width') || 480;

        custom_css += id_selector + '.ggt-portfolio { margin-left: -' + desktop_gutter + 'px; margin-right: -' + desktop_gutter + 'px; }';

        custom_css += id_selector + '.ggt-portfolio .ggt-portfolio-item { padding:' + desktop_gutter + 'px; }';

        custom_css += ' @media only screen and (max-width: ' + tablet_width + 'px) { ' + id_selector + '.ggt-portfolio { margin-left: -' + tablet_gutter + 'px; margin-right: -' + tablet_gutter + 'px; } ' + id_selector + '.ggt-portfolio .ggt-portfolio-item { padding:' + tablet_gutter + 'px; } } ';

        custom_css += ' @media only screen and (max-width: ' + mobile_width + 'px) { ' + id_selector + '.ggt-portfolio { margin-left: -' + mobile_gutter + 'px; margin-right: -' + mobile_gutter + 'px; } ' + id_selector + '.ggt-portfolio .ggt-portfolio-item { padding:' + mobile_gutter + 'px; } } ';
    });

    if (custom_css != '') {
        custom_css = '<style type="text/css">' + custom_css + '</style>';
        $('head').append(custom_css);
    }
    
});