<?php

/*
Widget Name: GoGetThemes Tabs
Description: Display tabbed content in variety of styles.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/


class GGT_Tabs {

    protected $_tab_style;
    protected $_tab_elements;
    protected $_tab_panes;

    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_tabs', array($this, 'shortcode_func'));

        add_shortcode('GGT_tab', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_script('ggt-tabs', plugin_dir_url(__FILE__) . 'js/tabs' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        wp_enqueue_style('ggt-tabs', plugin_dir_url(__FILE__) . 'css/style.css', array(), GGT_VERSION);
    }

    public function shortcode_func($atts, $content = null, $tag) {

        $style = $mobile_width = $highlight_color = '';

        extract(shortcode_atts(array(
            'style' => 'style1',
            'mobile_width' => '767',
            'highlight_color' => '#f94213'
        ), $atts));

        // Imp: Helps to reset tabs collection when multiple tabs are present in the page
        $this->_tab_style = $style;
        $this->_tab_elements = array();
        $this->_tab_panes = array();

        $vertical_class = '';

        $vertical_styles = array('style7', 'style8', 'style9', 'style10');

        if (in_array($style, $vertical_styles, true)):

            $vertical_class = 'ggt-vertical';

        endif;

        do_shortcode($content); // let the child tabs get instantiated

        $uniqueid = uniqid();

        ob_start();

        ?>

        <div id="ggt-tabs-<?php echo $uniqueid; ?>"
             class="ggt-tabs <?php echo $vertical_class; ?> ggt-<?php echo esc_attr($style); ?>"
             data-mobile-width="<?php echo intval($mobile_width); ?>">

            <a href="#" class="ggt-tab-mobile-menu"><i class="ggtctrl-menu"></i>&nbsp;</a>

            <div class="ggt-tab-nav">

                <?php

                foreach ($this->_tab_elements as $tab_nav) :

                    echo $tab_nav;

                endforeach;

                ?>

            </div>

            <div class="ggt-tab-panes">

                <?php

                foreach ($this->_tab_panes as $tab_pane) :

                    echo $tab_pane;

                endforeach;

                ?>

            </div>

        </div>

        <?php $highlight_styles = array('style4', 'style6', 'style7', 'style8'); ?>

        <?php if (in_array($style, $highlight_styles, true)): ?>

            <style type="text/css">

                <?php if ($style == 'style4'): ?>

                #ggt-tabs-<?php echo $uniqueid; ?>.ggt-style4 .ggt-tab-nav .ggt-tab.ggt-active:before {
                    background: <?php echo $highlight_color; ?>;
                    }
                #ggt-tabs-<?php echo $uniqueid; ?>.ggt-style4.ggt-mobile-layout.ggt-mobile-open .ggt-tab.ggt-active {
                    border-left-color: <?php echo $highlight_color; ?>;
                    border-right-color: <?php echo $highlight_color; ?>;
                    }
                <?php elseif ($style == 'style6'): ?>

                #ggt-tabs-<?php echo $uniqueid; ?>.ggt-style6 .ggt-tab-nav .ggt-tab.ggt-active a {
                    border-color: <?php echo $highlight_color; ?>;
                    }
                <?php elseif ($style == 'style7'): ?>

                #ggt-tabs-<?php echo $uniqueid; ?>.ggt-style7 .ggt-tab-nav .ggt-tab.ggt-active a {
                    border-color: <?php echo $highlight_color; ?>;
                    }
                <?php elseif ($style == 'style8'): ?>

                #ggt-tabs-<?php echo $uniqueid; ?>.ggt-style8 .ggt-tab-nav .ggt-tab.ggt-active a {
                    border-left-color: <?php echo $highlight_color; ?>;
                    }
                <?php endif; ?>

            </style>

        <?php endif; ?>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $tab_title = $percentage = $icon_image = $icon_type = $icon_family = '';

        extract(shortcode_atts(array(
            'tab_title' => '',
            'icon_type' => 'none',
            'icon_image' => '',
            'icon_family' => 'fontawesome',
            "icon_fontawesome" => '',
            "icon_openiconic" => '',
            "icon_typicons" => '',
            "icon_entypo" => '',
            "icon_linecons" => '',

        ), $atts));

        $icon_type = esc_html($icon_type);

        if ($icon_type == 'icon' && !empty(${'icon_' . $icon_family}) && function_exists('vc_icon_element_fonts_enqueue'))
            vc_icon_element_fonts_enqueue($icon_family);

        $plain_styles = array('style2', 'style6', 'style7');

        if (in_array($this->_tab_style, $plain_styles, true)):

            $icon_type = 'none'; // do not display icons for plain styles even if chosen by the user

        endif;


        $tab_id = sanitize_title_with_dashes($tab_title) . '-' . uniqid();

        $tab_element = '<a href="#' . $tab_id . '">';

        if ($icon_type == 'icon_image') :

            $tab_element .= '<span class="ggt-image-wrapper">';

            $tab_element .= wp_get_attachment_image($icon_image, 'thumbnail', false, array('class' => 'ggt-image'));

            $tab_element .= '</span>';

        elseif ($icon_type == 'icon' && !empty(${'icon_' . $icon_family})) :

            $tab_element .= '<span class="ggt-icon-wrapper">';

            $tab_element .= GGT_get_icon(${'icon_' . $icon_family});

            $tab_element .= '</span>';

        endif;

        $tab_element .= '<span class="ggt-tab-title">';

        $tab_element .= esc_html($tab_title);

        $tab_element .= '</span>';

        $tab_element .= '</a>';

        $tab_nav = '<div class="ggt-tab">' . $tab_element . '</div>';

        $tab_content = '<div id="' . $tab_id . '" class="ggt-tab-pane">' . do_shortcode($content) . '</div>';

        $this->_tab_elements[] = $tab_nav;
        $this->_tab_panes[] = $tab_content;

    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Tabs", "ggt-vc-addons"),
                "base" => "GGT_tabs",
                "as_parent" => array('only' => 'GGT_tab'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => false,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Display tabbed content in variety of styles.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-tabs',
                "params" => array(

                    array(
                        "type" => "dropdown",
                        "param_name" => "style",
                        "heading" => __("Choose Tab Style", "ggt-vc-addons"),
                        "description" => __("Choose the particular style of tabs you need", "ggt-vc-addons"),
                        'value' => array(
                            __('Style 1', 'ggt-vc-addons') => 'style1',
                            __('Style 2', 'ggt-vc-addons') => 'style2',
                            __('Style 3', 'ggt-vc-addons') => 'style3',
                            __('Style 4', 'ggt-vc-addons') => 'style4',
                            __('Style 5', 'ggt-vc-addons') => 'style5',
                            __('Style 6', 'ggt-vc-addons') => 'style6',
                            __('Vertical Style 1', 'ggt-vc-addons') => 'style7',
                            __('Vertical Style 2', 'ggt-vc-addons') => 'style8',
                            __('Vertical Style 3', 'ggt-vc-addons') => 'style9',
                            __('Vertical Style 4', 'ggt-vc-addons') => 'style10'
                        ),
                        'std' => 'style1',
                        "admin_label" => true,
                    ),

                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'highlight_color',
                        'heading' => __('Tab Highlight color', 'ggt-vc-addons'),
                        'value' => '#f94213',
                        'dependency' => array(
                            'element' => 'style',
                            'value' => array('style4', 'style6', 'style7', 'style8')
                        ),
                    ),

                    array(
                        "type" => "GGT_number",
                        "heading" => __("Mobile Resolution", "ggt-vc-addons"),
                        "description" => __("The resolution to treat as a mobile resolution for invoking responsive tabs.", "ggt-vc-addons"),
                        "param_name" => "mobile_width",
                        "value" => 600,
                        "min" => 320,
                        "max" => 1400,
                        "suffix" => "px",
                    ),

                ),
            ));


        }
    }

    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Tab Item", "my-text-domain"),
                    "base" => "GGT_tab",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_tabs'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-tab',
                    "params" => array(
                        // add params same as with any other content element
                        array(
                            'type' => 'textfield',
                            'param_name' => 'tab_title',
                            "admin_label" => true,
                            'heading' => __('Tab Title', 'ggt-vc-addons'),
                            'description' => __('The title for the tab shown as name for tab navigation.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'dropdown',
                            'param_name' => 'icon_type',
                            'heading' => __('Choose Tab Icon Type or None', 'ggt-vc-addons'),
                            'heading' => __('Some styles may ignore icons chosen.', 'ggt-vc-addons'),
                            'std' => 'none',
                            'value' => array(
                                __('None', 'ggt-vc-addons') => 'none',
                                __('Icon', 'ggt-vc-addons') => 'icon',
                                __('Icon Image', 'ggt-vc-addons') => 'icon_image',
                            )
                        ),

                        array(
                            'type' => 'attach_image',
                            'param_name' => 'icon_image',
                            'heading' => __('Tab Image.', 'ggt-vc-addons'),
                            "dependency" => array('element' => "icon_type", 'value' => 'icon_image'),
                        ),

                        array(
                            'type' => 'dropdown',
                            'heading' => __('Icon library', 'ggt-vc-addons'),
                            'value' => array(
                                __('Font Awesome', 'ggt-vc-addons') => 'fontawesome',
                                __('Open Iconic', 'ggt-vc-addons') => 'openiconic',
                                __('Typicons', 'ggt-vc-addons') => 'typicons',
                                __('Entypo', 'ggt-vc-addons') => 'entypo',
                                __('Linecons', 'ggt-vc-addons') => 'linecons',
                            ),
                            'std' => 'fontawesome',
                            'param_name' => 'icon_family',
                            'description' => __('Select icon library.', 'ggt-vc-addons'),
                            "dependency" => array('element' => "icon_type", 'value' => 'icon'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Tab Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_fontawesome',
                            'value' => 'fa fa-info-circle',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'fontawesome',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_openiconic',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'openiconic',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'openiconic',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_typicons',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'typicons',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'typicons',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_entypo',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'entypo',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'entypo',
                            ),
                        ),
                        array(
                            'type' => 'iconpicker',
                            'heading' => __('Icon', 'ggt-vc-addons'),
                            'param_name' => 'icon_linecons',
                            'settings' => array(
                                'emptyIcon' => false,
                                // default true, display an "EMPTY" icon?
                                'type' => 'linecons',
                                'iconsPerPage' => 4000,
                                // default 100, how many icons per/page to display
                            ),
                            'dependency' => array(
                                'element' => 'icon_family',
                                'value' => 'linecons',
                            ),
                            'description' => __('Select icon from library.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'textarea_html',
                            'param_name' => 'content',
                            'heading' => __('Tab Content', 'ggt-vc-addons'),
                            'description' => __('The content of the tab.', 'ggt-vc-addons'),
                        ),

                    )
                )

            );

        }
    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_tabs extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_tab extends WPBakeryShortCode {
    }
}