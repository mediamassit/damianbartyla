<?php

/*
Widget Name: GoGetThemes Testimonials Slider
Description: Display responsive touch friendly slider of testimonials from clients/customers.
Author: GoGetThemes
Author URI: http://www.gogetthemes.com
*/


class GGT_Testimonials_Slider {

    protected $_per_line;
    protected $_desktop_testimonials_padding;
    protected $_testimonial_slider_item_text_color;
    protected $_testimonial_slider_item_title_color;
    protected $_testimonial_slider_item_url_color;


    /**
     * Get things started
     */
    public function __construct() {

        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));

        add_shortcode('GGT_testimonials_slider', array($this, 'shortcode_func'));

        add_shortcode('GGT_testimonial_slide', array($this, 'child_shortcode_func'));

        add_action('init', array($this, 'map_vc_element'));

        add_action('init', array($this, 'map_child_vc_element'));

    }

    function load_scripts() {

        wp_enqueue_script('ggt-flexslider', GGT_PLUGIN_URL . 'assets/js/jquery.flexslider' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        wp_enqueue_style('ggt-flexslider', GGT_PLUGIN_URL . 'assets/css/flexslider.css', array(), GGT_VERSION);

        wp_enqueue_script('ggt-testimonials-slider', plugin_dir_url(__FILE__) . 'js/testimonials' . GGT_BUNDLE_JS_SUFFIX . '.js', array('jquery'), GGT_VERSION);

        wp_enqueue_style('ggt-testimonials-slider', plugin_dir_url(__FILE__) . 'css/style.css', array(), GGT_VERSION);

    }





    public function shortcode_func($atts, $content = null, $tag) {

        //$slideshow_speed = $animation_speed = $animation = $pause_on_action = $pause_on_hover = $direction_nav = $control_nav = '';

        $settings = shortcode_atts(array(
            'slideshow_speed' => 5000,
            'animation_speed' => 600,
            'animation' => 'slide',
            'pause_on_action' => 'false',
            'pause_on_hover' => 'false',
            'direction_nav' => 'false',
            'control_nav' => 'false'


        ), $atts);


        $uniqueid = uniqid();




        ob_start();

        ?>







        <div
            class="ggt-testimonials-slider ggt-flexslider ggt-container"<?php foreach ($settings as $key => $val) {
            if (!empty($val)) {
                echo ' data-' . $key . '="' . esc_attr($val) . '"';
            }
        } ?>>

            <div class="ggt-slides">
                <?php

                do_shortcode($content);

                ?>

            </div>

        </div>

        <?php

        $output = ob_get_clean();

        return $output;
    }

    public function child_shortcode_func($atts, $content = null, $tag) {

        $author = $credentials = $desktop_testimonials_padding = $testimonial_slider_item_text_color = $testimonial_slider_item_title_color = $testimonial_slider_item_url_color = $author_image = '';

        extract(shortcode_atts(array(
            'author' => '',
            'credentials' => '',
            'desktop_testimonials_padding' => '77',
            'testimonial_slider_item_text_color' => '',
            'testimonial_slider_item_title_color' => '',
            'testimonial_slider_item_url_color' => '',
            'author_image' => ''


        ), $atts));

        $this->_desktop_testimonials_padding = $desktop_testimonials_padding;
        $this->_testimonial_slider_item_text_color = $testimonial_slider_item_text_color;
        $this->_testimonial_slider_item_title_color = $testimonial_slider_item_title_color;
        $this->_testimonial_slider_item_url_color = $testimonial_slider_item_url_color;


        $testimonial_slider_item_text_color = ' style="color:' . esc_attr($this->_testimonial_slider_item_text_color) . '"';
        $testimonial_slider_item_title_color = ' style="color:' . esc_attr($this->_testimonial_slider_item_title_color) . '"';
        $testimonial_slider_item_url_color = ' style="color:' . esc_attr($this->_testimonial_slider_item_url_color) . '"';


        if (function_exists('wpb_js_remove_wpautop'))
            $content = wpb_js_remove_wpautop($content); // fix unclosed/unwanted paragraph tags in $content

        ?>





<!--        <div id="ggt-test---><?php //echo $uniqueid; ?><!--" class="ggt-testimonial-padding">-->
<!--            11111</div>-->

        <div class="ggt-slide ggt-testimonial-wrapper">

            <div class="ggt-testimonial"  style="padding:<?php echo $desktop_testimonials_padding; ?>px;">
                <div class="ggt-testimonial-text" <?php echo $testimonial_slider_item_text_color; ?>>

                    <i class="ggt-icon-quote"></i>

                    <?php echo wp_kses_post($content) ?>

                </div>

                <div class="ggt-testimonial-user">

                    <div class="ggt-image-wrapper">
                        <?php echo wp_get_attachment_image($author_image, 'thumbnail', false, array('class' => 'ggt-image full')); ?>
                    </div>

                    <div class="ggt-text">
                        <h4 class="ggt-author-name" <?php echo $testimonial_slider_item_title_color; ?>><?php echo esc_html($author) ?></h4>

                        <div class="ggt-author-credentials" <?php echo $testimonial_slider_item_url_color; ?>><?php echo wp_kses_post($credentials); ?></div>
                    </div>

                </div>

            </div>

        </div>

        <?php
    }

    function map_vc_element() {
        if (function_exists("vc_map")) {

            //Register "container" content element. It will hold all your inner (child) content elements
            vc_map(array(
                "name" => __("Testimonials Slider", "ggt-vc-addons"),
                "base" => "GGT_testimonials_slider",
                "as_parent" => array('only' => 'GGT_testimonial_slide'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => true,
                "category" => __("GoGetThemes Elements", "ggt-vc-addons"),
                "is_container" => true,
                'description' => __('Capture client testimonials in a slider.', 'ggt-vc-addons'),
                "js_view" => 'VcColumnView',
                "icon" => 'icon-ggt-testimonials-slider',
                "params" => array(

                    array(
                        'type' => 'GGT_number',
                        "param_name" => "slideshow_speed",
                        'heading' => __('Slideshow speed', 'ggt-vc-addons'),
                        'value' => 5000
                    ),

                    array(
                        'type' => 'GGT_number',
                        "param_name" => "animation_speed",
                        'heading' => __('Animation Speed', 'ggt-vc-addons'),
                        'value' => 600
                    ),
                    array(
                        'type' => 'checkbox',
                        "param_name" => "pause_on_action",
                        'heading' => __('Pause slider on action.', 'ggt-vc-addons'),
                        'description' => __('Should the slideshow pause once user initiates an action using navigation/direction controls.', 'ggt-vc-addons'),
                        "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    ),

                    array(
                        'type' => 'checkbox',
                        "param_name" => "pause_on_hover",
                        'heading' => __('Pause on Hover', 'ggt-vc-addons'),
                        'description' => __('Should the slider pause on mouse hover over the slider.', 'ggt-vc-addons'),
                        "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    ),

                    array(
                        'type' => 'checkbox',
                        "param_name" => "direction_nav",
                        'heading' => __('Direction Navigation', 'ggt-vc-addons'),
                        'description' => __('Should the slider have direction navigation.', 'ggt-vc-addons'),
                        "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    ),

                    array(
                        'type' => 'checkbox',
                        "param_name" => "control_nav",
                        'heading' => __('Navigation Controls', 'ggt-vc-addons'),
                        'description' => __('Should the slider have navigation controls.', 'ggt-vc-addons'),
                        "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                    ),
                    array(
                        "type" => "GGT_number",
                        "heading" => __("Desktop Spacing", "ggt-vc-addons"),
                        "description" => __("Space in pixels in Desktop Resolution", "ggt-vc-addons"),
                        "param_name" => "desktop_testimonials_padding",
                        "admin_label" => true,
                        "value" => 50,
                        "min" => 1,
                        "max" => 500,
                        "suffix" => "px",
                    ),

                ),
            ));


        }
    }

    function map_child_vc_element() {
        if (function_exists("vc_map")) {
            vc_map(array(
                    "name" => __("Testimonials Item", "ggt-vc-addons"),
                    "base" => "GGT_testimonial_slide",
                    "content_element" => true,
                    "as_child" => array('only' => 'GGT_testimonials_slider'), // Use only|except attributes to limit parent (separate multiple values with comma)
                    "icon" => 'icon-ggt-testimonial',
                    "category" => __('Testimonials', 'ggt-vc-addons'),
                    "params" => array(
                        // add params same as with any other content element
                        array(
                            'type' => 'textfield',
                            'param_name' => 'author',
                            "admin_label" => true,
                            'heading' => __('Name', 'ggt-vc-addons'),
                            'description' => __('The author of the testimonial', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'textfield',
                            'param_name' => 'credentials',
                            'heading' => __('Author Details', 'ggt-vc-addons'),
                            'description' => __('The details of the author like company name, position held, company URL etc.', 'ggt-vc-addons'),
                        ),

                        array(
                            'type' => 'attach_image',
                            'param_name' => 'author_image',
                            'heading' => __('Author Image', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'textarea_html',
                            'param_name' => 'content',
                            'heading' => __('Text', 'ggt-vc-addons'),
                            'description' => __('What your client/customer has to say', 'ggt-vc-addons'),
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'testimonial_slider_item_text_color',
                            'heading' => __('Testimonials text color', 'ggt-vc-addons'),
                            'value' => 'transparent',
                            'group' => __('Custom color' , 'ggt-vc-addons')
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'testimonial_slider_item_title_color',
                            'heading' => __('Testimonials name color', 'ggt-vc-addons'),
                            'value' => 'transparent',
                            'group' => __('Custom color' , 'ggt-vc-addons')
                        ),
                        array(
                            'type' => 'colorpicker',
                            'param_name' => 'testimonial_slider_item_url_color',
                            'heading' => __('Testimonials Company name color', 'ggt-vc-addons'),
                            'value' => 'transparent',
                            'group' => __('Custom color' , 'ggt-vc-addons')
                        ),
                    )
                )

            );

        }
    }

//    function map_child_vc_element() {
//        if (function_exists("vc_map")) {
//
//            $testimonial_params = vc_map_integrate_shortcode('GGT_testimonial', '', __('Testimonials', 'ggt-vc-addons'));
//
//            vc_map(array(
//                    "name" => __("Testimonial Sliders Item", "ggt-vc-addons"),
//                    "base" => "GGT_testimonial_slide",
//                    "content_element" => true,
//                    "as_child" => array('only' => 'GGT_testimonials_slider'), // Use only|except attributes to limit parent (separate multiple values with comma)
//                    "icon" => 'icon-ggt-testimonials-slide',
//                    "category" => __('Testimonials', 'ggt-vc-addons'),
//                    "params" => $testimonial_params
//                )
//
//            );
//
//        }
//    }

}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_GGT_testimonials_slider extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_GGT_testimonial_slide extends WPBakeryShortCode {
    }
}
