jQuery(function ($) {

    $('.ggt-testimonials-slider').each(function () {


        var slider_elem = $(this);

        var slideshow_speed = slider_elem.data('slideshow_speed') || 5000;

        var animation_speed = slider_elem.data('animation_speed') || 600;

        var pause_on_action = slider_elem.data('pause_on_action') ? true : false;

        var pause_on_hover = slider_elem.data('pause_on_hover') ? true : false;

        var direction_nav = slider_elem.data('direction_nav') ? true : false;

        var control_nav = slider_elem.data('control_nav') ? true : false;


        slider_elem.flexslider({
            selector: ".ggt-slides > .ggt-slide",
            animation: "slide",
            slideshowSpeed: slideshow_speed,
            animationSpeed: animation_speed,
            namespace: "ggt-flex-",
            pauseOnAction: pause_on_action,
            pauseOnHover: pause_on_hover,
            controlNav: control_nav,
            directionNav: direction_nav,
            prevText: "<span></span>",
            nextText: "<span></span>",
            smoothHeight: false,
            animationLoop: true,
            slideshow: true,
            easing: "swing",
            controlsContainer: "ggt-testimonials-slider"});

    });


    // Padding


    //var custom_css = '';
    //$('.ggt-testimonial-padding').each(function () {
    //
    //    spacer_elem = $(this);
    //
    //    var id_selector = '#' + spacer_elem.attr('id');
    //
    //    var desktop_spacing = (typeof spacer_elem.data('desktop_testimonials_padding') !== 'undefined') ? spacer_elem.data('desktop_testimonials_padding') : 50;
    //
    //    var tablet_testimonials_padding = (typeof spacer_elem.data('tablet_testimonials_padding') !== 'undefined') ? spacer_elem.data('tablet_testimonials_padding') : 30;
    //
    //    var tablet_testimonials_width = spacer_elem.data('tablet_testimonials_width') || 960;
    //
    //    var mobile_testimonials_padding = (typeof spacer_elem.data('mobile_testimonials_padding') !== 'undefined') ? spacer_elem.data('mobile_testimonials_padding') : 10;
    //
    //    var mobile_testimonials_width = spacer_elem.data('mobile_testimonials_width') || 480;
    //
    //    custom_css += id_selector + ' { height:' + desktop_spacing + 'px; }';
    //
    //    custom_css += ' @media only screen and (max-width: ' + tablet_testimonials_width + 'px) { ' + id_selector + ' { height:' + tablet_testimonials_padding + 'px; } } ';
    //
    //    custom_css += ' @media only screen and (max-width: ' + mobile_testimonials_width + 'px) { ' + id_selector + ' { height:' + mobile_testimonials_padding + 'px; } } ';
    //
    //});
    //if (custom_css != '') {
    //    custom_css = '<style type="text/css">' + custom_css + '</style>';
    //    $('head').append(custom_css);
    //}


});