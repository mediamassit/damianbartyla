<?php

// Exit if accessed directly
if (!defined('ABSPATH'))
    exit;

if (!function_exists('GGT_get_vc_map_carousel_display_options')) {

    function GGT_get_vc_map_carousel_display_options() {

        $map = array(
            array(
                'type' => 'GGT_number',
                'param_name' => 'display_columns',
                'heading' => __('Columns per row', 'ggt-vc-addons'),
                'min' => 1,
                'max' => 8,
                'integer' => true,
                'value' => 3,
                'group' => __('Desktop', 'ggt-vc-addons'),
            ),
            array(
                'type' => 'GGT_number',
                'param_name' => 'scroll_columns',
                'heading' => __('Columns to scroll', 'ggt-vc-addons'),
                'min' => 1,
                'max' => 8,
                'integer' => true,
                'value' => 3,
                'group' => __('Desktop', 'ggt-vc-addons'),
            ),
            array(
                'type' => 'GGT_number',
                'param_name' => 'gutter',
                'heading' => __('Gutter', 'ggt-vc-addons'),
                'description' => __('Space between columns.', 'ggt-vc-addons'),
                'value' => 10,
                'group' => __('Desktop', 'ggt-vc-addons'),
            ),

            array(
                'type' => 'GGT_number',
                'param_name' => 'tablet_display_columns',
                'heading' => __('Columns per row', 'ggt-vc-addons'),
                'min' => 1,
                'max' => 4,
                'integer' => true,
                'value' => 2,
                'group' => __('Tablet', 'ggt-vc-addons'),
            ),
            array(
                'type' => 'GGT_number',
                'param_name' => 'tablet_scroll_columns',
                'heading' => __('Columns to scroll', 'ggt-vc-addons'),
                'min' => 1,
                'max' => 4,
                'integer' => true,
                'value' => 2,
                'group' => __('Tablet', 'ggt-vc-addons'),
            ),
            array(
                'type' => 'GGT_number',
                'param_name' => 'tablet_gutter',
                'heading' => __('Gutter', 'ggt-vc-addons'),
                'description' => __('Space between columns.', 'ggt-vc-addons'),
                'value' => 10,
                'group' => __('Tablet', 'ggt-vc-addons'),
            ),

            array(
                'type' => 'textfield',
                'param_name' => 'tablet_width',
                'heading' => __('Resolution', 'ggt-vc-addons'),
                'description' => __('The resolution to treat as a tablet resolution.', 'ggt-vc-addons'),
                'std' => 800,
                'sanitize' => 'intval',
                'group' => __('Tablet', 'ggt-vc-addons'),
            ),

            array(
                'type' => 'GGT_number',
                'param_name' => 'mobile_display_columns',
                'heading' => __('Columns per row', 'ggt-vc-addons'),
                'min' => 1,
                'max' => 3,
                'integer' => true,
                'value' => 1,
                'group' => __('Mobile', 'ggt-vc-addons'),
            ),
            array(
                'type' => 'GGT_number',
                'param_name' => 'mobile_scroll_columns',
                'heading' => __('Columns to scroll', 'ggt-vc-addons'),
                'min' => 1,
                'max' => 3,
                'integer' => true,
                'value' => 1,
                'group' => __('Mobile', 'ggt-vc-addons'),
            ),
            array(
                'type' => 'GGT_number',
                'param_name' => 'mobile_gutter',
                'heading' => __('Gutter', 'ggt-vc-addons'),
                'description' => __('Space between columns.', 'ggt-vc-addons'),
                'value' => 10,
                'group' => __('Mobile', 'ggt-vc-addons'),
            ),

            array(
                'type' => 'textfield',
                'param_name' => 'mobile_width',
                'heading' => __('Resolution', 'ggt-vc-addons'),
                'description' => __('The resolution to treat as a mobile resolution.', 'ggt-vc-addons'),
                'std' => 480,
                'sanitize' => 'intval',
                'group' => __('Mobile', 'ggt-vc-addons'),
            )
        );


        return $map;
    }
}


if (!function_exists('GGT_get_vc_map_carousel_options')) {

    function GGT_get_vc_map_carousel_options($group = '') {

        $map = array(
            array(
                'type' => 'checkbox',
                "param_name" => "arrows",
                'heading' => __('Prev/Next Arrows?', 'ggt-vc-addons'),
                "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                'group' => $group
            ),

            array(
                'type' => 'checkbox',
                "param_name" => "dots",
                'heading' => __('Show dot indicators for navigation?', 'ggt-vc-addons'),
                "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                'group' => $group
            ),

            array(
                'type' => 'checkbox',
                'param_name' => 'autoplay',
                'heading' => __('Autoplay?', 'ggt-vc-addons'),
                'description' => __('Should the carousel autoplay as in a slideshow.', 'ggt-vc-addons'),
                "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                'group' => $group
            ),

            array(
                'type' => 'GGT_number',
                "param_name" => "autoplay_speed",
                'heading' => __('Autplay speed', 'ggt-vc-addons'),
                'value' => 3000,
                'group' => $group
            ),

            array(
                'type' => 'GGT_number',
                "param_name" => "animation_speed",
                'heading' => __('Autoplay animation speed in ms', 'ggt-vc-addons'),
                'value' => 300,
                'group' => $group
            ),

            array(
                'type' => 'checkbox',
                "param_name" => "pause_on_hover",
                'heading' => __('Pause on Hover', 'ggt-vc-addons'),
                'description' => __('Should the slider pause on mouse hover over the slider.', 'ggt-vc-addons'),
                "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                'group' => $group,
            ));

        return $map;
    }
}


if (!function_exists('GGT_get_default_atts_carousel')) {

    function GGT_get_default_atts_carousel() {

        $atts = array(
            'arrows' => '',
            'dots' => '',
            'autoplay' => '',
            'autoplay_speed' => 3000,
            'animation_speed' => 300,
            'pause_on_hover' => '',
            'display_columns' => 3,
            'scroll_columns' => 3,
            'gutter' => 10,
            'tablet_display_columns' => 2,
            'tablet_scroll_columns' => 2,
            'tablet_gutter' => 10,
            'tablet_width' => 800,
            'mobile_display_columns' => 1,
            'mobile_scroll_columns' => 1,
            'mobile_gutter' => 10,
            'mobile_width' => 480,
        );

        return $atts;

    }
}