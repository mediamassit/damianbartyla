<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Interface for VC addons
 */
interface GGT_Addon {

	public function map_vc_element();
    public function shortcode_func($atts, $content, $tag);
    public function load_scripts();
}

/**
 * Interface for container addons which have a child element
 */
interface GGT_Container_Addon extends GGT_Addon {

    public function map_child_vc_element();
    public function child_shortcode_func($atts, $content, $tag);
}