<?php
/**
 * Plugin Name: GGT Addons for Visual Composer
 * Description: GGT Addon by GoGetThemes.com
 * Author: GoGetThemes
 * Author URI: http://gogetthemes.com
 * Version: 1.1
 * Text Domain: ggt-vc-addons
 * Domain Path: languages
 *
 */

// Exit if accessed directly
if (!defined('ABSPATH'))
    exit;

define('GGT_BUNDLE_JS_SUFFIX', '.min');

if (!class_exists('ggt_VC_Addons')) :

    /**
     * Main ggt_VC_Addons Class
     *
     */
    final class ggt_VC_Addons {

        /** Singleton *************************************************************/

        private static $instance;

        /**
         * Main GGT_VC_Addons Instance
         *
         * Insures that only one instance of GGT_VC_Addons exists in memory at any one
         * time. Also prevents needing to define globals all over the place.
         */
        public static function instance() {
            if (!isset(self::$instance) && !(self::$instance instanceof ggt_VC_Addons)) {

                self::$instance = new ggt_VC_Addons;

                self::$instance->setup_constants();

                add_action('plugins_loaded', array(self::$instance, 'load_plugin_textdomain'));

                add_action('plugins_loaded', array(self::$instance, 'includes'));

                add_action('plugins_loaded', array(self::$instance, 'setup'));

                self::$instance->hooks();

            }
            return self::$instance;
        }

        /**
         * Throw error on object clone
         *
         * The whole idea of the singleton design pattern is that there is a single
         * object therefore, we don't want the object to be cloned.
         */
        public function __clone() {
            // Cloning instances of the class is forbidden
            _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'ggt-vc-addons'), '1.6');
        }

        /**
         * Disable unserializing of the class
         *
         */
        public function __wakeup() {
            // Unserializing instances of the class is forbidden
            _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'ggt-vc-addons'), '1.6');
        }

        /**
         * Setup plugin constants
         *
         */
        private function setup_constants() {

            // Plugin version
            if (!defined('GGT_VERSION')) {
                define('GGT_VERSION', '1.0');
            }

            // Plugin Folder Path
            if (!defined('GGT_PLUGIN_DIR')) {
                define('GGT_PLUGIN_DIR', plugin_dir_path(__FILE__));
            }

            // Plugin Folder Path
            if (!defined('GGT_ADDONS_DIR')) {
                define('GGT_ADDONS_DIR', plugin_dir_path(__FILE__) . 'includes/addons/');
            }

            // Plugin Folder URL
            if (!defined('GGT_PLUGIN_URL')) {
                define('GGT_PLUGIN_URL', plugin_dir_url(__FILE__));
            }

            // Plugin Root File
            if (!defined('GGT_PLUGIN_FILE')) {
                define('GGT_PLUGIN_FILE', __FILE__);
            }
        }

        /**
         * Include required files
         *
         */
        public function includes() {

            require_once GGT_PLUGIN_DIR . 'includes/helper-functions.php';
            require_once GGT_PLUGIN_DIR . 'includes/mapper-functions.php';

            require_once GGT_ADDONS_DIR . 'accordion/class-ggt-accordion.php';
            require_once GGT_ADDONS_DIR . 'carousel/class-ggt-carousel.php';
            require_once GGT_ADDONS_DIR . 'clients/class-ggt-clients.php';
            require_once GGT_ADDONS_DIR . 'banner/class-ggt-banners.php';
            require_once GGT_ADDONS_DIR . 'heading/class-ggt-heading.php';
            require_once GGT_ADDONS_DIR . 'info-box/class-ggt-infobox.php';
            require_once GGT_ADDONS_DIR . 'calltoaction/class-ggt-calltoaction.php';
            require_once GGT_ADDONS_DIR . 'title/class-ggt-title.php';
            require_once GGT_ADDONS_DIR . 'spacer/class-ggt-spacer.php';
            require_once GGT_ADDONS_DIR . 'recent-posts/class-ggt-recent-posts.php';
            require_once GGT_ADDONS_DIR . 'post-loop/class-ggt-post-loop.php';
            require_once GGT_ADDONS_DIR . 'post-roll/class-ggt-post-roll.php';
            require_once GGT_ADDONS_DIR . 'counters/class-ggt-counters.php';
            require_once GGT_ADDONS_DIR . 'piecharts/class-ggt-piecharts.php';
            require_once GGT_ADDONS_DIR . 'portfolio/class-ggt-portfolio.php';
            require_once GGT_ADDONS_DIR . 'posts-carousel/class-ggt-posts-carousel.php';
            require_once GGT_ADDONS_DIR . 'pricing-table/class-ggt-pricing-table.php';
            require_once GGT_ADDONS_DIR . 'button/class-ggt-button.php';
            require_once GGT_ADDONS_DIR . 'services/class-ggt-services.php';
            require_once GGT_ADDONS_DIR . 'stats-bar/class-ggt-stats-bar.php';
            require_once GGT_ADDONS_DIR . 'tabs/class-ggt-tabs.php';
            require_once GGT_ADDONS_DIR . 'team/class-ggt-team.php';
            require_once GGT_ADDONS_DIR . 'testimonials/class-ggt-testimonials.php';
            require_once GGT_ADDONS_DIR . 'testimonials-slider/class-ggt-testimonials-slider.php';


            require_once GGT_PLUGIN_DIR . 'includes/params/number/class-ggt-number-param.php';

        }

        /* Setup all addons with their shortcodes and VC Mappings */
        public function setup() {

            /* Custom VC Elements */
            new GGT_Accordion();
            new GGT_Carousel();
            new GGT_Clients();
            new GGT_Banners();
            new GGT_Heading();
            new GGT_Infobox();
            new GGT_Calltoaction();
            new GGT_Title();
            new GGT_Spacer();
            new GGT_Recent_posts();
            new GGT_Postloop();
            new GGT_Postroll();
            new GGT_Counters();
            new GGT_Piecharts();
            new GGT_Portfolio();
            new GGT_Posts_Carousel();
            new GGT_Pricing_Table();
            new GGT_Button();
            new GGT_Services();
            new GGT_Stats_Bars();
            new GGT_Tabs();
            new GGT_Team();
            new GGT_Testimonials();
            new GGT_Testimonials_Slider();

            /* Custom VC Param Types */
            new GGT_Number_Param();

        }

        /**
         * Load Plugin Text Domain
         *
         * Looks for the plugin translation files in certain directories and loads
         * them to allow the plugin to be localised
         */
        public function load_plugin_textdomain() {

            $lang_dir = apply_filters('GGT_vc_addons_lang_dir', trailingslashit(GGT_PLUGIN_DIR . 'languages'));

            // Traditional WordPress plugin locale filter
            $locale = apply_filters('plugin_locale', get_locale(), 'ggt-vc-addons');
            $mofile = sprintf('%1$s-%2$s.mo', 'ggt-vc-addons', $locale);

            // Setup paths to current locale file
            $mofile_local = $lang_dir . $mofile;

            if (file_exists($mofile_local)) {
                // Look in the /wp-content/plugins/ggt-vc-addons/languages/ folder
                load_textdomain('ggt-vc-addons', $mofile_local);
            }
            else {
                // Load the default language files
                load_plugin_textdomain('ggt-vc-addons', false, $lang_dir);
            }

            return false;
        }

        /**
         * Setup the default hooks and actions
         */
        private function hooks() {

            add_action('admin_enqueue_scripts', array($this, 'load_admin_scripts'), 100);

            add_action('wp_enqueue_scripts', array($this, 'load_frontend_scripts'), 100);

            add_action('init', array($this, 'modify_existing_mappings'), 100);

            // Filter to replace default css class names for vc_row shortcode and vc_column
            add_filter('vc_shortcodes_css_class', array($this, 'custom_css_classes_for_vc_row'), 10, 3);

        }


        /**
         * Load Frontend Scripts/Styles
         *
         */
        public function load_frontend_scripts() {


            wp_register_style('ggt-frontend-styles', GGT_PLUGIN_URL . 'assets/css/ggt-frontend.css', array(), GGT_VERSION);
            wp_enqueue_style('ggt-frontend-styles');

//            wp_register_style('ggt-icomoon-styles', GGT_PLUGIN_URL . 'assets/css/icomoon.css', array(), GGT_VERSION);
//            wp_enqueue_style('ggt-icomoon-styles');

            wp_register_script('ggt-frontend-scripts', GGT_PLUGIN_URL . 'assets/js/ggt-frontend' . GGT_BUNDLE_JS_SUFFIX . '.js', array(), GGT_VERSION, true);
            wp_enqueue_script('ggt-frontend-scripts');

        }

        /**
         * Load Admin Scripts/Styles
         *
         */
        public function load_admin_scripts() {

//            wp_register_style('ggt-admin-styles', GGT_PLUGIN_URL . 'assets/css/ggt-admin.css', array(), GGT_VERSION);
//            wp_enqueue_style('ggt-admin-styles');
//
//            wp_register_script('ggt-admin-scripts', GGT_PLUGIN_URL . 'assets/js/ggt-admin' . GGT_BUNDLE_JS_SUFFIX . '.js', array(), GGT_VERSION, true);
//            wp_enqueue_script('ggt-admin-scripts');

            wp_enqueue_script('jquery-ui-datepicker');
        }



        /**
         * Load Admin Scripts/Styles
         *
         */
        function modify_existing_mappings() {
            $attributes = array(
                'type' => 'checkbox',
                'heading' => "Dark Background?",
                'param_name' => 'GGT_dark_bg',
                "value" => array(__("Yes", "ggt-vc-addons") => 'true'),
                'description' => __("Indicate if this row has a dark background color. Dark color scheme will be applied for all elements in this row.", "ggt-vc-addons")
            );

            if (function_exists('vc_add_param')) {
                vc_add_param('vc_row', $attributes);
                vc_add_param('vc_row_inner', $attributes);
            }
        }

        /**
         * Load Admin Scripts/Styles
         * Take care of situations where themes do not pass the atts value to the filter
         */
        function custom_css_classes_for_vc_row($class_string, $tag, $atts = null) {

            if (!empty($atts)) {
                if ($tag == 'vc_row' || $tag == 'vc_row_inner') {
                    if (isset($atts['GGT_dark_bg']) && ($atts['GGT_dark_bg'] == 'true'))
                        $class_string .= ' ggt-dark-bg';
                }
            }
            return $class_string;
        }

    }

endif; // End if class_exists check


/**
 * The main function responsible for returning the one true ggt_VC_Addons
 * Instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * Example: <?php $ggt = GGT(); ?>
 */

add_action('init', 'register_post_types');
function register_post_types(){
    register_post_type('post_portfolio', array(
        'label'  => null,
        'labels' => array(
            'name'               => 'Portfolio',
            'singular_name'      => 'Portfolio item',
            'add_new'            => 'Add Portfolio',
            'add_new_item'       => 'Add Portfolio item',
            'edit_item'          => 'Edit Portfolio item',
            'new_item'           => 'New Portfolio item',
            'view_item'          => 'View Portfolio item',
            'search_items'       => 'Search Portfolio item',
            'not_found'          => 'Not Found',
            'not_found_in_trash' => 'Not Found in Trash',
            'parent_item_colon'  => '',
            'menu_name'          => 'Portfolio',
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => null,
        'exclude_from_search' => null,
        'show_ui'             => null,
        'show_in_menu'        => null,
        'menu_position'       => null,
        'menu_icon'           => null,
        'hierarchical'        => false,
        'supports'            => array('title','editor','thumbnail'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'taxonomies'          => array('category'),
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
        'show_in_nav_menus'   => null,
    ) );
}

function GGT() {
    return ggt_VC_Addons::instance();
}

// Get GGT Running
GGT();