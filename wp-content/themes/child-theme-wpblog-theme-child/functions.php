<?php add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',get_stylesheet_directory_uri() . '/style.css',array('parent-style') );

}


	register_nav_menus( array(
		'footer_custom'      => _x( 'Footer Menu custom', 'backend', 'wpblog-theme' ),
		'baner_menu'      => _x( 'Top Baner Menu', 'backend', 'wpblog-theme' ),
	) );

function get_template_part_args($template, $args = array(), $indent = false) {
	extract((array) $args);

	if($indent > 0) {
		ob_start();
		include(locate_template($template . '.php'));
		$html = ob_get_clean();

		echo implode("\n".str_repeat('	', (int) $indent), explode("\n", $html));
	}
	else {
		include(locate_template($template . '.php'));
	}
}

if(!function_exists('get_paginate_links') && function_exists('paginate_links')) {
	/**
	 * Return paginate links array.
	 * @param mixed $args
	 * @return bool
	 */
	function get_paginate_links($args) {
		$links = paginate_links(array_merge($args, array('type' => 'array', 'prev_text' => 'PREV', 'next_text' => 'NEXT')));
		$data = array();

		if(is_array($links) && sizeof($links) > 0) {
			foreach($links as $link) {
				preg_match_all('#<([A-Za-z]+)\s?(.*?)>(.*?)<\/[A-Za-z]+>#', $link, $matches);
				
				$tag = @$matches[1][0];
				$atts = @$matches[2][0];
				$text = @$matches[3][0];

				if($tag == 'a') {
					preg_match('#href=(\'|")(.*?)(\'|")#', $atts, $link);
					$data[] = array(
						'text' => $text,
						'link' => @$link[2],
						'current' => false,
						'next' => $text === 'NEXT',
						'prev' => $text === 'PREV'
					);
				}

				if($tag == 'span' && (strpos($atts, 'dots') || strpos($atts, 'current'))) {
					$data[] = array(
						'text' => $text,
						'link' => null,
						'current' => strpos($atts, 'current') !== false,
						'next' => false,
						'prev' => false
					);
				}
			}
		}

		foreach($data as $i => $link) {
			if($link['link']) {
				$data[$i]['link'] .= ($_SERVER['QUERY_STRING'] && strpos($link['link'], '?') === false) ? '?'.$_SERVER['QUERY_STRING'] : '';
			}
		}

		return $data;
	}
}




function facebookDisplay($img){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/657696174325788/posts?access_token=1407420879352933|YNjkcCv999W8gAcqeud9frTo2Sk");
	$result = curl_exec($ch);
	curl_close($ch);
	$obj = json_decode($result);
	$obj = $obj->data;
	//var_dump($obj);
	for($i=0; $i<2; $i++){
		$single_obj = $obj[$i]->id;
		$single_obj = str_replace('657696174325788_','',$single_obj);
		if($i==0){
					 echo '<script src="//connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v2.5" 
      async></script>  
	  	<div class="col-md-5"><img src="'.$img.'" style="vertical-align:top;" class="fb_img"></div>
  		<div class="col-md-7" style="padding:0; padding-bottom:10px;"><div class="fb-post" 
      data-href="https://www.facebook.com/657696174325788/posts/'.$single_obj.'/"
      data-width="auto"></div></div>';
		}else{
			
		 echo '
		 <div class="fb-post" 
      data-href="https://www.facebook.com/657696174325788/posts/'.$single_obj.'/"
      data-width="auto"></div>';
		}
	}
	
}



function tweet($img) {
$token = '866986183796117509-KV7XS8Y40N6qesT8zm1Kf9oOfoKeZKC';
$token_secret = 'qvUArzDpuG5pu2CgN0oywWKAgDIaEkQyZMJfsSb6P4pba';
$consumer_key = 'kci6tcgsBvEzjaMpgUbElNgAq';
$consumer_secret = 'xcxj7L7KKXn8FrVkfElwM294vfZjfkyf0Mh3lsWlCHdOKiNvca';
$username = 'MediamassDev';
	
$host = 'api.twitter.com';
$method = 'GET';
$path = '/1.1/statuses/user_timeline.json'; // api call path

$query = array( // query parameters
    'screen_name' => $username,
    'count' => '2'
);

$oauth = array(
    'oauth_consumer_key' => $consumer_key,
    'oauth_token' => $token,
    'oauth_nonce' => (string)mt_rand(), // a stronger nonce is recommended
    'oauth_timestamp' => time(),
    'oauth_signature_method' => 'HMAC-SHA1',
    'oauth_version' => '1.0'
);

$oauth = array_map("rawurlencode", $oauth); // must be encoded before sorting
$query = array_map("rawurlencode", $query);

$arr = array_merge($oauth, $query); // combine the values THEN sort

asort($arr); // secondary sort (value)
ksort($arr); // primary sort (key)

// http_build_query automatically encodes, but our parameters
// are already encoded, and must be by this point, so we undo
// the encoding step
$querystring = urldecode(http_build_query($arr, '', '&'));

$url = "https://$host$path";

// mash everything together for the text to hash
$base_string = $method."&".rawurlencode($url)."&".rawurlencode($querystring);

// same with the key
$key = rawurlencode($consumer_secret)."&".rawurlencode($token_secret);

// generate the hash
$signature = rawurlencode(base64_encode(hash_hmac('sha1', $base_string, $key, true)));

// this time we're using a normal GET query, and we're only encoding the query params
// (without the oauth params)
$url .= "?".http_build_query($query);
$url=str_replace("&amp;","&",$url); //Patch by @Frewuill

$oauth['oauth_signature'] = $signature; // don't want to abandon all that work!
ksort($oauth); // probably not necessary, but twitter's demo does it

// also not necessary, but twitter's demo does this too
function add_quotes($str) { return '"'.$str.'"'; }
$oauth = array_map("add_quotes", $oauth);

// this is the full value of the Authorization line
$auth = "OAuth " . urldecode(http_build_query($oauth, '', ', '));

// if you're doing post, you need to skip the GET building above
// and instead supply query parameters to CURLOPT_POSTFIELDS
$options = array( CURLOPT_HTTPHEADER => array("Authorization: $auth"),
                  //CURLOPT_POSTFIELDS => $postfields,
                  CURLOPT_HEADER => false,
                  CURLOPT_URL => $url,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_SSL_VERIFYPEER => false);

// do our business
$feed = curl_init();
curl_setopt_array($feed, $options);
$json = curl_exec($feed);
curl_close($feed);

$twitter_data = json_decode($json);
	for($i=0;$i<2;$i++){
		$id = $twitter_data[$i]->id_str;
		if($id){
			if($i==0){
			echo '<img src="'.$img.'" class="tw_img"><blockquote class="twitter-tweet" data-lang="pl" >
					<p lang="pl" dir="ltr"><a href="https://twitter.com/'.$username.'/status/'.$id.'"></a>
				</blockquote>';
			}else{
				echo '<div style="clear:both;"></div><blockquote class="twitter-tweet" data-lang="pl">
					<p lang="pl" dir="ltr"><a href="https://twitter.com/'.$username.'/status/'.$id.'"></a>
				</blockquote>';
			}
		}
	}
}