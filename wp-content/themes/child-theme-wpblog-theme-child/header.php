<?php
/**
 * @package        WPblog Theme
 * @author         GoGetThemes.com
 * @copyright      2017 GoGetThemes
 * @version        Release: v1.0
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 */

?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0" />

    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
        <?php if (wpblog_theme_option('favicon_img', 'url')) { ?>
            <link rel="shortcut icon" href="<?php echo esc_url(wpblog_theme_option('favicon_img', 'url')); ?>"
                  type="image/x-icon"/>
        <?php } ?>
    <?php } ?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
</head>
<body <?php body_class();?>>
<div class="head_baner">
	<div class="container">
		<div class="row">
			<div class="col-xs-10">
				<div class="head_name">
					<a href="<?php echo get_home_url(); ?>"><h1>Damian <span>Bartyla</span></h1></a>
					<h3>prezydent Bytomia</h3>
				</div>
			</div>
			<div class="col-xs-2">
				<div class="head_social">
					<?php get_template_part("part-social-footer"); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<ul class="baner_menu">
				<?php
					wp_nav_menu(
							array(
									'container'            => "",
									'container_class'    => "",
									'container_id'        => "",
									'fallback_cb'        => false,
									'menu_class'        => "",
									'before'            => '',
									'after'             => '',
									'link_before'        => '',
									'link_after'        => '',
									'items_wrap'        => '%3$s',
									'theme_location'    => "baner_menu",
									'walker'            => '',
							)
					);
				?>
			</ul>
		</div>
		<img class="comment" src="<?php echo get_stylesheet_directory_uri();?>/img/komentuje.png" alt="">
	<div class="bartyla">
		<img src="<?php echo get_stylesheet_directory_uri();?>/img/damian_bartyla.png" alt="">
	</div>
	</div>
</div>
<div class="header-style
    <?php
    if( function_exists('get_field')) {
        $transparent_header = get_field('transparent_header');
        echo esc_html ( $transparent_header );
        echo esc_html ( ' ' );
        $select_header_skin = get_field('select_header_skin');
        echo esc_html ( $select_header_skin );

        }
    else {}
    ?>">





<div id="page" class="hfeed site page">
	<?php if(is_front_page() || is_home()): ?>
	<header id="masthead" class="site-header">
		<?php wpblog_theme_header();?>
	</header>
	<?php 
	else:
		echo '<div style="height:40px;"></div>';
	endif; ?>
	<div id="main" class="wrapper main">