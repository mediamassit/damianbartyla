<?php if($max_pages > 1) : ?>
<div class="pagination__wrapper">
	<nav>
		<ul>
			<?php foreach($pagination as $link) : ?>
			<?php if($link['current']) : ?>
			<li  class="active"><a href="#"><span><?php echo $link['text']; ?></span></a></li>
			<?php elseif($link['prev']) : ?>
			<li><a href="<?php echo $link['link']; ?>" class="prev"><span><em><?php _e('<', THEME_NAME); ?></em></span></a></li>
			<?php elseif($link['next']) : ?>
			<li><a href="<?php echo $link['link']; ?>" class="next"><span><em><?php _e('>', THEME_NAME); ?></em></span></a></li>
			<?php else : ?>
			<li><a href="<?php echo $link['link']; ?>"><span><?php echo $link['text']; ?></span></a></li>
			<?php endif; ?>
			<?php endforeach; ?>
		</ul>
	</nav>
</div>
<?php endif; ?>
