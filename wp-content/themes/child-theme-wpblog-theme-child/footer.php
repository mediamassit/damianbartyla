<?php
/**
 * @package        WPblog Theme
 * @author         GoGetThemes.com
 * @copyright      2017 GoGetThemes
 * @version        Release: v1.0
 * The template for displaying the footer
 */
?>

	<div class="container" style="margin-bottom:30px;margin-top:30px;">
	<div class="row">
				<div class="col-xs-12">
					<img src="<?php echo get_stylesheet_directory_uri()?>/img/dk.png" alt="" style="width:100%;">
				</div>
			<div class="col-md-6">
				
				<?php 
				$img_tw = get_stylesheet_directory_uri().'/img/tw.png';
				tweet($img_tw); ?>
			</div>
			<div class="col-md-6">
					<?php 
					$img = get_stylesheet_directory_uri().'/img/fb.png';
					facebookDisplay($img); ?>
			</div>
		</div>
	</div>
	</div><!-- #main .wrapper -->
	<footer class="footer-wrap">
        <?php if ( is_active_sidebar( 'sidebar-subscribe1' ) || is_active_sidebar( 'sidebar-subscribe2' )) : ?>
            <div class="container">
                <?php if ( is_active_sidebar( 'sidebar-subscribe1' ) ) : ?>
                    <div class="col-lg-6">
                        <?php dynamic_sidebar( 'sidebar-subscribe1' ); ?>
                    </div>
                <?php endif; ?>
                <?php if ( is_active_sidebar( 'sidebar-subscribe2' ) ) : ?>
                    <div class="col-lg-6">
                        <?php dynamic_sidebar( 'sidebar-subscribe2' ); ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <?php if ( is_active_sidebar( 'sidebar-footer1' ) || is_active_sidebar( 'sidebar-footer2' ) || is_active_sidebar( 'sidebar-footer3' ) || is_active_sidebar( 'sidebar-footer4' )) : ?>
            <div class="container footer-widget-area">
                <?php if ( is_active_sidebar( 'sidebar-footer1' ) ) : ?>
                    <div class="col-lg-3">
                        <?php dynamic_sidebar( 'sidebar-footer1' ); ?>
                    </div>
                <?php endif; ?>
                <?php if ( is_active_sidebar( 'sidebar-footer2' ) ) : ?>
                    <div class="col-lg-3">
                        <?php dynamic_sidebar( 'sidebar-footer2' ); ?>
                    </div>
                <?php endif; ?>
                <?php if ( is_active_sidebar( 'sidebar-footer3' ) ) : ?>
                    <div class="col-lg-3">
                        <?php dynamic_sidebar( 'sidebar-footer3' ); ?>
                    </div>
                <?php endif; ?>
                <?php if ( is_active_sidebar( 'sidebar-footer4' ) ) : ?>
                    <div class="col-lg-3">
                        <?php dynamic_sidebar( 'sidebar-footer4' ); ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>

            <div class="row site-info-menu-wrap">
                <div class="container">
					<!--cut social-->
                    <div class="col-lg-8 site-info-menu">
                        <div class="footer-menu">
                            <?php dynamic_sidebar( 'sidebar-footer-menu' ); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row site-info">
                <div class="container">
                    <div class="col-lg-4">
                        <div class="site-info-social">
                            <?php get_template_part("part-social-footer"); ?>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="site-info-copyright">
                            <?php if ('' != wpblog_theme_option('copyright_text')): ?>
                                <?php echo esc_html(wpblog_theme_option('copyright_text')); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-lg-4 text-right">
                        <div class="site-info-menu">
                            <ul><?php
							wp_nav_menu(
									array(
											'container'            => "",
											'container_class'    => "",
											'container_id'        => "",
											'fallback_cb'        => false,
											'menu_class'        => "",
											'before'            => '',
											'after'             => '',
											'link_before'        => '',
											'link_after'        => '',
											'items_wrap'        => '%3$s',
											'theme_location'    => "footer_custom",
											'walker'            => '',
									)
							);
							?>
							</ul>
                        </div>
                    </div>
                </div>
            </div>

    </footer>
</div>

<?php wp_footer(); ?>
<script>
( function( $ ) {
	$( '.read-more-link' ).html('czytaj więcej');
	$('.tweet-InformationCircle-widgetParent').css('min-height','300px');
} )( jQuery );
</script>
</div>
</body>
</html>