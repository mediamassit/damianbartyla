��    #      4      L      L  
   M  	   X     b     x  	   �     �    �     �  V   �                3     H     Y     l  W   q     �     �     �     �     	          #  E   /     u     |  	   �     �  .   �     �  $   �          $     *  \  ?     �	     �	     �	     �	     �	     �	    �	       Z        r     z  6   �     �     �     �  a   �     M     _     q     �     �     �     �  D   �                    '     C  '   c  $   �  	   �     �     �   % Comments % Replies &larr; Older Comments &larr; Previous 1 Comment 1 Reply <span class="meta-prep meta-prep-entry-date">Published </span> <span class="entry-date"><time class="entry-date" datetime="%1$s">%2$s</time></span> at <a href="%3$s" title="Link to full-size image">%4$s &times; %5$s</a> in <a href="%6$s" title="Return to %7$s" rel="gallery">%8$s</a>. About %s Apologies, but no results were found. Perhaps searching will help find a related post. Archives Comment navigation Comments are closed. Continue reading Daily Archives: %s Edit It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment Leave a reply Newer Comments &rarr; Newer posts Next &rarr; Nothing Found Older posts One thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; Pages: Post author Read more Search Results for: %s This is somewhat embarrassing, isn&rsquo;t it? View all posts by %s Your comment is awaiting moderation. labelSearch for: says: submit buttonSearch Project-Id-Version: WordPress Blank Pot v1.0.0
POT-Creation-Date: 2016-05-24 15:03+0700
PO-Revision-Date: 2017-05-22 11:53+0000
Last-Translator: mediamass <mediamass.dev@gmail.com>
Language-Team: Polish
Report-Msgid-Bugs-To: Translator Name <translations@example.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2)
X-Textdomain-Support: yesX-Generator: Poedit 1.6.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html_x:1,2c;esc_html__;esc_attr_e;esc_attr_x:1,2c;esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c;_n:1,2;_n_noop:1,2;__ngettext:1,2;__ngettext_noop:1,2;_c,_nc:4c,1,2
X-Poedit-Basepath: ..
Language: pl-PL
X-Generator: Loco - https://localise.biz/
X-Poedit-SearchPath-0: . % Komentarzy % Odpowiedzi Starsze komentarze &larr; Poprzedni 1 komentarz
 1 Odpowiedź <span class="meta-prep meta-prep-entry-date">Published </span> <span class="entry-date"><time class="entry-date" datetime="%1$s">%2$s</time></span> at <a href="%3$s" title="Link to full-size image">%4$s &times; %5$s</a> in <a href="%6$s" title="Return to %7$s" rel="gallery">%8$s</a>. O% s
 Przepraszamy, ale nie znaleziono wyników. Być może wyszukiwanie pomoże znaleźć post. Archiwa Nawiguj Możliwość dodawania komentarzy nie jest dostępna.
 Kontynuuj czytanie
 Codzienne archiwum:% s Edytuj Wydaje się, że nie możemy znaleźć tego, czego szukasz. Być może wyszukiwanie może pomóc. Zostaw komentarz
 Zostaw odpowiedź Nowsze komentarze Nowsze posty Następny &rarr; nic nie znaleziono
 Starsze posty komentarz do:  &ldquo;%2$s&rdquo; komentarze do:  &ldquo;%2$s&rdquo; Strony:
 Autor Czytaj więcej
 Wyniki wyszukiwania dla: %s To trochę kłopotliwe, co nie? 
Zobacz wszystkie posty użytkownika% s Twój komentarz czeka na moderację. Szukaj :    Szukaj 